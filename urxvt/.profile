# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# WARNING: PROFILE IS NOT SOURCED WITH ZSH UNLESS MANUALLY DONE BY ZPROFILE

# Include root libs
PATH="/sbin:/usr/sbin:$PATH"

# Add games dir if available
if [ -d "/usr/games/bin" ]; then
	export PATH="$PATH:/usr/games/bin"
elif [ -d "/usr/games/" ]; then
	export PATH="$PATH:/usr/games/"
fi


# set PATH so it includes user's private bin if it exists
# Also include ~/.bin
if [ -d "$HOME/.bin" ] ; then
	export PATH="$HOME/.bin:$PATH"
fi

if [ -d "$HOME/.local/bin" ]; then
	# Prefer system libs over pip
	export PATH="$PATH:${HOME}/.local/bin"
fi

if [ -d "/opt/bin" ]; then
	export PATH="/opt/bin:$PATH"
fi

# CASK
if [ -d "$HOME/.cask/" ]; then
	export PATH="$PATH:${HOME}/.cask/bin"
fi
if [ -d "$HOME/.cargo/bin" ]; then
	export PATH="$PATH:${HOME}/.cargo/bin"
fi
if [ -d "/usr/share/bcc/tools" ]; then
	export PATH="$PATH:/usr/share/bcc/tools"
fi

export XDG_CURRENT_DESKTOP=X-Generic
# export QT_STYLE_OVERRIDE=oxygen

# Use ZSH
if command -v zsh >/dev/null 2>&1; then
	export SHELL=/bin/zsh
fi

# Default apps
if command -v vim >/dev/null 2>&1; then
	export EDITOR='vim'
else
	export EDITOR='vi'
fi
export PAGER='less'

# Tab size of 4 in a pager
export LESS='-R -x4'

# if command -v qutebrowser >/dev/null 2>&1; then
# 	export BROWSER='qutebrowser --loglines=0'
# fi

# For gnus
export EMAIL="kaygkamat@gmail.com"
export NAME="Kay Kamat"
export SMTPSERVER="smtp.gmail.com"
export DEBEMAIL=$EMAIL
export DEBFULLNAME=$NAME
export DEBDELAY=15

export SUDO_PROMPT="[su]: "

# robocup-software
export SOCCER_THEME="DARCULIZED"

# Fix some bugs in java apps on bspwm
export _JAVA_AWT_WM_NONREPARENTING=1

# Android stupidity
export ANDROID_EMULATOR_USE_SYSTEM_LIBS=1

# Gnu GLOBAL use pygments plugin
# TODO check to validate non-deb paths
if [ -f /usr/lib/gtags/pygments-parser.so ]; then
	export GTAGSLABEL=pygments
fi

# ssh-agent is managed by ssh-ident

[ -f /usr/local/bin/virtualenvwrapper.sh ] && source /usr/local/bin/virtualenvwrapper.sh
[ -f /usr/share/virtualenvwrapper/virtualenvwrapper.sh ] && source /usr/share/virtualenvwrapper/virtualenvwrapper.sh
# cargo
[ -f ~/.cargo/env ] && source "$HOME/.cargo/env"

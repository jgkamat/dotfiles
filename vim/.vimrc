" Jay Kamat's vimrc! Its very typical though...


" Plug automatic install
if empty(glob('~/.vim/autoload/plug.vim'))
	silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
				\ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	autocmd VimEnter * PlugInstall
endif

" set the runtime path to include Vundle and initialize
call plug#begin('~/.vim/plugged')

" ================================

" install plugins through plug
Plug 'editorconfig/editorconfig-vim'
" commenter script
Plug 'tomtom/tcomment_vim'
" naviation
Plug 'christoomey/vim-tmux-navigator'
"ctrl p
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': 'yes \| ./install' }
"useful keybindings
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-surround'

Plug 'vim-scripts/Highlight-UnMatched-Brackets'

" undo tree
Plug 'simnalamburt/vim-mundo'

" solarized
Plug 'romainl/flattened'

" ================================

" end plug setup.
call plug#end()
filetype plugin indent on
"begin my vimrc

" Highlight search
set hlsearch
"sets up mouse integration and copy (copy requires vim-gtk and vim 4.3+)
if !has('nvim')
	set ttymouse=xterm2
else
	" Clear search on esc
	nnoremap <silent> <Esc> <Esc>:nohlsearch<CR>
endif

set mouse=a
set clipboard^=unnamed,unnamedplus

" sets the timeout to zero
set ttimeout
set ttimeoutlen=0

"sets tabs to four spaces
"set softtabstop=4 sw=4 expandtab

" Try to force size 4 tabs if editorconfig dosent work.
set ts=4

" Set formatoptions, I feel really bad for overriding this...
" This adds the things I want always, and removes the stuff I dont!
au FileType * setlocal formatoptions-=o formatoptions+=j1crq

" Reccomendation from editorconfig-vim
let g:EditorConfig_exclude_patterns = ['fugitive://.*']

"sets background color and syntax highlighting
set background=dark
syntax on

" Gui options
set go-=m  "remove menu bar
set go-=T  "remove toolbar
set go-=r  "remove right-hand scroll bar
set go-=L  "remove left-hand scroll bar

" Old workaround
" 	set t_Co=16
" 	let g:solarized_termcolors=16
" 	colorscheme solarized

" Dont use solarized in other terminals (I haven't configured)
let term=$TERM
if term == 'rxvt-unicode'
elseif term == 'rxvt-unicode-256color'
elseif term == 'screen-256color'
else
	" set t_Co=256
	let g:solarized_termcolors=256
	" Don't use x clipboard if in dumb term
	if term == 'linux'
		set clipboard&
	endif
endif

" force a reload of colorschemes
colorscheme default
colorscheme flattened_dark
" Fix bg color being broken
hi  Normal                                  ctermfg=12  ctermbg='NONE'  guifg=#839496  guibg=#002b36  gui=NONE

"turns on end of line whitespace checker
highlight ExtraWhitespace ctermbg=black guibg=darkgrey
au InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
au InsertLeave * match ExtraWhitespace /\s\+$/
match ExtraWhitespace /\s\+$/

"turns on side line numbers
set autoindent
set relativenumber
set number

" word wrap settings
set wrap linebreak nolist
set breakindent

" make yanking better
map Y y$

" no help
map <F1> <Esc>
imap <F1> <Esc>

"maps f2 to turn on and off line numbers
map <silent> <f2> :set invnumber <CR>:set invrelativenumber <CR>

"set better splitting behavior
set splitbelow
set splitright

"set up spellchecker
set spelllang=en_us
" no torture in spellcheck
hi clear SpellBad
hi clear SpellRare
hi clear SpellLocal
hi clear SpellCap
hi SpellBad cterm=underline ctermfg=red
hi SpellCap cterm=underline ctermfg=red

"Moving around w/o ctrl-w.
noremap <C-J> <C-W><C-J>
noremap <C-K> <C-W><C-K>
noremap <C-L> <C-W><C-L>
noremap <C-H> <C-W><C-H>

" Nice exit
command Q q
command WQ wq
command W w
"aliases!
command T term
command Term term

" tex slowdown fix, some workarounds too
set nocursorcolumn
set nocursorline
" set norelativenumber
syntax sync minlines=256
set lazyredraw
set re=1

" Ingore stuff
set wildignore+=*/tmp/*,*.so,*.swp,*.zip     " MacOSX/Linux
set wildignore+=*\\tmp\\*,*.swp,*.zip,*.exe  " Windows
set wildignore+=*.class,*.iml,*.jpg,*.gif,*.png,*.pdf  " non text

" No python
" let g:loaded_python_provider = 1
map <silent> <c-p> :FZF<cr>
let g:fzf_action = {
			\ 'ctrl-m': 'tabedit',
			\ 'ctrl-o': 'e',
			\ 'ctrl-t': 'tabedit',
			\ 'ctrl-h':  'botright split',
			\ 'ctrl-v':  'vertical botright split' }

" \ 'alt-j':  'botright split',
" \ 'alt-k':  'topleft split',
" \ 'alt-h':  'vertical topleft split',
" \ 'alt-l':  'vertical botright split' }

"neocomplete start up
" let g:acp_enableAtStartup = 0
" let g:neocomplete#enable_at_startup = 1
" "neocoplete tab to complete
" inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
"
" " neocompleteCache
" let g:neocomplcache_enable_at_startup = 1

"NERD Tree
map <C-t> :NERDTreeToggle<CR>
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif

" use single line comments in java/c
let g:tcommentLineC = {
			\ 'commentstring': '// %s',
			\ 'replacements': g:tcomment#replacements_c
			\ }
call tcomment#type#Define('c', g:tcommentLineC)
call tcomment#type#Define('java', g:tcommentLineC)

" AUTOCOMPLETION
"
set tags=~/.vimtags
" Dont make function options show up!
set completeopt-=preview

" set leader to unbound key
let mapleader=","

" Moving tabs
nnoremap <silent> <A-h> :tabprevious<CR>
nnoremap <silent> <A-l> :tabnext<CR>

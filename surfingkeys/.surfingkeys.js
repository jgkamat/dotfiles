// Copyright (C) 2020  Jay Kamat <jaygkamat@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// modes
map(';i', 'i');
// unmap('i');
// mapkey('i', '#0enter PassThrough mode to temporarily suppress SurfingKeys', function() {
// 	Normal.passThrough();
// });
map('i', '<Alt-i>');

// Tab navigation
map('H', 'E');
map('L', 'R');
map('J', 'S');
map('K', 'D');
map('<Alt-o>', '<Ctrl-6>')
map('<Ctrl-o>', 'B')
map('<Ctrl-i>', 'F')
map('u', 'X');
map('<Alt-h>', '<<');
map('<Alt-l>', '>>');

// clipboard
map('p', 'cc');

unmap('R');
mapkey('R', '#4Reload the page', function() {
    RUNTIME("reloadTab", { nocache: true });
});
map('b', 'T');

map('c', 'yt');

// omnibar triggers
map('o', 'go');

// omnibar mappings
cmap('<Ctrl-n>', '<Tab>');
cmap('<Ctrl-p>', '<Shift-Tab>');
cmap('<Alt-n>', '<Tab>');
cmap('<Alt-p>', '<Shift-Tab>');

// Nice things
map('<Ctrl-p>', '<Alt-p>');
map('<Ctrl-d>', 'd');
map('<Ctrl-u>', 'e');

map('go', ';U');

map('+', 'zi');
map('_', 'zo');
map('0', 'zr');

// search
map('<Ctrl-/>', '/');

// unmaps
unmap('X');
unmap('T');
unmap('d');
unmap('e');
unmap('gh');

// Jumps
function mapJumpToTab(key, needle) {
	mapkey(key, '#14Quick jump to ' + needle, function() {
		RUNTIME('getTabs', {
			queryInfo: {},
			query: needle,
		}, function(response) {
			if (response.tabs.length) {
				RUNTIME.repeats = response.tabs[0].index + 1;
				RUNTIME("focusTabByIndex");
			}
		});
	});
}
mapJumpToTab('<Alt-d>', 'discord.com')
mapJumpToTab('<Alt-e>', 'messenger')
mapJumpToTab('<Alt-t>', 'tasks')
mapJumpToTab('<Alt-w>', 'workplace.com/chat')
mapJumpToTab('<Alt-c>', 'calendar')

// hints
map('F', 'af');
map(';s', ';fs');
map(';y', 'ya');

// Per domain bindings
function getDomPath(el) {
  var stack = [];
  while ( el.parentNode != null ) {
    var sibCount = 0;
    var sibIndex = 0;
    for ( var i = 0; i < el.parentNode.childNodes.length; i++ ) {
      var sib = el.parentNode.childNodes[i];
      if ( sib.nodeName == el.nodeName ) {
        if ( sib === el ) {
          sibIndex = sibCount;
        }
        sibCount++;
      }
    }
    if ( el.hasAttribute('id') && el.id != '' ) {
      stack.unshift(el.nodeName.toLowerCase() + '#' + el.id);
    } else if ( sibCount > 1 ) {
      stack.unshift(el.nodeName.toLowerCase() + ':eq(' + sibIndex + ')');
    } else {
      stack.unshift(el.nodeName.toLowerCase());
    }
    el = el.parentNode;
  }

  return stack.slice(1); // removes the html element
}

function fakeKey(name, code, alt=false, shift=false) {
	const event = new KeyboardEvent(
		'keydown', {
			code: name,
			key: name,
			keyCode: code,
			altKey: alt, shiftKey: shift,
			bubbles: true,
			path: getDomPath(document.activeElement),
			target: document.activeElement,
			srcElement: document.activeElement,
		});
	// console.log(event);
	// event.target = document.activeElement;
	// console.log(event.target);
	// console.log(document.activeElement);
	document.dispatchEvent(event);
}


unmap('<Ctrl-j>');
mapkey('<Ctrl-j>', '#2Scroll a line down', function() {
	const oldScroll = runtime.conf.scrollStepSize;
	runtime.conf.scrollStepSize = 50;
	Normal.scroll("down");
	runtime.conf.scrollStepSize = oldScroll;
});

mapkey('<Ctrl-k>', '#2Scroll a line down', function() {
	const oldScroll = runtime.conf.scrollStepSize;
	runtime.conf.scrollStepSize = 50;
	Normal.scroll("up");
	runtime.conf.scrollStepSize = oldScroll;
});

// Window commands
map('gw', 'W');

// Discord
mapkey('<Ctrl-j>', '#13Discord Chat Down', function() {
	fakeKey("ArrowDown", 40, true);
}, {domain: /discord\.com/i});
mapkey('<Ctrl-k>', '#13Discord Chat Up', function() {
	fakeKey("ArrowUp", 38, true);
}, {domain: /discord\.com/i});
mapkey('<Alt-a>', '#13Discord Chat Next', function() {
	fakeKey("ArrowDown", 40, true, true);
}, {domain: /discord\.com/i});
imapkey('<Ctrl-j>', '#13Discord Chat Down', function() {
	fakeKey("ArrowDown", 40, true);
}, {domain: /discord\.com/i});
imapkey('<Ctrl-k>', '#13Discord Chat Up', function() {
	fakeKey("ArrowUp", 38, true);
}, {domain: /discord\.com/i});

// Messenger
mapkey('<Ctrl-j>', '#13Chat Down', function() {
	fakeKey("ArrowDown", 40, true);
}, {domain: /messenger\.com/i});
mapkey('<Ctrl-k>', '#13Chat Up', function() {
	fakeKey("ArrowUp", 38, true);
}, {domain: /messenger\.com/i});

imapkey('<Ctrl-j>', '#13Chat Down', function() {
	fakeKey("ArrowDown", 40, true);
}, {domain: /messenger\.com/i});
imapkey('<Ctrl-k>', '#13Chat Up', function() {
	fakeKey("ArrowUp", 38, true);
}, {domain: /messenger\.com/i});



// Overrides of base keys
unmap('af');
unmap('ab');
mapkey('a', '#8Open a bookmark', function() {
    Front.openOmnibar(({type: "Bookmarks", tabbed: false}));
});
mapkey('A', '#8Open a bookmark', function() {
    Front.openOmnibar(({type: "Bookmarks"}));
});

// Bunny config
const PASSTHROUGH_TERM = [
	'i', 's', 'sw', 'w', 'tbgs', 'fbgs', 'fbgf', 'tbgf', 'zbgs', 'zbgf', 'abgs', 'codex', '@od',
	'@sb', '@i', 'tw', 'fbpkg', 'smc', 'alitepath', 'duf', 'wut', 'sf']
addSearchAlias('b', 'bunny', 'https://www.internalfb.com/intern/bunny/?q=');
PASSTHROUGH_TERM.forEach((pass) => addSearchAlias(pass, pass, 'https://www.internalfb.com/intern/bunny/?q=' + pass + ' '))

// spawndaemon config
mapkey('<Ctrl-s>', "#7Spawn url in mpv", function() {
	RUNTIME('request', {
		method: 'get',
		url: "file:///tmp/jay/spawndaemon-hash"
	}, function (resp) {
		const auth = resp.text;
		Clipboard.write("spawndaemon:" + auth + ":mpv:" + window.location.href);
	})
});
unmap('<Alt-s>');
mapkey('<Alt-s>', '#7Spawn a link URL in mpv', function() {
	RUNTIME('request', {
		method: 'get',
		url: "file:///tmp/jay/spawndaemon-hash"
	}, function (resp) {
		const auth = resp.text;
		Hints.create('*[href]', function(element) {
			Clipboard.write("spawndaemon:" + auth + ":mpv:" + element.href);
		});
	})
});

// Website hacks
// Kill accesskeys
document.addEventListener('DOMContentLoaded', function() {
	document.querySelectorAll("[accesskey]")
		.forEach((l) => l.removeAttribute("accesskey"))
}, false);

// Settings
settings.smoothScroll = false;
settings.scrollStepSize = 200;
settings.showModeStatus = true

settings.defaultSearchEngine = 'd';
settings.focusFirstCandidate = false;
settings.modeAfterYank = "Normal";
settings.hintAlign = "left";
settings.stealFocusOnLoad = true;
settings.enableAutoFocus = false;
settings.editableBodyCare = false;
settings.focusAfterClosed = "last";
// TODO determine if we can afford to enable rich hints.
settings.richHintsForKeystroke = 0;
// settings.interceptedErrors = ["*"];
settings.startToShowEmoji = 9999999999999;
settings.tabsThreshold = 0;
settings.omnibarSuggestionTimeout = 100;
settings.hintExplicit = true;

Hints.style(
	`
	font-family: Monoid;
	font-weight: Bold;
	font-size: 9pt;
	`);
// set theme
settings.theme = `
.sk_theme {
    font-family: Input Sans Condensed, Charcoal, sans-serif;
    font-size: 10pt;
    background: #24272e;
    color: #abb2bf;
}
.sk_theme tbody {
    color: #fff;
}
.sk_theme input {
    color: #d0d0d0;
}
.sk_theme .url {
    color: #61afef;
}
.sk_theme .annotation {
    color: #56b6c2;
}
.sk_theme .omnibar_highlight {
    color: #528bff;
}
.sk_theme .omnibar_timestamp {
    color: #e5c07b;
}
.sk_theme .omnibar_visitcount {
    color: #98c379;
}
.sk_theme #sk_omnibarSearchResult ul li:nth-child(odd) {
    background: #303030;
}
.sk_theme #sk_omnibarSearchResult ul li.focused {
    background: #3e4452;
}
#sk_status, #sk_find {
    font-size: 20pt;
}`;
// click `Save` button to make above settings to take effect.

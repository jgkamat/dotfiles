

;;; Code:

(with-eval-after-load 'gnus
  (setq user-mail-address "jaygkamat@gmail.com"
		user-full-name "Jay Kamat")

  (setq gnus-select-method
		'(nnimap "gmail"
				 (nnimap-address "imap.gmail.com")  ; it could also be imap.googlemail.com if that's your server.
				 (nnimap-server-port "imaps")
				 (nnimap-stream ssl)))

  (setq smtpmail-smtp-server "smtp.gmail.com"
		smtpmail-smtp-service 587
		gnus-ignored-newsgroups "^to\\.\\|^[0-9. ]+\\( \\|$\\)\\|^[\"]\"[#'()]")

  (setq
   gnus-build-sparse-threads 'more
   gnus-fetch-old-headers t
   gnus-fetch-old-ephemeral-headers t

   gnus-use-header-prefetch t

   gnus-inhibit-images t

   mml-secure-openpgp-encrypt-to-self t
   mml-secure-smime-encrypt-to-self t

   ;; Don't use non-monospaced fonts for html emails
   ;; shr-use-fonts nil
   ))
(provide '.gnus)

;;; .gnus ends here

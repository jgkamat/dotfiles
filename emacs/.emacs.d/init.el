;;; init.el --- My personal Emacs Config File -*- lexical-binding: t no-byte-compile: t -*-

;; Jay's Emacs Config.

;;; Commentary:
;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(require 'cl-lib)


;;; Code:

;; Make gc better
(setq gc-cons-percentage 0.35
      ;; Emacs default
      gc-cons-threshold 800000)
;; Use doom-emacs's reccomendation
;; (setq gc-cons-threshold 16777216
;;       gc-cons-percentage 0.1)

(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")
(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))
(add-to-list 'load-path (expand-file-name "matrix" user-emacs-directory))
(add-to-list 'load-path (expand-file-name "lisp/secrets" user-emacs-directory))

(defconst jay/site-lisp
  (cond
   ((equal system-type 'gnu/linux) "/usr/share/emacs/site-lisp/")
   ((equal system-type 'darwin) (concat "/usr/local/Cellar/emacs/"
                                        (number-to-string emacs-major-version) "."
                                        (number-to-string emacs-minor-version)
                                        "/share/emacs/site-lisp/"))
   (t (concat "/usr/local/emacs/site-lisp/"))))

(dolist (child-dir '("mu4e" "elpa-src/notmuch-0.23.7"))
  (let ((dir (expand-file-name child-dir jay/site-lisp)))
    (when (file-exists-p dir)
      (add-to-list 'load-path dir))))


(add-to-list 'exec-path "/usr/local/bin")

;; themes
(load-theme 'solarized t)
(set-frame-parameter nil 'background-mode 'dark)
(set-terminal-parameter nil 'background-mode 'dark)
(enable-theme 'solarized)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ac-etags-requires 1)
 '(package-selected-packages nil)
 '(send-mail-function 'smtpmail-send-it)
 '(tls-checktrust t)
 '(warning-suppress-types '(((package reinitialization)))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(circe-highlight-nick-face ((t (:foreground "orange" :weight bold))))
 '(circe-my-message-face ((t (:foreground "dodger blue"))))
 '(circe-originator-face ((t (:foreground "#e05f27" :weight bold))))
 '(circe-prompt-face ((t (:weight bold))))
 '(circe-server-face ((t (:inherit font-lock-comment-face))))
 '(comint-highlight-prompt ((t (:inherit nil))))
 '(erc-input-face ((t (:foreground "dodger blue"))))
 '(erc-my-nick-face ((t (:foreground "DarkOrange1" :weight bold))))
 '(erc-prompt-face ((t (:weight bold))))
 '(evil-visual-mark-face ((t (:background "gray20" :foreground "white" :slant italic))))
 '(fic-face ((t (:background "background" :foreground "red" :weight bold))))
 '(flycheck-warning ((t (:underline (:color "grey30" :style wave)))))
 '(ivy-virtual ((t (:inherit font-lock-builtin-face :slant italic))))
 '(line-number-current-line ((t (:inherit line-number :foreground "orange" :weight bold))))
 '(lui-button-face ((t (:underline t))))
 '(lui-highlight-face ((t (:foreground "orange" :weight bold))))
 '(lui-track-bar ((t (:inherit default :background "#5e6263" :height 0.1 :extend t))))
 '(notmuch-search-flagged-face ((t (:foreground "#2F7BDE"))))
 '(notmuch-tag-flagged ((t (:foreground "#2F7BDE"))))
 '(popup-tip-face ((t (:background "#5e6263" :foreground "black"))))
 '(relative-line-numbers-current-line ((t (:inherit relative-line-numbers :foreground "gold" :weight bold))))
 '(sh-heredoc ((t (:foreground "yellow1"))))
 '(slack-message-output-text ((t (:weight normal))))
 '(tabbar-default ((t (:inherit variable-pitch :background "gray20" :foreground "gray20" :box (:line-width 1 :color "gray20") :height 80))))
 '(tabbar-modified ((t (:inherit tabbar-default :background "gray75" :foreground "gray30" :box (:line-width 5 :color "gray30") :weight bold))))
 '(tabbar-selected ((t (:inherit tabbar-default :background "gray75" :foreground "black" :box (:line-width 5 :color "black") :weight bold))))
 '(tabbar-selected-modified ((t (:inherit tabbar-default :background "grey75" :foreground "black" :box (:line-width 5 :color "black") :weight bold))))
 '(tabbar-unselected ((t (:inherit tabbar-default :background "gray75" :foreground "gray20" :box (:line-width 5 :color "gray20") :weight bold))))
 '(tuareg-font-lock-governing-face ((t (:foreground "#15968D" :weight bold))))
 '(tuareg-font-lock-operator-face ((t (:foreground "#af8700"))))
 '(variable-pitch ((t (:height 120 :family "Sans Serif")))))


(require 'config)
(require 'work-config)
(require 'my-packages)
(require 'compile-tweaks)

(setq package-selected-packages nil)

(provide 'init)

;;; init.el ends here

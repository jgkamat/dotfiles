;; keybindings-tweak.el --- My keybinds  -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

;; General
(require 'jay-constants)

(require 'general)

(keyboard-translate ?\C-q ?\C-h)
(keyboard-translate ?\C-h ?\C-q) ; Remap C-h -> C-q and vice versa.

;; Keybinds for changing font size (zooming)
;; C-x C-+/-
(defadvice text-scale-increase (around all-buffers (arg) activate)
  (dolist (buffer (buffer-list))
    (with-current-buffer buffer
      ad-do-it)))

;; Only M-RET in prog modes (not org)
(general-define-key
 :states '(insert)
 :keymaps '(prog-mode-map)
 "M-RET" #'indent-new-comment-line)

(defun insert-comma ()
  "Insert a comma."
  (interactive)
  (insert-char ?,))

;; (general-define-key
;;  :prefix my-leader-1
;;  :states '(motion normal)
;;  "q" #'quit-window
;;  "," #'insert-comma)

;; kill compile
(define-key my-keys-minor-mode-map (kbd "C-c k") 'kill-compilation)

(defun toggle-maximize-buffer ()
  "Maximize buffer"
  (interactive)
  (if (= 1 (length (window-list)))
      (jump-to-register '_)
    (progn
      (window-configuration-to-register '_)
      (delete-other-windows))))
(define-key my-keys-minor-mode-map (kbd "C-c z") #'toggle-maximize-buffer)

;; Splitting
(general-define-key
 :keymaps '(my-keys-minor-mode-map)
 :states '(normal insert)
 "C-x \\" 'split-window-right
 "C-x -" 'split-window-below
 "C-x =" 'delete-window)

(defun rename-file-and-buffer (new-name)
  "Renames both current buffer and file it's visiting to NEW-NAME."
  (interactive "FNew name: ")
  (let ((name (buffer-name))
        (filename (buffer-file-name)))
    (if (not filename)
        (message "Buffer '%s' is not visiting a file!" name)
      (if (get-buffer new-name)
          (message "A buffer named '%s' already exists!" new-name)
        (progn
          (rename-file filename new-name 1)
          (rename-buffer new-name)
          (set-visited-file-name new-name)
          (set-buffer-modified-p nil))))))
;; Rename
(general-define-key
 :keymaps '(my-keys-minor-mode-map)
 "C-x C-r" 'rename-file-and-buffer
 ;; Bind C-: to M-:
 "C-:" #'eval-expression)

;; Undo killed files
(defvar killed-file-list nil
  "List of recently killed files.")
(defun add-file-to-killed-file-list ()
  "If buffer is associated with a file name, add that file to the `killed-file-list' when killing the buffer."
  (when buffer-file-name
    (push buffer-file-name killed-file-list)))
(add-hook 'kill-buffer-hook #'add-file-to-killed-file-list)

;; (defun reopen-killed-file ()
;;   "Reopen the most recently killed file, if one exists."
;;   (interactive)
;;   (when killed-file-list
;;     (find-file (pop killed-file-list))))
;; Un-kill a file
;; FIXME this causes a loop for some reason
;; diff-no-select("/dev/shm/#!home!jay!schedules!todo.org#.~29ba846426ebb2d86b29c27efd172e8c043d5746~" #<buffer todo.org> "-U 0 --strip-trailing-cr" noasync #<buffer  *diff-hl-diff*>)
;; diff-hl-flydiff-buffer-with-head("/home/jay/schedules/todo.org" Git)
;; apply(diff-hl-flydiff-buffer-with-head ("/home/jay/schedules/todo.org" Git))
;; diff-hl-changes-buffer("/home/jay/schedules/todo.org" Git)
;; diff-hl-changes()
;; diff-hl-update()
;; run-hooks(auto-revert-mode-hook auto-revert-mode-on-hook)
;; auto-revert-mode()
;; magit-turn-on-auto-revert-mode-if-desired()
;; magit-auto-revert-mode-enable-in-buffers()
;; run-hooks(after-change-major-mode-hook)
;; run-mode-hooks(diff-mode-hook)
;; diff-mode()
;; diff-no-select("/dev/shm/#!home!jay!schedules!todo.org#.~29ba846426ebb2d86b29c27efd172e8c043d5746~" #<buffer todo.org> "-U 0 --strip-trailing-cr" noasync #<buffer  *diff-hl-diff*>)

;; (define-key global-map (kbd "C-x u") 'reopen-killed-file)

;; Toggle between two buffers
(general-define-key
 :keymaps '(my-keys-minor-mode-map)
 "M-O" #'eyebrowse-last-window-config
 ;; "M-o" #'mode-line-other-buffer
 "M-o" #'projectile-project-buffers-other-buffer
 "M-i" #'projectile-ibuffer
 "M-I" #'ibuffer)

(general-define-key
 :keymaps '(my-keys-minor-mode-map)
 :states '(normal motion)
 "M-b" #'switch-to-buffer)

;; Unbind comment DWIM (similar to SUPER-;)
(global-unset-key (kbd "M-;"))
;; Unbind goto line
(global-unset-key (kbd "M-g"))
;; Unbind suspend
(global-unset-key (kbd "C-x C-z"))

;; Kill buffers no confirmation
(global-set-key [(control x) (k)] 'jay/kill-this-buffer)

;; Smart reposition
(global-set-key (kbd "M-SPC") #'reposition-window)

;; Clamp wrapping comments to 78 chars
(global-set-key
 (kbd "M-q")
 (lambda ()
   (interactive)
   (let ((fill-column (min fill-column 78)))
     (fill-paragraph))))

;; No window close
(global-unset-key (kbd "M-ESC ESC"))

;; Minibuffer (eval) history
(define-key minibuffer-local-map (kbd "C-p") (lookup-key minibuffer-local-map (kbd "M-p")))
(define-key minibuffer-local-map (kbd "C-n") (lookup-key minibuffer-local-map (kbd "M-n")))
(define-key minibuffer-local-map (kbd "C-w") #'backward-kill-word)
(define-key minibuffer-local-map (kbd "M-w") #'backward-kill-word)
(define-key minibuffer-local-map (kbd "C-u") #'backward-kill-sentence)
(define-key minibuffer-local-map (kbd "M-t") #'counsel-minibuffer-directory)
(define-key minibuffer-local-shell-command-map (kbd "C-<return>") #'exit-minibuffer)


(evil-set-initial-state 'package-menu-mode 'normal)
(evil-set-initial-state 'ibuffer-mode 'normal)

;;;; Hydras
(use-package hydra
  :defer nil
  :config
  (defhydra hydra-killer (global-map "C-x")
    "kill"
    ("k" jay/kill-this-buffer "buffer")
    ("w" eyebrowse-close-window-config "window")
    ;; ("u" reopen-killed-file "restore")
    ("p" projectile-switch-project "project")
    ("0" delete-window "frame"))

  (defhydra hydra-errorer (global-map "C-x")
    "error"
    ("`" next-error "next")))

;; Emacs lisp eval defun
(general-define-key
 :keymaps '(emacs-lisp-mode-map)
 "C-c C-c" #'eval-defun
 "C-c C-S-c" (apply-partially #'eval-defun t))

;; Proced
(general-define-key
 :states '(normal)
 :keymaps '(proced-mode-map)
 "R" 'revert-buffer)
(general-define-key
 :prefix my-leader-1
 "p" #'proced)

;; Gud

(general-define-key
 :keymaps '(gud-minor-mode-map)
 "C-c d b" #'gud-break
 "C-c d r" #'gud-remove
 "C-c d c" #'gud-cont
 "C-c d h" #'gud-stop-subjob
 "C-c d s" #'gud-step
 "C-c d S" #'gud-stepi)

(provide 'keybindings-tweak)

;;; keybindings-tweak.el ends here

;;; eshell-config.el --- My configs for eshell -*- lexical-binding: t -*-

;;; Commentary:
;; Eshell is awesome

;;; Code:

(defun jay/close-if-needed ()
  (when (not (= 1 (length (window-list))))
    (delete-window)))

(use-package eshell
  :ensure nil
  :defer t

  :init
  (defun jay/eshell-start (prefix)
    "Start eshell in current projectile root, unless given a prefix arg."
    (interactive "P")
    (let ((default-directory
            (if (and (not prefix)
                     (projectile-project-p)
                     (not (eq major-mode 'dired-mode)))
                (projectile-project-root)
              default-directory)))
      (eshell t)))

  (require 'jay-constants)
  (add-hook
   'eshell-mode-hook
   #'jay/eshell-init)
  (defun jay/eshell-init ()
    "Eshell initialization code"
    ;; (eshell-smart-initialize)
    ;; Don't use show-paren mode
    (setq-local show-paren-mode nil)

    ;; eshell special keys
    (evil-define-key
      '(normal insert)
      eshell-mode-map
      (kbd "M-j") #'eshell-next-prompt
      (kbd "M-k") #'eshell-previous-prompt
      (kbd "M-a") #'tracking-next-buffer
      (kbd "M-e") #'eshell-smart-goto-end
      (kbd "M-RET") #'eshell-smart-goto-end
      (kbd "M-p") #'eshell-previous-input
      (kbd "M-n") #'eshell-next-input
      (kbd "M-r") #'counsel-esh-history
      (kbd "M-t") #'counsel-esh-directory
      ;; ivy completion
      (kbd "<tab>") #'completion-at-point)
    (evil-define-key
      'insert
      eshell-mode-map
      (kbd "C-d") #'eshell-send-eof-to-process)
    (evil-define-key
      'normal
      eshell-mode-map
      ;; TODO this is done improperly, for example 'I' does not work.
      ;; Do somehting similar to erc.
      (kbd "0") #'eshell-bol))
  :config
  (setq eshell-history-size 10000
        ;; Starts with keywords and end properly
        eshell-prompt-regexp "\\(^[*\\[][^#$\n]*[#$]\\|>>>\\) "
        eshell-save-history-on-exit t
        eshell-scroll-to-bottom-on-output nil
        eshell-scroll-show-maximum-output nil
        eshell-hist-ignoredups t
        eshell-pushd-tohome t)

  (defun jay/wrap-no-highlight (fun &rest args)
    (let ((eshell-highlight-prompt nil))
      (apply fun args)))
  ;; Allow subshells to work with eshell-next-prompt even if they aren't
  ;; created by eshell specifically
  (advice-add 'eshell-next-prompt :around #'jay/wrap-no-highlight)

  ;; Add back history expansion in newer versions of emacs
  (add-hook 'eshell-expand-input-functions
            #'eshell-expand-history-references)

  (setq
   eshell-prompt-function
   (lambda ()
     (concat
      "*"
      (propertize "[" 'face 'default)
      (propertize
       (let ((user (eshell/whoami)))
         (if (equal user "jay") "j" user))
       'face 'warning)
      (propertize "@" 'face 'default)
      (propertize (system-name) 'face `(:foreground "#598249"))
      " "
      (propertize
       (eshell/basename (abbreviate-file-name (eshell/pwd)))
       'face `(:foreground "#E05F27" :weight bold))
      (propertize "]" 'face 'default)
      (if (or
           (equal (eshell/whoami) "root")
           (= (user-uid) 0))
          (propertize "# " 'face 'warning)
        (propertize "$ " 'face `(:foreground "#598249"))))))

  ;; Add regex matching '[su]:'
  (setq eshell-password-prompt-regexp
        (format "\\(%s\\).*:\\s *\\'"
                (regexp-opt
                 (append '("[su]")
                         password-word-equivalents))))

  (setq
   ;; This affects tramp too!
   password-cache t
   password-cache-expiry 3600)

  ;; Plan 9 smart shell
  (require 'em-smart)
  (setq eshell-where-to-jump 'begin
        eshell-review-quick-commands nil
        eshell-smart-space-goes-to-end t)

  ;; Conserve memory
  (setq eshell-buffer-maximum-lines 20000)


  ;; Better truncation: https://emacs.stackexchange.com/questions/10846/how-to-improve-the-performance-of-eshell-buffer-truncation
  (defun jay/truncate-eshell-buffers ()
    "Truncate all eshell buffers."
    (interactive)
    (save-current-buffer
      (dolist (buffer (buffer-list t))
        (set-buffer buffer)
        (when (and (eq major-mode 'eshell-mode)
                   (> (line-number-at-pos (point-max)) eshell-buffer-maximum-lines))
          ;; Overtrim so we're not constantly trimming
          (let ((eshell-buffer-maximum-lines (/ eshell-buffer-maximum-lines 2)))
            (eshell-truncate-buffer))))))

  ;; After being idle for 5 seconds, truncate all the eshell-buffers if
  ;; needed. If this needs to be canceled, you can run `(cancel-timer
  ;; my/eshell-truncate-timer)'
  (defvar jay/eshell-truncate-timer nil)
  (unless jay/eshell-truncate-timer
    (setq jay/eshell-truncate-timer (run-with-idle-timer 60 t #'jay/truncate-eshell-buffers)))

  ;; Patch field-{beginning,end} to ignore fields in eshell buffers (real calculation is often slow)
  ;; This is techically breaking the api but whatever it's fast
  (defun jay/field-opti-beg (orig-f &rest args)
    (if (and (eq major-mode 'eshell-mode)
             (not args))
        1
      (apply orig-f args)))
  (defun jay/field-opti-end (orig-f &rest args)
    (if (and (eq major-mode 'eshell-mode)
             (not args))
        (point-max)
      (apply orig-f args)))
  (advice-add 'field-beginning :around #'jay/field-opti-beg)
  (advice-add 'field-end :around #'jay/field-opti-end)

  ;; Delete window on exit
  (add-hook 'eshell-exit-hook
            #'jay/close-if-needed))
(use-package esh-module
  :ensure nil
  :config
  ;; Huh?
  (require 'esh-module)
  (add-to-list 'eshell-modules-list 'eshell-tramp))

(use-package em-term
  :after eshell
  :ensure nil
  :config
  (multi-add-to-list
   'eshell-visual-commands
   "htop" "vi" "vim" "nvim" "nano"
   "sl" "LS" "w3m" "tmux" "rtv"
   "mpv" "nmtui" "systemctl" "journalctl" "ncdu")

  (setq eshell-visual-subcommands
        '(("git" "log" "l" "diff" "d" "show" "grep")
          ("g"   "log" "l" "diff" "d" "show" "grep")
          ("lab"   "log" "l" "diff" "d" "show" "grep")
          ("sudo" "wifi-menu" "vi"))))

(use-package esh-autosuggest
  :after eshell
  :hook (eshell-mode . esh-autosuggest-mode))

(use-package ansi-color
  :ensure nil
  :defer t
  :after (eshell shell)
  :config
  ;; TODO solarized theme does not work properly on ansi-term:
  (setq ansi-color-names-vector
        ["#2e2e2e" "#bd3832" "#598249" "#B68800" "#2F7BDE" "#a8366b" "#15968D" "#909396"]
        ansi-color-map (ansi-color-make-color-map)))

(use-package bash-completion
  :demand
  :config
  (bash-completion-setup))

;; (use-package fish-completion
;;   :hook ((eshell-mode . fish-completion-mode)
;;          (shell-mode . fish-completion-mode))
;;   :config
;;   (setq fish-completion-fallback-on-bash-p t))


(use-package shell
  :ensure nil
  :general
  (general-define-key
   :keymaps '(my-keys-minor-mode-map)
   :states '(normal)
   "C-c t" #'jay/shell-start)
  (general-define-key
   :prefix my-leader-1
   "t" #'jay/shell-start
   "T" (apply-partially #'jay/shell-start t))
  :init
  (defun jay/shell-start (prefix)
    "Start shell in current projectile root, unless given a prefix arg."
    (interactive "P")
    (let ((default-directory
            (if (and (not prefix)
                     (projectile-project-p)
                     (not (eq major-mode 'dired-mode)))
                (projectile-project-root)
              default-directory)))
      (shell (generate-new-buffer "*shell*"))))

  (setq comint-terminfo-terminal "eterm-color"
        shell-prompt-pattern (rx bol (or (and (or ">>>" ">" "*" "#" "$" "%"))
                                         (and (any "*[<")
                                              (0+ (not (any "#$\n")))
                                              (any "#$")))
                                 blank)
        term-prompt-regexp shell-prompt-pattern)

  (defun shell-next-prompt ()
    (interactive)
    (unless (re-search-forward term-prompt-regexp nil t)
      (comint-goto-process-mark)))
  (defun shell-prev-prompt ()
    (interactive)
    (when-let ((to-go (save-excursion
                        (forward-char -2)
                        (if (re-search-backward term-prompt-regexp nil t)
                            (progn (forward-char
                                    (length (match-string 0)))
                                   (point))
                          nil))))
      (goto-char to-go)))
  :config

  (setq comint-prompt-read-only t)

  ;; No evil spamming
  (when (fboundp 'evil-without-repeat)
    (defun jay/evil-no-repeat-wrapper (orig-fun &rest args)
      (evil-without-repeat
        (apply orig-fun args)))
    (advice-add 'comint-send-input :around #'jay/evil-no-repeat-wrapper))

  (setq comint-input-ring-size 10000)
  (defun turn-on-comint-history (history-file)
    (setq comint-input-ring-file-name history-file)
    (comint-read-input-ring 'silent))
  (add-hook 'shell-mode-hook
            (lambda ()
              (run-at-time
               "10 sec" nil
               (lambda ()
                 (turn-on-comint-history
                  (if (file-remote-p default-directory)
                      (concat (file-remote-p default-directory) "~/.zsh_history")
                    "~/.zsh_history"))))))
  (evil-define-key
    '(normal insert)
    shell-mode-map
    (kbd "M-j") #'shell-next-prompt
    (kbd "M-k") #'shell-prev-prompt
    (kbd "M-r") #'counsel-shell-history
    (kbd "M-t") #'counsel-esh-directory
    (kbd "C-u") (lambda ()
                  (interactive)
                  (beginning-of-line)
                  (kill-line))

    (kbd "C-a") #'beginning-of-line
    (kbd "C-e") #'end-of-line
    ;; ivy completion
    (kbd "<tab>") #'completion-at-point

    (kbd "M-RET") #'comint-goto-process-mark
    (kbd "C-RET") #'shell-resync-dirs
    (kbd "C-c C-k") #'comint-kill-subjob)
  (evil-define-key
    '(normal)
    shell-mode-map
    (kbd "C-u") nil)
  (evil-define-key
    'insert
    shell-mode-map

    (kbd "M-a") #'tracking-next-buffer
    (kbd "M-e") #'end-of-line
    (kbd "C-d") #'comint-send-eof)

  (defun jay/shell-sentinel (process _text)
    (when (and (process-buffer process)
               (eq 'shell-mode
                   (buffer-local-value 'major-mode
                                       (process-buffer process)))
               (eq 'exit (process-status process)))
      (kill-buffer (current-buffer))
      (jay/close-if-needed)))
  (add-hook
   'shell-mode-hook
   (lambda ()
     (advice-add
      (thread-last (current-buffer)
        (get-buffer-process)
        (process-sentinel))
      :after
      #'jay/shell-sentinel))))


(use-package term
  :ensure nil
  :defer t
  :general
  (general-define-key
   :keymaps 'term-mode-map
   :states '(normal)
   "0" #'term-bol
   "M-j" #'shell-next-prompt
   "M-k" #'shell-prev-prompt)
  :config
  ;; Kill terminal on exit
  (defadvice term-handle-exit
      (after term-kill-buffer-on-exit activate)
    (kill-buffer)
    (jay/close-if-needed))

  (defun term-pass-through (&rest keylist)
    "A small fun to simplify passing keys in KEYLIST through in term mode."
    (dolist (key keylist)
      (define-key term-raw-map (kbd key) (key-binding (kbd key)))))

  (defun jay/term-start (prefix)
    "Start shell in current projectile root, unless given a prefix arg."
    (interactive "P")
    (let ((default-directory
            (if (and (not prefix)
                     (projectile-project-p)
                     (not (eq major-mode 'dired-mode)))
                (projectile-project-root)
              default-directory)))
      (cl-letf (((symbol-function 'switch-to-buffer) #'display-buffer))
        (ansi-term
         (or (executable-find "zsh")
             (executable-find "bash"))))))

  ;; make M-x work in term
  (add-hook
   'term-mode-hook
   (lambda ()
     (define-key term-raw-map (kbd "C-r") #'term-send-raw)
     (define-key term-raw-map (kbd "C-w") #'term-send-raw)
     (define-key term-raw-map (kbd "M-x") #'execute-extended-command)
     (define-key term-raw-map (kbd "C-p") #'term-send-up)
     (define-key term-raw-map (kbd "C-n") #'term-send-down)
     (define-key term-raw-map (kbd "M-p") #'term-send-up)
     (define-key term-raw-map (kbd "M-n") #'term-send-down)

     (apply #'term-pass-through "M-:"
            ;; M-1 -> M-9
            (mapcar
             #'(lambda (x) (concat "M-" (number-to-string x)))
             (number-sequence 1 9))))))


(provide 'eshell-config)

;;; eshell-config.el ends here

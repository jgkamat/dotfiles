;;; elfeed-config.el --- Config for elfeed -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:
(require 'general)
(require 'jay-constants)

(use-package elfeed
  :defer t
  :general
  (general-define-key
   :prefix my-leader-1
   "n" #'elfeed)
  :config
  (general-define-key
   :keymaps 'elfeed-search-mode-map :states '(normal)
   "q" (lookup-key elfeed-search-mode-map (kbd "q"))
   "RET" #'elfeed-search-show-entry
   "<C-return>" #'elfeed-search-browse-url
   "s" (lookup-key elfeed-search-mode-map (kbd "s"))
   "d" (lookup-key elfeed-search-mode-map (kbd "r"))
   "u" (lookup-key elfeed-search-mode-map (kbd "u"))
   "r" (lookup-key elfeed-search-mode-map (kbd "g"))
   "R" (lookup-key elfeed-search-mode-map (kbd "G")))
  (general-define-key
   :keymaps 'elfeed-show-mode-map :states '(normal)
   "q" (lookup-key elfeed-search-mode-map (kbd "q")))


  ;; Fetch on midnight
  (add-hook 'midnight-hook #'elfeed-update)

  (setq elfeed-search-filter "@2-months-ago +unread "))

(use-package elfeed-org
  :hook (elfeed-search-mode)
  :defer t
  :after elfeed
  :config
  (elfeed-org)
  (setq rmh-elfeed-org-files (list "~/Sync/schedules/feed.org"))
  ;; Filter out garbage
  (add-hook 'elfeed-new-entry-hook
            (elfeed-make-tagger :feed-title "Escapist"
                                :entry-title '(not "Zero Punctuation")
                                :add 'junk
                                :remove 'unread)))

(provide 'elfeed-config)

;; elfeed-config.el ends here

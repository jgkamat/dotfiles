;;; jailbot.el --- For the times that humans make you want to rip your head off

;;; Commentary:
;; Automates jailing people when they say a particular phrase, defaulting to '@channel'
;;
;; This will also overwrite your slack settings, so you probably don't want to run this on your actual machine.
;;
;; You will need to populate plusplusbot-channel, plusplusbot-team-id and customize the vars below to make this work

;;; Code:
(require 'slack)
(require 'cl)

(setq
  ;; Get this via slack-current-team-id
  jailbot-team-id "T033JPZLT"
  ;; Plusplus allows subtracting or adding via the slackapi if you use username {---,+++}
  jailbot-message "This application is currently in heavy development. Please ignore anything that I do."

  jailbot-enable nil
  ;; Whitelist certain people (don't attack them)
  jailbot-whitelist '("kberzinch"  "ryanstrat" "ross" "sarahstorer"))

(defun slack-create-group-from-name (name callback)
  "Create a group non-interactively.
NAME the name to call this group
CALLBACK function to call on success"
  (slack-request
    slack-create-group-url
    (slack-team-find jailbot-team-id)
    :type "POST"
    :params (list (cons "name" name))
    :success callback
    :sync nil))

;;; Giving 'er all she's got cap'n!
(provide 'jailbot)
;;; jailbot.el ends here


;; RETIRED

(use-package slack
	:commands (slack-start)
	:init
	(setq slack-buffer-emojify t ;; if you want to enable emoji, default nil
		slack-prefer-current-team t)

	:config
	(require 'slack-secrets nil 'noerror)
	;; (require 'plusplusbot)

	(when (functionp 'slack-register-my-teams)
		(slack-register-my-teams))

	(setq slack-channel-buffer-name "*Slack*"
		slack-im-buffer-name "*Slack*"
		slack-group-buffer-name "*Slack*"
		slack-message-edit-buffer-name "*Slacke*"
		slack-message-write-buffer-name "*Slack*"

		slack-typing-visibility 'buffer

		slack-buffer-function #'switch-to-buffer)

	(setq lui-time-stamp-format "[%I:%M %p]")

	(evil-define-key 'normal slack-info-mode-map
		",u" 'slack-room-update-messages)
	(evil-define-key 'normal slack-mode-map
		",c" 'slack-buffer-kill
		",o" 'slack-select-rooms
		",t" 'slack-change-current-team
		"\C-p" 'slack-select-rooms
		",ra" 'slack-message-add-reaction
		",a" 'slack-message-add-reaction
		",rr" 'slack-message-remove-reaction
		",rs" 'slack-message-show-reaction-users
		",pl" 'slack-room-pins-list
		",pa" 'slack-message-pins-add
		",pr" 'slack-message-pins-remove
		",mm" 'slack-message-write-another-buffer
		",me" 'slack-message-edit
		",md" 'slack-message-delete
		",mt" 'slack-thread-show-or-create
		",u" 'slack-room-update-messages
		",2" 'slack-message-embed-mention
		",3" 'slack-message-embed-channel
		"\M-n" 'slack-buffer-goto-next-message
		"\M-p" 'slack-buffer-goto-prev-message

		"J" 'slack-buffer-goto-next-message
		"K" 'slack-buffer-goto-prev-message)
	(evil-define-key 'normal slack-edit-message-mode-map
		",k" 'slack-message-cancel-edit
		",s" 'slack-message-send-from-buffer
		",2" 'slack-message-embed-mention
		",3" 'slack-message-embed-channel)

	;; Add custom keybinds (with auto start)
	(defvar slack-started nil)
	(advice-add 'slack-start :before #'(lambda (&rest _) (setq slack-started t)))
	(defun slack-start-select ()
		(interactive)
		"Selects a slack steam, but starts slack if not started already"
		(when (not slack-started)
			(slack-start)
			;; This kills the man
			(sleep-for 4))
		(slack-select-rooms))

	(make-variable-buffer-local 'lui-fill-column)
	(add-hook 'window-configuration-change-hook
		'(lambda ()
			 (save-excursion
				 (walk-windows
					 (lambda (w)
						 (let ((buffer (window-buffer w)))
							 (set-buffer buffer)
							 (when (or (eq major-mode 'slack-mode) (eq major-mode 'lui-mode))
								 (setq lui-fill-column (- (window-width w) 15)))))))))

	;; Never notify for reactions please
	(defun slack-reaction-notify (&rest x))


	;; Prevent bot messages or slackbot from notifying me! =)
	(defun slack-message-filter-bots (func message room team)
		"An advice to slack-message-notify-alert to prevent bot message from yelling at us.
Blocks slackbot and normal bots. Apparently slackbot has the same permissions as a normal user."
		(unless (or
							(slack-bot-message-p message)
							(slack-message-sender-equalp message "USLACKBOT"))
			(funcall func message room team)))

	(advice-add #'slack-message-notify-alert :around #'slack-message-filter-bots)

	(defun throwaway (&rest _) )
	;; Throw away slack reaction notifications
	(advice-add #'slack-reaction-notify :around #'throwaway)


	;; I hate tracking mode
	(add-hook 'slack-mode-hook
		(lambda ()
			(tracking-mode -1)
			;; Enable flyspell mode
			(when (functionp #'flyspell-mode)
				(flyspell-mode 1))))

	:general
	(:prefix my-leader-1
		"s" #'slack-start-select
		"t" #'slack-change-current-team))

(use-package alert
	:commands (alert)
	:init
	(setq alert-default-style 'libnotify))

(provide 'slack-config)

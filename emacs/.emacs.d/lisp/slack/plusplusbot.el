;; plusplusbot.el --- For the times that slack makes you want to rip your head off

;;; Commentary:
;;
;; Automates downvoting yourself (or upvoting yourself) via emacs-slack
;;
;; You will need to populate plusplusbot-channel, plusplusbot-team-id and add a slackbot response to downvote you
;; (as slackbot has the permissions we need, while the slack api does not) to get everything working


;;; TODO don't fail completely when slack-ws is down.  Maybe fail silently.



(require 'slack)

;;; Code:

(setq
	;; Get this via (slack-message-get-room-id)
	plusplusbot-channel "G3NKY4MDW"
	;; Get this via slack-current-team-id
	plusplusbot-team-id "T033JPZLT"
	;; Plusplus allows subtracting or adding via the slackapi if you use username {---,+++}
	plusplusbot-message "jkamat ---"
	;; interval to launch. should be a number of seconds
	plusplusbot-interval 10
	;; Set this to true to launch the nukes
	plusplusbot-enable nil)

;; Stolen directly from emacs-slack's slack-message--send message, then tweaked
(defun plusplusbot-fire (&optional message)
	"This is my custom script to check if slack is up.
MESSAGE the message to send"
	(when (or (not (boundp slack-started)) (and slack-started))
		(let ((message (or message plusplusbot-message)))
			(if plusplusbot-team-id
				(let*
					((team (slack-team-find plusplusbot-team-id))
						(message
							(slack-link-channels
								(slack-link-users
									(slack-escape-message message)
									team)
								team)))
					(slack-message-inc-id team)
					(with-slots (message-id sent-message self-id) team
						(let*
							((m (list :id message-id
										:channel plusplusbot-channel
										:type "message"
										:user self-id
										:text message))
								(json (json-encode m))
								(obj (slack-message-create m team)))
							(slack-ws-send json team)
							;; This casues some warnings, but /shouldnt/ show up in automation
							(puthash message-id obj sent-message)
							)))
				(error "Call from Slack Buffer")))))

;;; Add a timer so we get nice repetition

(defvar plusplusbot-timer nil)
(unless plusplusbot-timer
	(setq plusplusbot-timer
		(run-at-time t plusplusbot-interval
			#'(lambda ()
					(when plusplusbot-enable
						(plusplusbot-fire))))))

(defun plusplusbot-pause ()
	"Stops the onslaught."
	(interactive)
	(setq plusplusbot-enable nil))

(defun plusplusbot-resume ()
	"Restart the onslaught."
	(interactive)
	(setq plusplusbot-enable t))

;;; Locked and loaded.
(provide 'plusplusbot)

(provide 'plusplusbot)
;;; plusplusbot.el ends here

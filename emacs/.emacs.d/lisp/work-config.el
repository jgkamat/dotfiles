
;; My config for work

;; Use external php mode

(require 'jay-constants)
;; (defvar master-dir (getenv "ADMIN_SCRIPTS"))
;; (let ((lp (expand-file-name  "master.emacs" master-dir)))
;;   (when (file-exists-p lp)
;;     (use-package php-mode :demand t)
;;     (load-library lp)
;;     (use-package lsp-hack :defer t
;;       :config
;;       (setq find-file-visit-truename t))
;;     (add-hook 'php-mode-hook #'lsp-hack-enable)
;;     (setq-default show-trailing-whitespace nil)))

;; (when J-WORK-SERVER
;;   (setq url-proxy-services
;;         '(("no_proxy" . "^\\(localhost\\|10.*\\)")
;;           ("http" . "fwdproxy:8080")
;;           ("https" . "fwdproxy:8080"))))


(provide 'work-config)

;; my-packages.el --- Root for external packages -*- lexical-binding: t no-byte-compile: t -*-

;; Copyright (C) 2014-2019 Jay Kamat
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;; Init:
(require 'package)
(require 'jay-constants)
(require 'seq)
(defvar +check-bad-host+ nil)
(when (and (eq emacs-major-version 26) (< emacs-minor-version 3))
  (setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3"))

;;;; Refresh:
(when (not package-archive-contents)
  (package-refresh-contents))

;;;; Early Init
(defun jay/async-install ()
  "Install async."
  (unless (require 'async nil 'noerror)
    (package-install 'async)))
(defun jay/async-initialize ()
  "Initialize async."
  ;; Better compiles and upgrades
  (require 'async)
  (async-bytecomp-package-mode 1)
  (setq async-bytecomp-allowed-packages '(all)))

(defun jay/install-unless-present (package)
  "Install PACKAGE unless it's present already. Also refresh packages if not found.
PACKAGE is a symbol"
  (unless (require package nil 'noerror)
    (unless (assoc package package-archive-contents)
      (package-refresh-contents))
    (package-install package)
    (require package)))

(defun jay/early-install ()
  "Installation of very early packages we absolutely need."
  (jay/install-unless-present 'use-package)
  (jay/install-unless-present 'general))

(jay/async-install)
(jay/async-initialize)
(jay/early-install)

;; If you would like to use package-selected-packages instead of use-package to bootstrap:
;; (if (>= emacs-major-version 25)
;;     ;; Emacs 25
;;     (package-install-selected-packages)
;;   ;; Emacs 24
;;   (dolist (p package-selected-packages)
;;     (when (not (package-installed-p p))
;;       (package-install p))))

(require 'use-package)
;;;; Package Definitions
;;;;; Use Package
(use-package use-package
  :config
  (setq use-package-always-ensure t))

;;;;; General
(use-package general
  :config
  (setq general-default-keymaps '(evil-normal-state-map))
  (general-evil-setup 1)
  (general-define-key
   :keymaps '(evil-normal-state-map evil-motion-state-map)
   ;; Free our leader key!
   my-leader-1 nil)
  (general-define-key
   :keymaps '(evil-insert-state-map)
   "C-k" nil))

;;;;; Diminish

(use-package diminish)
(require 'general)
(require 'diminish)

;;;;; Dash
;; So unfortunate...
(use-package dash
  :config
  (dash-enable-font-lock))

;;;;; Auto-Compile
(use-package auto-compile
  :demand
  :config
  (auto-compile-on-load-mode)
  (auto-compile-on-save-mode))

;;;;; Quelpa

(use-package quelpa
  :config
  (setq quelpa-self-upgrade-p nil
        quelpa-update-melpa-p nil)
  :demand)
(use-package quelpa-use-package
  :demand)

;;;;; Evil
;;;;;; Evil
(use-package evil
  :init
  ;; For evil-collection
  (setq evil-want-integration t
        evil-want-keybinding nil)
  (setq evil-want-C-u-scroll t
        ;; Better search
        evil-search-module 'evil-search
        ;; Don't make verilog mode a literal nightmare
        evil-want-abbrev-expand-on-insert-exit nil
        evil-move-beyond-eol t
        evil-undo-system 'undo-tree)
  ;; https://emacs.stackexchange.com/questions/9583/how-to-treat-underscore-as-part-of-the-word
  (defalias #'forward-evil-word #'forward-evil-symbol)
  (setq-default evil-symbol-word-search t)
  :demand
  :config
  (evil-mode 1)

  (defun recenter-noarg (&rest _)
    (recenter))
  ;; Center on searches
  (advice-add 'evil-ex-search-next :after #'recenter-noarg)
  (advice-add 'evil-ex-search-previous :after #'recenter-noarg)
  ;; Center on first search (/ or ?)
  (advice-add 'evil-ex-search-forward :after #'recenter-noarg)
  (advice-add 'evil-ex-search-backward :after #'recenter-noarg)
  ;; Center on g; g,
  (advice-add 'goto-last-change :after #'recenter-noarg)
  (advice-add 'goto-last-change-reverse :after #'recenter-noarg)
  ;; Center on ctrl-o/i
  (advice-add 'evil-jump-forward :after #'recenter-noarg)
  (advice-add 'evil-jump-backward :after #'recenter-noarg)
  ;; Clear search hl on insert mode
  (add-hook 'evil-normal-state-exit-hook #'evil-ex-nohighlight)
  ;; Clear highlight on esc
  (advice-add 'keyboard-quit :before #'evil-ex-nohighlight)

  ;; Evil esc always quits
  (defun minibuffer-keyboard-quit ()
    "Abort recursive edit.
In Delete Selection mode, if the mark is active, just deactivate it;
then it takes a second \\[keyboard-quit] to abort the minibuffer."
    (interactive)
    (if (and delete-selection-mode transient-mark-mode mark-active)
        (setq deactivate-mark  t)
      (when (get-buffer "*Completions*")
        (delete-windows-on "*Completions*"))
      (abort-recursive-edit)))
  (define-key evil-normal-state-map [escape] 'keyboard-quit)
  (define-key evil-visual-state-map [escape] 'keyboard-quit)
  (define-key minibuffer-local-map [escape] 'minibuffer-keyboard-quit)
  (define-key minibuffer-local-ns-map [escape] 'minibuffer-keyboard-quit)
  (define-key minibuffer-local-completion-map [escape] 'minibuffer-keyboard-quit)
  (define-key minibuffer-local-must-match-map [escape] 'minibuffer-keyboard-quit)
  (define-key minibuffer-local-isearch-map [escape] 'minibuffer-keyboard-quit)

  ;; Nice Half Scrolling
  ;; TODO Get scrolling working perfectly (C-d C-u returns to same line) when odd(?) number of columns
  (defun vim-scroll-half-page (direction)
    "Scrolls half page up if `direction' is non-nil, otherwise will scroll half page down."
    (let ((opos (cdr (nth 6 (posn-at-point))))
          (scroll-margin 0)
          (height (*  (/ (window-body-height) 4) 2)))
      ;; opos = original position line relative to window
      (if direction
          (previous-line height)  ;; Scroll up a page
        (next-line height))  ;; Scroll down half a page
      (recenter opos)))      ;; Restore cursor/point position

  (defun vim-scroll-half-page-down ()
    "Scrolls exactly half page down keeping cursor/point position."
    (interactive)
    (vim-scroll-half-page nil))

  (defun vim-scroll-half-page-up ()
    "Scrolls exactly half page up keeping cursor/point position."
    (interactive)
    (vim-scroll-half-page t))

  ;; Evil mode mappings
  (defun jay/yank-to-end-of-line ()
    "Yank to end of line."
    (interactive)
    (evil-yank (point) (point-at-eol)))

  ;; Center mark jumps
  (defadvice evil-goto-mark (after advice-for-evil-goto-mark activate)
    (evil-scroll-line-to-center (line-number-at-pos)))

  (defun jay/kill-this-buffer ()
    "Kill the current buffer."
    (interactive)
    (cond
     ((not (active-minibuffer-window))
      (kill-buffer (current-buffer)))
     (t
      (abort-recursive-edit))))

  (evil-ex-define-cmd "W" 'save-buffer)
  (evil-ex-define-cmd "Q" 'jay/kill-this-buffer)
  (evil-ex-define-cmd "q" 'jay/kill-this-buffer)
  (evil-ex-define-cmd "qall" 'delete-window)

  (defun save-and-delete ()
    (interactive)
    (save-buffer)
    (jay/kill-this-buffer))
  (evil-ex-define-cmd "wq" 'save-and-delete)
  (evil-ex-define-cmd "WQ" 'save-and-delete)
  (evil-ex-define-cmd "Wq" 'save-and-delete)
  (evil-ex-define-cmd "wQ" 'save-and-delete)

  ;; defining evil ex commands needs a raw define key
  (define-key evil-ex-map "e " 'counsel-find-file)
  (define-key evil-ex-map "w " 'write-file)
  :general
  ;; Make emacs use the default man
  ;; (nmap "K" #'man-follow)
  ;; Yank to EOL
  (general-define-key
   :keymaps '(evil-normal-state-map)
   "Y" #'jay/yank-to-end-of-line
   ;; Change prefix argument
   "M-y" #'universal-argument)
  (
   :keymaps 'evil-normal-state-map
   "C-d" 'vim-scroll-half-page-down
   "C-u" 'vim-scroll-half-page-up))

;;;;;; Evil Related Packages
(use-package goto-chg
  :after evil
  :config
  ;; Evil g-; and g-, span width
  (setq glc-default-span 4))

(use-package evil-commentary
  :after evil
  :config
  (evil-commentary-mode))

(use-package evil-surround
  :after evil
  :config
  (global-evil-surround-mode 1))

(use-package evil-quickscope
  :after evil
  :init
  (add-hook 'prog-mode-hook 'turn-on-evil-quickscope-mode))

(use-package evil-visual-mark-mode
  :after evil
  :config
  ;; Clear marks on deletion, run with :delm!
  (advice-add 'evil-delete-marks :after
              (lambda (&rest args)
                (evil-visual-mark-render)))
  ;; Don't hide marks when in non-normaL mode
  (defun evil-visual-mark-drop-hide (func &rest args)
    (unless evil-visual-mark-mode
      (apply func args)))
  (advice-add #'evil-visual-mark-hide :around #'evil-visual-mark-drop-hide))


(use-package evil-indent-plus
  :after evil
  :config
  (evil-indent-plus-default-bindings))

(use-package evil-replace-with-register
  :after evil
  :config
  (setq evil-replace-with-register-key (kbd "gr"))
  (evil-replace-with-register-install))

(use-package evil-numbers
  :after evil
  :general
  (:keymaps '(evil-normal-state-map evil-visual-state-map)
            "C-=" 'evil-numbers/inc-at-pt
            "C--" 'evil-numbers/dec-at-pt))
(use-package evil-extra-operator
  :defer nil
  :after evil
  :general
  (
   :prefix my-leader-1
   "e" #'evil-operator-eval
   "r" #'evil-operator-eval-replace)

  :config
  ;; Backport of https://github.com/Dewdrops/evil-extra-operator/pull/7/files
  (defcustom evil-extra-operator-eval-replace-modes-alist
    '()
    "Alist used to determine evil-operator-eval-replace's behaviour.
Each element of this alist should be of this form:

 (MAJOR-MODE EVAL-FUNC [ARGS...])

MAJOR-MODE denotes the major mode of buffer. EVAL-FUNC should be a function
with at least 2 arguments: the region beginning and the region end. ARGS will
be passed to EVAL-FUNC as its rest arguments"
    :type '(alist :key-type symbol)
    :group 'evil-extra-operator)
  (evil-define-operator evil-operator-eval-replace (beg end)
    "Evil operator for evaluating code."
    :move-point nil
    (interactive "<r>")
    (let* ((ele (assoc major-mode evil-extra-operator-eval-replace-modes-alist))
           (f-a (cdr-safe ele))
           (func (car-safe f-a))
           (args (cdr-safe f-a))
           (text (buffer-substring-no-properties beg end))
           (result (if (fboundp func)
                       (apply func beg end args)
                     (format "%s" (eval (read text))))))
      (delete-region beg end)
      (insert result))))

(use-package evil-collection
  :after evil
  :demand
  :config
  ;; Why do we need this
  (setq evil-collection-company-use-tng nil
        evil-collection-setup-minibuffer nil
        evil-collection-want-find-usages-bindings nil)
  ;; Remove modes where I have my own settings (or I dislike evil bindings)
  (setq evil-collection-mode-list (remove 'diff-mode evil-collection-mode-list))
  (evil-collection-init))

;;;;;; Evil {Smart,Clever}parens

;; evil-cleverparens tweaks
(use-package evil-cleverparens
  ;; Don't defer, as we need a couple forms
  :demand)
(use-package evil-smartparens
  :defer nil
  :general
  (general-define-key
   :keymaps '(my-keys-minor-mode-map evil-insert-state-map evil-normal-state-map)
   :states '(normal insert)
   "C-s" #'sp-split-sexp
   "C-S-s" #'sp-splice-sexp)
  (general-define-key
   :keymaps '(my-keys-minor-mode-map)
   :states '(normal insert visual)
   ;; Support slurping in non-lisps
   "M-." #'sp-slurp-hybrid-sexp
   "M-," #'sp-forward-barf-sexp)

  ;; Define sp-strict bindings even though we don't do strict
  (general-define-key
   :states '(normal)
   :keymaps '(evil-smartparens-mode-map)
   "d" #'evil-sp-delete
   "c" #'evil-sp-change
   "y" #'evil-sp-yank
   "s" #'evil-sp-substitute
   "S" #'evil-sp-change-whole-line
   ;; Actually from cleverparens, but we want it only when evil-sp is enabled
   "X" #'evil-cp-delete-char-or-splice-backwards
   "x" #'evil-cp-delete-char-or-splice)

  :config
  (add-to-list 'evil-change-commands #'evil-sp-change)
  ;; (remove-hook 'smartparens-mode-hook
  ;;              (lambda ()
  ;;                (evil-smartparens-mode 1)))
  )

;;;;; Diff Hl
(use-package diff-hl
  :after magit
  :config
  (global-diff-hl-mode 1)
  ;; TODO Seems to be infinite looping with magit auto-revert for some reason?
  (diff-hl-flydiff-mode -1)
  (add-hook 'magit-post-refresh-hook 'diff-hl-magit-post-refresh)

  ;; wip modes
  (magit-wip-after-save-mode))

;;;;; Time
(use-package time
  :config
  (display-time-mode t)
  (setq display-time-mail-string ""))

;;;;; Company
(use-package company
  :diminish (company-mode . "com")
  :config
  (make-local-variable 'company-backends)
  (add-hook 'after-init-hook 'global-company-mode)
  (setq company-global-modes
        '(not erc-mode circe-mode circe-channel-mode circe-server-mode circe-query-mode)
        company-auto-complete-chars '(?.))
  ;; company-idle-delay nil
  )
(use-package company-jedi
  :after company
  :config
  (defun jay/python-mode-hook ()
    ;; (add-to-list 'company-backends 'company-jedi)
    )
  (add-hook 'python-mode-hook 'jay/python-mode-hook)
  ;; Use python3 by default!
  (when (executable-find "python3")
    (setq jedi:environment-root "jedi-python3")
    (setq jedi:environment-virtualenv
          (append python-environment-virtualenv
                  `("--python" ,(executable-find "python3")))
          jedi:complete-on-dot t)))

;; No bothering me!
(setq
 tags-revert-without-query 1)

;;;;; Dumb Jump
(use-package dumb-jump
  :general
  (general-define-key
   :prefix my-leader-1
   ;; TODO override C-] too
   "j" #'dumb-jump-go)
  :config
  (define-key popup-menu-keymap [escape] 'keyboard-quit)
  (add-to-list 'dumb-jump-language-file-exts '(:language "c++" :ext "h.in" :agtype "cpp" :rgtype "cpp")))


;;;;; Slime
(use-package slime-company :defer t)
(use-package slime
  :commands slime
  :init
  (setq inferior-lisp-program (executable-find "sbcl"))
  (setq slime-contribs '(slime-fancy slime-asdf slime-company))
  (defun slime-compile-current ()
    "Compile current file in slime"
    (interactive)
    (slime-compile-and-load-file (buffer-file-name)))

  (defun slime-system-dwim ()
    "Smartly reloads the current system!"
    (interactive)
    (defvar sys-name nil)
    (unless sys-name
      (setq sys-name (slime-read-system-name)))
    (slime-reload-system sys-name))

  (defun slime-system-dwim-reset ()
    "Reset the last loaded syste"
    (interactive)
    (defvar sys-name nil)
    (setq sys-name nil)
    (slime-system-dwim))

  (evil-set-initial-state 'slime-repl-mode 'normal)
  :config
  (setq slime-inferior-lisp-args '("--eval" "'(push (truename \".\") asdf:*central-registry*)'")))

;;;;; Indentation and Style
(use-package aggressive-indent
  :init
  (add-hook 'emacs-lisp-mode-hook #'aggressive-indent-mode)
  (add-hook 'lisp-mode-hook #'aggressive-indent-mode))


;; Do adaptive word wrap
(use-package adaptive-wrap
  :defer t
  :config
  (setq adaptive-wrap-extra-indent 2))

;; Highlight indentation
(use-package highlight-indent-guides
  :diminish highlight-indent-guides-mode
  :config
  (setq
   highlight-indent-guides-auto-enabled t
   highlight-indent-guides-method 'character
   highlight-indent-guides-auto-odd-face-perc 2
   highlight-indent-guides-auto-even-face-perc 4
   highlight-indent-guides-auto-character-face-perc 8
   highlight-indent-guides-character ?\|
   highlight-indent-guides-responsive 'stack)

  (add-hook 'prog-mode-hook
            (lambda ()
              (unless (jay/very-large-file)
                (highlight-indent-guides-mode))))
  (add-hook 'yaml-mode-hook #'highlight-indent-guides-mode)
  (add-hook 'nxml-mode-hook #'highlight-indent-guides-mode)
  (defun jay/hig-highlighter (level responsive display)
    (if (< level 1)
        nil
      (highlight-indent-guides--highlighter-default level responsive display)))
  (setq highlight-indent-guides-highlighter-function #'jay/hig-highlighter))


;; Turn on visual-line-mode everywhere
(global-visual-line-mode +1)

;; Turn on TODO highlighting
(use-package fic-mode
  :config
  (add-hook 'prog-mode-hook 'fic-mode))

;;;;; Shackle
(use-package shackle
  :config
  (shackle-mode)

  (defun jay/shackle--smart-split-dir ()
    (if (>= (window-pixel-height)
            (window-pixel-width))
        'below
      'right))

  (defun jay/shackle-dynamic-tyling (buffer alist plist)
    (if (get-buffer-window buffer (selected-frame))
        ;; Buffer is already visible, don't split
        (shackle--display-buffer buffer alist (plist-put plist
                                                         :custom nil))
      (let
          ((frame (shackle--splittable-frame))
           (window
            (cond
             ((string= (buffer-name) "*scratch*")
              (selected-window))
             ((eq (jay/shackle--smart-split-dir) 'below)
              (split-window-below))
             (t
              (split-window-right)))))
        (prog1 (window--display-buffer
                buffer window 'window alist)
          (when window
            (setq shackle-last-window window
                  shackle-last-buffer buffer))
          (unless (cdr (assq 'inhibit-switch-frame alist))
            (window--maybe-raise-frame frame))))))

  (setq shackle-rules
        '(
          ;; (compilation-mode
          ;;  :select nil
          ;;  :popup t
          ;;  :dedicated t
          ;;  :align below)
          (help-mode
           :select t
           :popup t
           :align below)
          ;; (Man-mode
          ;;  :select t
          ;;  :same t
          ;;  :inhibit-window-quit t)
          ("\\`\\*WoMan"
           :regexp t
           :select t
           :popup t)
          (ggtags-global-mode
           :select t
           :same t
           :inhibit-window-quit t)
          ("*Flycheck error messages*"
           :select nil
           :popup t
           :align below)
          ;; Prevent eww from destroying window layout
          (eww-mode
           :select t
           :inhibit-window-quit t)
          (w3m-mode
           :select t
           :inhibit-window-quit t)
          (undo-tree-visualizer-mode
           :select nil)
          ("*rmsbolt-output*"
           :regexp t
           :select nil)
          ("\\`\\*ansi-"
           :regexp t
           :select t
           :custom jay/shackle-dynamic-tyling)
          ("\\`\\*e?shell"
           :regexp t
           :select t
           :custom jay/shackle-dynamic-tyling)
          ("\\*Async Shell Command"
           :regexp t
           :ignore t))
        shackle-default-rule nil
        shackle-default-size 0.2)

  (defun jay/shackle-dedicated (func buffer alist plist)
    "add support for :dedicated t to shackle"
    (let ((window (funcall func buffer alist plist)))
      (when (and (plist-get plist :dedicated) (window-live-p window)
                 ;; Only set :dedicated for windows that can be closed with q
                 (window-parameter window 'quit-restore))
        (set-window-dedicated-p window t))
      window))
  (advice-add 'shackle-display-buffer :around #'jay/shackle-dedicated)


  (defvar display-buffer-same-window-commands
    '(occur-mode-goto-occurrence compile-goto-error))
  (add-to-list 'display-buffer-alist
               '((lambda (&rest _)
                   (memq this-command display-buffer-same-window-commands))
                 (display-buffer-reuse-window
                  display-buffer-same-window)
                 (inhibit-same-window . nil))))

;;;;; Projectile

(use-package projectile
  :diminish projectile-mode
  :init
  (projectile-mode)
  :config
  (define-key projectile-mode-map
    (kbd "C-c p")
    'projectile-command-map)
  ;; If you experience problems with slowness in eshell, try this?
  ;; (setq projectile-enable-caching nil)
  (setq projectile-completion-system 'ivy)
  (add-to-list 'projectile-globally-ignored-file-suffixes "pyc")
  ;; Only search for git submodules, prevent's bbatsov/projectile#712
  (cl-nsubstitute ".git/" ".git" projectile-project-root-files-bottom-up
                  :test #'string=)
  (add-to-list 'projectile-project-root-files-bottom-up ".git/")

  ;; Don't ever switch to ibuffer with other-buffer
  (multi-add-to-list 'projectile-globally-ignored-modes
                     "ibuffer-mode"
                     "ggtags-global-mode"
                     "pianobar-mode"
                     "mu4e-compose-mode"
                     "mu4e-headers-mode"
                     "magit-diff-mode")

  ;; Recurse inside submodules using git, rather than projectile
  (setq projectile-git-submodule-command ""
        ;; In order to do this, we have to drop -o, which lists untracked files
        ;; This only works on git 2.11+
        ;; TODO bug: in git ls-files causes this to be broken sometimes https://github.com/git/git/commit/188dce131
        ;; https://public-inbox.org/git/20170512181950.GE98586@google.com/t/
        projectile-git-command "git ls-files -zc --exclude-standard --recurse-submodules")

  ;; I hack on qb more than anything else, so this is useful.
  (projectile-register-project-type
   'qutebrowser '("qutebrowser/")
   :compile "python3 -m qutebrowser --no-err-windows --enable-webengine-inspector -T"
   :test "tox -e py37 -- --qute-bdd-webengine"
   :run "python3 -m qutebrowser --no-err-windows --enable-webengine-inspector -T"
   :compilation-dir "."
   :test-dir "tests/"
   :test-suffix ".py")

  (defun jay/find-file ()
    "Indirection over a global find-file."
    (interactive)
    (projectile-find-file))

  (general-define-key
   :keymaps '(evil-normal-state-map)
   "C-p" #'jay/find-file)
  :general
  (general-define-key
   :prefix my-leader-1
   "J" #'projectile-find-tag
   "M-j" #'projectile-regenerate-tags))
(use-package ggtags
  :defer t
  ;; :after projectile
  :config
  (setq ggtags-mode-line-project-name nil
        ggtags-enable-navigation-keys nil
        ;; Try to figure out what the best action is here
        ;; ggtags-auto-jump-to-match nil
        )
  :general
  (general-define-key
   :states '(normal emacs)
   :keymaps 'ggtags-mode-map
   "gr" nil))

;;;;; Flyspell
;; turn on flyspell only in org mode

(use-package flyspell
  :commands (flyspell-prog-mode flyspell-mode)
  :init
  (add-hook 'git-commit-mode-hook #'turn-on-flyspell)
  (add-hook 'markdown-mode-hook #'turn-on-flyspell)
  ;; Emails
  (add-hook 'message-mode-hook #'turn-on-flyspell)
  (add-hook 'erc-mode-hook (lambda () (erc-spelling-mode 1)))
  ;; TODO evaluate and remove if this is slow
  (add-hook 'prog-mode-hook 'flyspell-prog-mode)
  (add-hook 'org-mode-hook #'turn-on-flyspell)

  ;; Use flyspell in qutebrowser popup editor
  (add-to-list 'auto-mode-alist '("qutebrowser-editor-.*\\'" . markdown-mode))
  :config
  (defun turn-on-flyspell ()
    "Force 'flyspell-mode' on using a positive arg.  For use in hooks."
    (interactive)
    (flyspell-mode 1)))

;;;;; Org Toplevel
(use-package org
  :ensure org-contrib
  :init
  ;; (setq org-completion-use-ido nil)
  :config

  (evil-set-initial-state 'org-agenda-mode 'normal))

;; Org mode stuff
(use-package ox-gfm :after org)
(use-package ox-twbs :after org)
(use-package ox-reveal :after org)
(use-package htmlize :after org)
(use-package ob-C :ensure nil :after org)

(require 'org-config)

;;;;; Modeline

(use-package smart-mode-line
  :init
  (setq
   sml/no-confirm-load-theme t
   powerline-arrow-shape 'curve
   powerline-default-separator-dir '(right . left)

   sml/theme 'respectful
   column-number-mode t
   ;; settings might cause memory issues
   sml/shorten-directory t
   sml/shorten-modes nil
   sml/name-width 20
   sml/mode-width 'full)
  (when (eq system-type 'darwin)
    ;; Emacs 29 seems to have set this on fire...
    (setq sml/extra-filler -10))
  :config
  (custom-set-faces)
  (sml/setup)
  (add-to-list 'sml/replacer-regexp-list '("^~/dotfiles/emacs/\\.emacs\\.d/" ":JED:")))

(use-package smart-mode-line-powerline-theme :defer t)

(use-package nyan-mode
  :config
  (setq nyan-bar-length 20)
  (nyan-mode 1))

;;;;; Undo Tree
(use-package undo-tree
  :init

  ;; Lower undo-limit, attempting to make gc bet
  (setq
   undo-no-redo t
   undo-limit 50000
   undo-strong-limit 80000
   undo-tree-auto-save-history nil)

  :config
  (add-hook 'undo-tree-mode-hook
            (lambda ()
              (defvar undo-tree-map nil
                "Keymap used in undo-tree-mode.")

              (let ((map (make-sparse-keymap)))
                ;; remap `undo' and `undo-only' to `undo-tree-undo'
                (define-key map [remap undo] 'undo-tree-undo)
                (define-key map [remap undo-only] 'undo-tree-undo)
                ;; bind standard undo bindings (since these match redo counterparts)
                (define-key map (kbd "C-/") 'undo-tree-undo)
                (define-key map "\C-_" 'undo-tree-undo)
                ;; redo doesn't exist normally, so define our own keybindings
                (define-key map (kbd "C-?") 'undo-tree-redo)
                (define-key map (kbd "M-_") 'undo-tree-redo)
                ;; just in case something has defined `redo'...
                (define-key map [remap redo] 'undo-tree-redo)
                ;; we use "C-x u" for the undo-tree visualizer
                (define-key map (kbd "\C-c u") 'undo-tree-visualize)
                ;; set keymap
                (setq undo-tree-map map))))

  (global-undo-tree-mode)
  :general
  (
   :keymaps '(undo-tree-map)
   "C-x u" nil)
  (
   :prefix my-leader-1
   "u" #'undo-tree-visualize))

;;;;; Smartparens
(use-package smartparens-config
  :ensure smartparens
  :config
  (defun my-create-newline-and-enter-sexp (&rest _ignored)
    "Open a new brace or bracket expression, with relevant newlines and indent."
    (newline)
    (indent-according-to-mode)
    (forward-line -1)
    (indent-according-to-mode))

  (sp-local-pair '(c-mode c++-mode java-mode go-mode php-mode rust-mode javascript-mode)
                 "{" nil :post-handlers '((my-create-newline-and-enter-sexp "RET")))
  (setq sp-cancel-autoskip-on-backward-movement nil)
  (add-hook 'prog-mode-hook #'smartparens-mode)
  ;; (add-hook 'circe-channel-mode-hook #'smartparens-mode)
  )

;;;;; Editorconfig
(use-package editorconfig
  :init
  (defvar editorconfig-mode-lighter " ec")
  :config
  (editorconfig-mode 1)
  ;; Use magit git conventions instead of git overrides
  (add-to-list 'editorconfig-exclude-regexps "COMMIT_EDITMSG")
  ;; Don't use editorconfig on messages
  (add-to-list 'editorconfig-exclude-modes 'message-mode)
  ;; TODO remove me once patch is upstream
  (defun jay/reset-lisp-offset (&rest _props)
    "Clear out `lisp-indent-offset' back to Emacs default."
    (when (or (eql major-mode 'emacs-lisp-mode)
              (eql major-mode 'lisp-mode))
      (setq lisp-indent-offset nil)))
  (add-hook 'editorconfig-custom-hooks #'jay/reset-lisp-offset))

;;;;; Eyebrowse
(use-package eyebrowse
  :init
  (eyebrowse-mode t)
  :config
  (setq
   eyebrowse-wrap-around t
   ;; Scratch buffer
   eyebrowse-new-workspace t)
  (add-to-list 'window-persistent-parameters '(window-side . writable))
  (add-to-list 'window-persistent-parameters '(window-slot . writable))

  (general-define-key
   :keymaps 'eyebrowse-mode-map
   ;; Eyebrowse config
   "M-g" (key-binding (kbd "C-c C-w <"))
   "M-;" (key-binding (kbd "C-c C-w >"))
   "C-x w" (key-binding (kbd "C-c C-w \"")))
  :general
  (general-define-key
   :keymaps 'my-keys-minor-mode-map
   "M-1" 'eyebrowse-switch-to-window-config-1
   "M-2" 'eyebrowse-switch-to-window-config-2
   "M-3" 'eyebrowse-switch-to-window-config-3
   "M-4" 'eyebrowse-switch-to-window-config-4
   "M-5" 'eyebrowse-switch-to-window-config-5
   "M-6" 'eyebrowse-switch-to-window-config-6
   "M-7" 'eyebrowse-switch-to-window-config-7
   "M-8" 'eyebrowse-switch-to-window-config-8
   "M-9" 'eyebrowse-switch-to-window-config-9))

;;;;; Magit
;; magit (must be loaded after elpy)
(use-package magit
  :commands (magit-status)
  :init
  ;; Use with-editor everywhere
  (add-hook 'shell-mode-hook  'with-editor-export-editor)
  (add-hook 'term-exec-hook   'with-editor-export-editor)
  (add-hook 'eshell-mode-hook 'with-editor-export-editor)
  :config
  ;; Magit ido
  (setq magit-completing-read-function #'ivy-completing-read)
  ;; Add magit support for ssh-ident (It has a slightly different prompt)
  ;; TODO figure out why, and report as a bug to ssh-ident
  (add-to-list 'magit-process-password-prompt-regexps
               "^\\(Enter \\)?[Pp]assphrase\\( for .*\\)?: ?$")
  ;; Transient escape
  (define-key transient-map (kbd "<escape>") 'transient-quit-one)
  :general
  (
   ;; Magit keybinds
   "C-c g c" 'magit-commit
   "C-c g d" 'magit-diff
   "C-c g k" 'magit-push-implicitly
   "C-c g j" 'magit-pull-from-upstream
   "C-c g o" 'magit-checkout
   "C-c g a" 'magit-stage-modified
   "C-c g l" 'magit-log-head
   "C-c g s" 'magit-status
   "C-c g g" 'magit-status
   ;;bisect
   "C-c g b s" 'magit-bisect-start
   "C-c g b g" 'magit-bisect-good
   "C-c g b b" 'magit-bisect-bad
   "C-c g b n" 'magit-bisect-skip
   "C-c g b r" 'magit-bisect-reset)
  (
   :prefix my-leader-1
   "g" #'magit-status
   "b" #'magit-blame-addition))

;; (use-package forge
;;   :after magit)

(defun jay/clear-url-buffers ()
  "Thanks @vermiculus :).

Clears buffers generated by url.el when making requests"
  (interactive)
  (mapcar
   #'kill-buffer
   (seq-filter
    (lambda (buffer)
      (alist-get 'url-http-method
                 (buffer-local-variables buffer)))
    (buffer-list))))

;;;;; Extra FileType Modes
;; Graphviz dot
(use-package graphviz-dot-mode
  :defer t
  :config
  (setq graphviz-dot-auto-indent-on-semi nil))

;; (use-package docker
;;   :defer t
;;   :commands (docker-mode docker-global-mode)
;;   :config
;;   (evil-set-initial-state 'docker-images-mode 'emacs)
;;   (evil-set-initial-state 'docker-containers-mode 'emacs)

;;   :general
;;   (
;;    :keymaps 'docker-mode-map
;;    ;; Docker map
;;    "C-c d i j" 'docker-pull
;;    "C-c d i k" 'docker-push
;;    "C-c d c k" 'docker-kill))

;; (use-package docker-tramp
;;   :defer t
;;   :after docker)

;; filetypes for custom modes
;; (use-package dockerfile-mode
;;   :defer t)
;; (use-package groovy-mode
;;   :defer t
;;   :init
;;   (add-to-list 'auto-mode-alist '("\\.gradle\\'" . groovy-mode)))
;; (use-package ksp-cfg-mode
;;   :defer t
;;   :init
;;   (add-to-list 'auto-mode-alist '("\\.craft\\'" . ksp-cfg-mode))
;;   (add-to-list 'auto-mode-alist '("\\.cfg\\'" . ksp-cfg-mode)))
;; (use-package conf-mode
;;   :defer t
;;   :init
;;   (add-to-list 'auto-mode-alist '("\\.unmask\\'" . conf-mode)))
(use-package css-mode
  :defer t
  :init
  (add-to-list 'auto-mode-alist '("\\.stylesheet\\'" . css-mode))
  (add-to-list 'auto-mode-alist '("\\.accept_keywords\\'" . conf-mode)))
(use-package alda-mode :defer t
  :general
  (general-mmap
    "gp" 'alda-evil-play-region))

(use-package scala-mode
  :defer t)
(use-package jsonnet-mode
  :defer t
  :init
  (add-to-list 'auto-mode-alist '("\\.TEMPLATE\\'" . jsonnet-mode)))

;; (use-package stylus-mode :defer t)
(use-package cask-mode :defer t)
(use-package protobuf-mode :defer t)
(use-package feature-mode :defer t)
(use-package cmake-mode :defer t)
(use-package markdown-mode :defer t)
;;(use-package gitconfig-mode :defer t)
(use-package pkgbuild-mode :defer t)
(use-package yaml-mode :defer t)
;; (use-package tuareg :defer t)
;; (use-package php-mode :defer t)
(use-package thrift :defer t)
(use-package bpftrace-mode :defer t)
(use-package lua-mode :defer t)

;; (use-package fennel-mode :defer t)
;; (use-package apache-mode :defer t)
;; (use-package clojure-mode :defer t)
;; (use-package rust-mode :defer t)
;; (use-package qml-mode :defer t)
;; (use-package vimrc-mode :defer t)
;; (use-package gitignore-mode :defer t)
;; (use-package arduino-mode :defer t)
;; (use-package hy-mode :defer t)
;; (use-package haskell-mode :defer t)

;; (use-package systemd :defer t)
;; (use-package go-mode :defer t)

(use-package gif-screencast :defer t
  :config
  (general-define-key
   :keymaps 'gif-screencast-mode-map
   "<f8>" #'gif-screencast-toggle-pause
   "<f9>" #'gif-screencast-stop))

;;;; LSP madness
;; (use-package lsp-mode :defer t)
;; (use-package lsp-ui
;;   :defer t
;;   :init
;;   (add-hook 'lsp-mode-hook 'lsp-ui-mode))
;; (use-package company-lsp
;;   :config
;;   (push 'company-lsp company-backends))

;; Meghanda
;; (use-package meghanada
;;   :defer t
;;   :init
;;   (add-hook 'java-mode-hook
;;             (lambda ()
;;               (when (eql system-type 'gnu/linux)
;;                 ;; meghanada-mode on
;;                 (meghanada-mode t)))))

;; Hexl maps
(use-package hexl
  :disabled
  :ensure nil
  :defer t
  :general
  (
   :prefix my-leader-1
   :keymaps 'hexl-mode-map
   "x" #'hexl-insert-hex-char
   "d" #'hexl-insert-decimal-char
   "o" #'hexl-insert-octal-char))


(use-package verilog-mode
  :disabled
  :defer t
  :ensure nil
  :init
  ;; Whyy
  (setq verilog-auto-newline nil))


;;;;; Doc View
;; Docview Config
(use-package doc-view
  :ensure nil
  :defer t
  :config

  (defun doc-view-top-file ()
    "Goes to the top of a doc view buffer"
    (interactive)
    (doc-view-first-page)
    (doc-view-previous-line-or-previous-page 100))

  (setf doc-view-continuous t)
  (setf doc-view-resolution 144)

  (evil-set-initial-state 'doc-view-mode 'emacs)

  :general
  (
   :keymaps '(doc-view-mode-map)
   "j" 'doc-view-next-line-or-next-page
   "k" 'doc-view-previous-line-or-previous-page
   "G" 'doc-view-last-page
   "g" 'doc-view-top-file
   "C-d" 'doc-view-next-page
   "C-u" 'doc-view-previous-page
   "h" 'image-backward-hscroll
   "l" 'image-forward-hscroll
   "zi" 'doc-view-enlarge
   "i" 'doc-view-enlarge
   "zo" 'doc-view-shrink
   "o" 'doc-view-shrink

   ;; Workaround (why are my keybinds not taking priority?) I guess its because docview loads later
   "C-p" nil))

;;;;; Play

(use-package zone
  :defer t
  :ensure nil
  :general
  (general-define-key
   :prefix my-leader-1
   "z" #'zone))

;;;;; Calculator
;; Calculator mode copy
(use-package calculator
  :defer t
  :config
  (evil-set-initial-state 'calculator-mode 'emacs)
  :general
  (
   :keymaps '(calculator-mode-map)
   ;; Docker map
   "y" #'calculator-copy
   "p" #'calculator-paste))

(use-package calc
  :defer t
  :ensure nil
  :general

  (general-define-key
   :prefix my-leader-1
   ;; "v" #'pop-compile
   "c" #'calc
   "C" #'quick-calc))

;;;;; Messaging
(use-package alert
  :defer t
  :config
  (setq alert-default-style
        (cond
         ((eq system-type 'darwin)
          'osx-notifier)
         (t
          'libnotify))))

;; Matrix client.el
(use-package a :defer t)
(use-package ov :defer t)

;;;;; Ace Window
(use-package ace-window
  :config
  (setq aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l))
  (setq aw-dispatch-alist
        '((?x aw-delete-window " Ace - Delete Window")
          (?m aw-swap-window " Ace - Swap Window")
          (?o aw-flip-window)
          (?p aw-flip-window)
          (?u aw-flip-window)
          (?c delete-other-windows " Ace - Maximize Window")))
  (setq aw-scope 'frame)
  :general
  (
   :keymaps '(my-keys-minor-mode-map)
   "M-u" #'ace-window))
;;;;; Open With
;; Disable openwith for conflicts with EMMS
;; (use-package openwith
;;   :init
;;   (openwith-mode 1)
;;   :config
;;   ;; Don't add openwith files to recentf
;;   (add-to-list 'recentf-exclude
;;                (openwith-make-extension-regexp
;;                 '("mpg" "mpeg" "mp3" "mp4"
;;                   "avi" "wmv" "wav" "mov" "flv"
;;                   "ogm" "ogg" "mkv" "webm")))
;;   (setq
;;    openwith-associations
;;    (list
;;     (list (openwith-make-extension-regexp
;;            '("mpg" "mpeg" "mp3" "mp4"
;;              "avi" "wmv" "wav" "mov" "flv"
;;              "ogm" "ogg" "mkv" "webm"))
;;           "mpv"
;;           '(file)))))

;;;;; Presentation Mode
(use-package presentation
  :defer t)
;;;;; Pdf Tools
(use-package pdf-tools
  :if (eq system-type 'gnu/linux)
  :defer t
  :config
  (pdf-tools-install)
  (setq-default pdf-view-display-size 'fit-page))

;;;;; Yas

(use-package yasnippet-snippets
  :demand
  :config
  (yas-reload-all)
  (add-hook 'prog-mode-hook #'yas-minor-mode))

;;;;; Dired tweaks

(use-package dired-recent
  :after dired
  :init
  (require 'dired-x)
  (dired-recent-mode 1)
  :config
  (setq dired-recent-mode-map (make-sparse-keymap)
        async-shell-command-buffer 'rename-buffer)
  ;; Hijack functionality for dired-recent-ignored-prefix to do my own filtering
  (defun jay/dired-recent-ignored-p (old-fn &rest args)
    (let ((path (cl-first args)))
      (or
       (cl-search "/.torrents/" path)
       (cl-search "/tombs/" path)
       (cl-search "/beta/" path)
       (apply old-fn args))))
  (advice-add
   'dired-recent-ignored-p
   :around
   #'jay/dired-recent-ignored-p)
  :general
  (general-define-key
   :prefix my-leader-1
   "D" #'dired-recent-open))

(use-package dired-ranger
  :general
  (general-define-key
   :states '(normal)
   :keymaps '(dired-mode-map)
   "x" #'dired-do-delete
   "y" #'dired-ranger-copy
   "d" nil
   "p" #'dired-ranger-move
   "P" #'dired-ranger-paste))

;;;;; Libraries

(use-package request
  :after dash
  :config

  (defun jay/yt-rss (channel-url name)
    "Create org-elfeed links for a channel"
    (interactive "sChannel URL: \nsPretty Name: ")
    (request
     channel-url
     :parser (lambda () (libxml-parse-html-region (point) (point-max)))
     :success
     (cl-function
      (lambda (&key data &allow-other-keys)
        (let ((rss-result nil))
          (-->
           ;; Get all rss keys
           (-tree-map-nodes
            (lambda (x)
              (pcase x (`(link
                          ((rel . "alternate")
                           (type . "application/rss+xml")
                           (title . "RSS")
                           (href . ,rss-link) . ,_))
                        rss-link)))
            (lambda (x)
              (setq rss-result
                    (pcase x (`(link
                                ((rel . "alternate")
                                 (type . "application/rss+xml")
                                 (title . "RSS")
                                 (href . ,rss-link) . ,_))
                              rss-link)))
              rss-result)
            data)
           ;; Get elt
           (identity rss-result)
           ;; Format as org
           (format "[[%s][%s]]" it name)
           ;; Insert into buffer
           (insert it))))))))

;;;;; Misc Other Packages

(use-package memory-usage :defer t)
;; (use-package wc-mode :defer t
;;   :init
;;   (setq wc-idle-wait 1)
;;   )

(use-package rainbow-mode :defer t)

;;;;; Personal Packages
(use-package imgur-rewriter
  :ensure nil
  :quelpa (imgur-rewriter :fetcher gitlab :repo "jgkamat/imgur-rewriter")
  :demand
  :config
  (defun jay/imgur-override (func external url &rest _)
    "Rewrite imgur URLs before they hit the browser."
    (imgur-rewriter-rewrite-url
     url
     (lambda (urls)
       (dolist (url (reverse urls))
         (apply func external url _)))))
  ;; Override my personal url tasker to rewrite urls before it sees it
  (advice-add #'jay/browse-url-generic :around #'jay/imgur-override))

(quelpa '(rmsbolt
          :files (:defaults "starters")
          :fetcher gitlab
          :repo "jgkamat/rmsbolt"))
(setq rmsbolt-compile-delay 2)


;; (require 'matrix-config)
;;;; Load Other Configs


(require 'keybindings-tweak)

(require 'eshell-config)

;; (require 'erc-config)
(require 'circe-config)


;; syntax checking
(require 'flycheck-config)


;; Pianobar
(require 'pianobar-config)

;; outshine
(require 'outshine-config)

;; gnus
;; (require 'gnus-config)
(require 'j-mu4e-config)
(require 'elfeed-config)

;; Pretty charts
(use-package chart
  :commands (chart-emacs-storage chart-emacs-lists))

;; internal web browser config
;; (require 'w3m-config)
(require 'eww-config)

;; Profiler
(use-package profiler
  :defer t
  :commands (profiler-start profiler-stop profiler-report))

;; Ido customization
;; (require 'ido-config)
;; (require 'helm-config-j)
(require 'ivy-config)

(require 'mpdel-config)

;;;;; Diminish
;; Exclude minor modes from mode line
;; TODO move to use-package
(diminish 'auto-complete-mode)
(diminish 'evil-commentary-mode)
(diminish 'auto-revert-mode)
(diminish 'projectile-mode)
(diminish 'smartparens-mode)
(diminish 'adaptive-wrap-mode)
(diminish 'visual-line-mode)
(diminish 'undo-tree-mode)
(diminish 'docker-mode)
(diminish 'org-indent-mode)
(diminish 'eldoc-mode)
(diminish 'compilation-in-progress)
(diminish 'auto-revert-mode)
(diminish 'magit-wip-after-save-local-mode)
(diminish 'yas-minor-mode)

(provide 'my-packages)

;;; my-packages.el ends here

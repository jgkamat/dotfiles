;;; weechat-config --- weechat configuration

;;; Commentary:
;;; Self explanatory

;;; Code:

(require 'weechat)
(require 'general)

(general-define-key
	:keymaps 'weechat-mode-map
	:states 'normal
	"\C-p" #'weechat-monitor-buffer)

(provide 'weechat-config)

;;; weechat-config.el ends here

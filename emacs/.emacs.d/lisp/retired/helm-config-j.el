;;; helm-config-j.el --- My helm configs -*- lexical-binding: t -*-

(require 'package)

(require 'recentf)
(require 'evil)
(require 'helm)
(require 'helm-command)
(require 'helm-config)

;;; Code:

(helm-mode 1)

(global-set-key (kbd "M-x") #'helm-M-x)

;; This is your old M-x.
(global-set-key (kbd "C-c M-x") 'execute-extended-command)

(defun jay/recentf-find-file ()
  "Find a recent file using Ido."
  (interactive)
  (let ((file (completing-read "Choose recent file: " recentf-list nil t)))
    (when file
      (find-file file))))

(defun jay/find-all-files ()
  "Combine recentf ido and projectile ido"
  (interactive)
  (let*
      ((my-ido-list
        (append
         ;; If in projectile project, add project files!
         (if (projectile-project-p)
             (projectile-current-project-files)
           nil)
         ;; Always add the recentf list
         recentf-list))
       (file (completing-read "--> " my-ido-list nil t)))
    (when file
      (find-file
       (if (eql (elt file 0) ?/)
           file
         (concat (projectile-project-root) file))))))

(define-key evil-normal-state-map (kbd "C-p") #'jay/find-all-files)

(setq helm-autoresize-max-height 30
      helm-autoresize-min-height 1)
(helm-autoresize-mode 1)

;; Fuzzy
(setq helm-M-x-fuzzy-match t)

;; Helm positioning
(setq helm-split-window-in-side-p t)

;; Helm exemptions
;; TODO disable helm in erc
;; (add-to-list 'helm-completing-read-handlers-alist '(erc-complete-word-at-point . nil))
;; (add-to-list 'helm-completing-read-handlers-alist '(erc-complete-word . nil))
;; (add-to-list 'helm-completing-read-handlers-alist '(completion-at-point . nil))

;; Ido show paren stuff

(provide 'helm-config-j)
;;; helm-config-j.el ends here

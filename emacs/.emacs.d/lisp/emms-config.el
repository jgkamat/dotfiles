;;; emms-config.el --- My emms config

;;; Commentary:

;;; Code:

(require 'jay-constants)

(use-package emms
  :commands (emms-smart-browse)
  :ensure emms
  :defer t
  :init
  ;; What is even going on.
  (defvar emms-playlist-mode-map (make-keymap))
  :config
  (require 'emms-setup)
  (emms-all)
  (emms-default-players)
  (defconst jay/music-dir "~/Music")
  (setq
   ;; emms-player-list (list 'emms-player-mpv)
   emms-source-file-default-directory (when (file-exists-p jay/music-dir)
                                        (expand-file-name jay/music-dir))
   emms-source-file-directory-tree-function 'emms-source-file-directory-tree-find
   emms-browser-covers 'emms-browser-cache-thumbnail
   emms-lyrics-display-on-modeline nil)
  (add-to-list 'emms-player-mpv-parameters "--no-audio-display")

  (emms-mode-line-disable)
  (emms-playing-time-disable-display)
  (require 'emms-history)
  (emms-history-load)

  :config
  (general-define-key
   :keymaps '(emms-playlist-mode-map)
   :states '(normal)
   "d" #'emms-playlist-mode-kill-entire-track
   "s" #'emms-shuffle
   "q" #'quit-window
   "M-n" #'emms-next
   "M-j" #'emms-next
   "M-p" #'emms-previous
   "M-k" #'emms-previous)

  (defconst emms-info-native--max-vorbis-comment-size (* 100 64 1024)
    "Maximum length for a single Vorbis comment field.
Technically a single Vorbis comment may have a length up to 2^32
bytes, but in practice processing must be constrained to prevent
memory exhaustion in case of garbled or malicious inputs.

This limit is used with Opus and FLAC streams as well, since
their comments have almost the same format as Vorbis.")

  (defun emms-info-native--find-stream-type (filename)
    "Deduce the stream type from FILENAME.
This is a naive implementation that relies solely on filename
extension.

Return one of symbols `vorbis', `opus', `flac', or `mp3'."
    (let ((case-fold-search t))
      (cond ((string-match ".ogg$" filename) 'opus)
            ((string-match ".opus$" filename) 'opus)
            ((string-match ".flac$" filename) 'flac)
            ((string-match ".mp3$" filename) 'mp3)
            (t nil))))
  :general
  (general-define-key
   :keymaps '(emms-browse-mode-map)
   :states '(normal)
   "q" #'quit-window)
  (general-define-key
   :prefix my-leader-1
   "M" #'emms))

(provide 'emms-config)

;;; emms-config.el ends here

;;; outshine-config.el --- Outshine configuration -*- lexical-binding: t -*-

;;; Commentary:
;; Using outshine in Emacs!

;;; Code:

(require 'general)
(require 'diminish)

(use-package outshine
  ;; this will get loaded by the scratch buffer
  :defer t
  :diminish outline-minor-mode
  :init
  ;; Enables outshine for *ALL* programming buffers
  ;; (add-hook 'prog-mode-hook 'outshine-mode)

  :general
  ;; Prevent overriding with evil
  (general-define-key
   :states '(normal)
   :keymaps 'outline-mode-map
   "zb" nil)
  (general-define-key
   :keymaps
   'outline-minor-mode-map
   :states '(normal)
   ;; Narrowing
   "zn" 'outshine-narrow-to-subtree
   "zw" 'widen
   "C-;" 'outshine-imenu
   ;; tab
   "<tab>" 'outline-cycle

   "M-j" #'outline-next-heading
   "M-k" #' outline-previous-heading)

  (:keymaps 'outline-minor-mode-map
            "M-RET" nil)

  :config
  ;; Narrowing now works within the headline rather than requiring to be on it
  (advice-add
   'outshine-narrow-to-subtree :before
   (lambda (&rest _)
     (unless (outline-on-heading-p t)
       (outline-previous-visible-heading 1))))


  (setq outshine-startup-folded-p nil))

(provide 'outshine-config)

;;; outshine-config.el ends here

;;; org-config.el --- evil keybindings for org-mode

;; This file is not part of GNU Emacs

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;;; Code:

(require 'jay-constants)
(require 'general)

(defvar j/org-prefix "~/Sync/schedules")

(use-package org
  :ensure nil
  :defer t
  :config
  (require 'org-agenda)
  (require 'org-clock)
  ;; Only show holidays I care about
  (setq holiday-islamic-holidays nil
        holiday-bahai-holidays nil
        holiday-hebrew-holidays nil

        org-agenda-restore-windows-after-quit t)
  (defun jay/org-save-all-org-buffers (&rest _)
    (org-save-all-org-buffers))
  (advice-add 'org-deadline :after #'jay/org-save-all-org-buffers)
  (advice-add 'org-agenda-quit :before #'jay/org-save-all-org-buffers)
  (advice-add 'org-agenda-Quit :before #'jay/org-save-all-org-buffers)


  (setq org-clock-x11idle-program-name "xprintidle"
        org-clock-idle-time 10
        org-clock-heading-function (lambda ()
                                     (truncate-string-to-width
                                      (replace-regexp-in-string
	                                     org-bracket-link-analytic-regexp "\\5"
	                                     (org-no-properties (org-get-heading t t t t)))
                                      10 nil nil "…")))
  ;; Org agenda
  (dolist (fname (list (expand-file-name "todo.org" j/org-prefix)
                       (expand-file-name "someday.org" j/org-prefix)
                       (expand-file-name "secrets/work.org" j/org-prefix)))
    (when (file-exists-p fname)
      (add-to-list 'org-agenda-files fname))))

(use-package evil-org
  :defer t
  :after org
  :init
  (add-hook 'org-mode-hook 'evil-org-mode) ;; only load with org-mode
  :general
  ;; normal state shortcuts
  (:states
   '(normal)
   :keymaps 'org-agenda-mode-map
   "n" #'org-agenda-later
   "p" #'org-agenda-earlier
   "q" #'org-agenda-quit
   "t" #'org-agenda-todo
   "d" #'org-agenda-deadline
   "gr" #'org-agenda-redo-all
   "s" #'orgbox-agenda-schedule
   ",A" #'org-agenda-archive-default-with-confirmation
   "M" #'org-agenda-month-view
   "W" #'org-agenda-week-view
   "D" #'org-agenda-day-view
   ",p" #'org-agenda-set-property)

  ;; Clocking commands
  (general-define-key
   "go" #'org-clock-out)
  (general-define-key
   ;; need to override, so use mode map :C
   :states 'normal
   :keymaps '(org-mode-map)
   "g" nil
   "gi" #'org-clock-in)
  (general-define-key
   :states 'normal
   :keymaps '(org-agenda-mode-map)
   "g" nil
   "gi" #'org-agenda-clock-in
   "go" #'org-agenda-clock-out)

  (:keymaps
   '(org-agenda-mode-map)
   :states '(normal)
   "RET" #'org-agenda-switch-to)

  :init
  ;; Always have org mode store link
  (global-set-key "\C-cl" 'org-store-link)

  :config
  (evil-org-set-key-theme '(navigation insert textobjects additional calendar todo))

  ;; Defer loading until after evil-org-set-key-theme
  (general-define-key
   :states '(normal)
   :prefix my-leader-1
   :keymaps 'org-mode-map
   "A" #'org-archive-subtree-default-with-confirmation
   "p" #'org-set-property
   "r" #'org-refile)
  (general-define-key
   :prefix my-leader-1
   "a" #'org-agenda
   ;; "S" #'counsel-org-agenda-headlines
   )
  (general-define-key
   :states '(normal)
   :keymaps 'org-agenda-mode-map
   :prefix my-leader-1
   "r" #'org-agenda-refile)

  (general-define-key
   :states '(normal)
   :keymaps 'org-mode-map
   "M-c" #'org-cycle-list-bullet
   "M-p" #'counsel-org-goto)

  (general-define-key
   :states '(normal motion)
   :keymaps 'org-mode-map
   "RET" #'org-return)

  (add-hook 'org-mode-hook
            (lambda ()
              (org-indent-mode 1)
              ;; Cycle bullets <S-Left>
              (local-set-key "\C-cl" 'org-store-link)
              (local-set-key "\C-ca" 'org-agenda)
              (local-set-key "\C-cr" 'org-meta-return)))

  ;; Misc org mode things

  ;; Save cursor position when exporting org documents
  (defun save-cursor-pos-function (orig-fun &rest var)
    (save-excursion (apply orig-fun var)))
  (advice-add 'org-publish :around #'save-cursor-pos-function)
  (advice-add 'org-publish-all :around #'save-cursor-pos-function)

  ;;syntax highlight code blocks
  (setq org-log-done t
        org-log-repeat nil
        org-cycle-include-plain-lists 'integrate ;; Cycle through plain lists
        org-archive-default-command #'org-archive-subtree
        org-pretty-entities t
        org-src-fontify-natively t
        org-return-follows-link t
        org-catch-invisible-edits 'show-and-error
        org-link-frame-setup
        (quote
         ((vm . vm-visit-folder)
          (vm-imap . vm-visit-imap-folder)
          (gnus . gnus)
          (file . find-file)
          (wl . wl)))

        org-agenda-custom-commands '(("n" "Agenda and all TODOs"
                                      ((agenda #1="")
                                       (alltodo #1#))))

        org-refile-use-outline-path t
        org-outline-path-complete-in-steps nil
        org-refile-targets '(("todo.org" :maxlevel . 1)
                             ("someday.org" :level . 1)
                             ("status_reports.org" :level . 2)
                             ("work.org" :maxlevel . 1))

        org-icalendar-use-deadline '(todo-due event-if-todo)
        org-icalendar-use-scheduled '(todo-due event-if-todo))

  ;; Org checkbox and cookie recursive
  (setq
   org-hierarchical-todo-statistics nil
   org-checkbox-hierarchical-statistics nil)


  ;; org babel
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((perl . t)
     (ruby . t)
     (shell . t)
     (python . t)
     (dot . t)
     (emacs-lisp . t)))
  (add-to-list 'org-src-lang-modes  '("dot" . graphviz-dot))
  (setq org-babel-python-command "python3"
        ;; to prevent indentation mishaps, set src-content to 2 and preserve to t.
        org-edit-src-content-indentation 2
        org-src-preserve-indentation nil
        org-inline-image-overlays t
        org-image-actual-width 600)

  ;; Refresh images after src block execution
  (defun my/fix-inline-images ()
    (when org-inline-image-overlays
      (org-redisplay-inline-images)))
  (add-hook 'org-babel-after-execute-hook 'my/fix-inline-images)

  ;; Dont confirm elisp code
  (defun my-org-confirm-babel-evaluate (lang body)
    "Function that filters babel confirmation.
LANG the language to filter
BODY the code body that is being run"
    (not (member lang '("emacs-lisp" "python" "dot" "shell"))))
  (setq org-confirm-babel-evaluate 'my-org-confirm-babel-evaluate)

  ;; Always use spaces in org blocks
  (add-hook 'org-src-mode-hook '(lambda () (setq indent-tabs-mode nil)))

  ;; Autogenerate IDs
  (defun eos/org-custom-id-get (&optional pom create prefix)
    "Get the CUSTOM_ID property of the entry at point-or-marker POM.
   If POM is nil, refer to the entry at point. If the entry does
   not have an CUSTOM_ID, the function returns nil. However, when
   CREATE is non nil, create a CUSTOM_ID if none is present
   already. PREFIX will be passed through to `org-id-new'. In any
   case, the CUSTOM_ID of the entry is returned."
    (interactive)
    (org-with-point-at pom
      (let ((id (org-entry-get nil "CUSTOM_ID")))
        (cond
         ((and id (stringp id) (string-match "\\S-" id))
          id)
         (create
          (setq id (org-id-new (concat prefix "h")))
          (org-entry-put pom "CUSTOM_ID" id)
          (org-id-add-location id (buffer-file-name (buffer-base-buffer)))
          id)))))
  (defun eos/org-add-ids-to-headlines-in-file ()
    "Add CUSTOM_ID properties to all headlines in the current
   file which do not already have one. Only adds ids if the
   `auto-id' option is set to `t' in the file somewhere. ie,
   #+OPTIONS: auto-id:t"
    (interactive)
    (save-excursion
      (widen)
      (goto-char (point-min))
      (when (re-search-forward "^#\\+OPTIONS:.*auto-id:t" (point-max) t)
        (org-map-entries (lambda () (eos/org-custom-id-get (point) 'create))))))

  ;; automatically add ids to saved org-mode headlines
  (add-hook 'org-mode-hook
            (lambda ()
              (add-hook
               'before-save-hook
               (lambda ()
                 (when (and (eq major-mode 'org-mode)
                            (eq buffer-read-only nil))
                   (eos/org-add-ids-to-headlines-in-file)))))))

(use-package org-capture
  :after org
  :ensure nil
  :general
  (general-define-key
   :prefix my-leader-1
   "k" #'org-capture)
  :config
  (require 'org-tempo)
  (setq org-capture-templates
        `(("c" "Contacts" entry
           (file ,(expand-file-name "secrets/contacts.org" j/org-prefix))
           "* %(org-contacts-template-name)
:PROPERTIES:
:EMAIL: %(org-contacts-template-email)
:END:")
          ("r" "Reflection" plain (file ,(expand-file-name "reflection.org" j/org-prefix))
           "*** DONE %^{Title}\nCLOSED: %U\n%?")
          ("t" "Task" entry (file+headline ,(expand-file-name "todo.org" j/org-prefix) "Tasks")
           "* PLANNING %^{Brief Description} %^g\n%?")
          ("w" "Work" entry (file+headline ,(expand-file-name "secrets/work.org" j/org-prefix) "Tasks")
           "* PLANNING %^{Brief Description} %^g\n%?")
          ;; ("s" "Someday" entry (file+headline ,(expand-file-name "someday.org" j/org-prefix) "New Tasks")
          ;;  "* PLANNING %^{Brief Description} %^g\n%?")
          ;; ("b" "Someday Work Backlog" entry (file+headline
          ;;                                    ,(expand-file-name "secrets/someday.org" j/org-prefix)
          ;;                                    "New Tasks")
          ;; "* PLANNING %^{Brief Description} %^g\n%?")
          ("s" "Status" entry (file ,(expand-file-name "secrets/status_reports.org" j/org-prefix))
           "* Jay's %<%F> Status\n- %?" :prepend t)
          ("j" "Journal" plain (file ,(expand-file-name "journal.org" j/org-prefix))
           "* %^{Title}\n%?")
          )))

(use-package org-contacts
  :ensure t
  :after org
  :config
  (setq org-contacts-files `(,(expand-file-name "secrets/contacts.org" j/org-prefix))))

(use-package zpresent :defer t)

;;; Orgbox Config
(use-package orgbox
  :after org
  :config
  (setq orgbox-start-time-of-day "10:00"))

;;; Org Notifications
(use-package org-notify
  :ensure t
  :after org
  :config

  (require 'desktop)
  ;; Save and restore org-notifiy-map in desktop file to reduce email dupes
  (add-to-list 'desktop-globals-to-save 'org-notify-map)

  (defun jay/org-notify-add (name &rest params)
    "Wrapper to prevent overriding :last-run keys."

    (let* ((existing-key (plist-get org-notify-map name)))
      ;; Clear out existing if it's the org-notify default
      (when (and (eq name 'default)
                 (equal existing-key
                        '((:time "1h" :actions -notify/window
                                 :period "2m" :duration 60))))
        (setq existing-key nil))
      (unless existing-key
        (apply 'org-notify-add name params))))


  (jay/org-notify-add 'default
                      '(:time "1h" :actions -notify/window)
                      '(:time "1d" :actions -notify/window))
  (jay/org-notify-add 'task
                      '(:time "1d" :actions -notify/window)
                      '(:time "1h" :actions -notify/window)
                      '(:time "-1s" :actions -notify/window :urgency critical)
                      '(:time "-1h" :actions -notify/window :period "20m" :urgency critical))
  ;; For LUG and other things
  (jay/org-notify-add 'meeting
                      '(:time "1h" :actions -notify/window :urgency critical)
                      '(:time "30m" :actions -notify/window :urgency critical)
                      '(:time "15m" :actions -notify/window :urgency critical)
                      '(:time "1m" :actions -notify/window :urgency critical))
  (jay/org-notify-add 'reflection
                      '(:time "-1s" :actions -notify/window :period "5m"))
  (jay/org-notify-add 'event
                      '(:time "2w" :actions -notify/window)
                      '(:time "1w" :actions -notify/window :urgency critical)
                      '(:time "1h" :actions -notify/window :urgency critical)
                      '(:time "1d" :actions -email))
  (jay/org-notify-add 'test-email
                      '(:time "10m" :actions -email))
  (jay/org-notify-add 'test-notifications
                      '(:time "10m" :period "1m" :actions -notify/window :urgency critical))

  (org-notify-start))

(use-package org-drill
  :ensure nil
  :after org
  :commands org-drill)


(provide 'org-config)
;;; org-config.el ends here



(defconst my-leader-1 ",")

;; Check to see if CI is running
(defvar LOW-POWER-MODE nil)
(defvar CI-RUNNING nil)
(defvar J-WORK (file-exists-p "~/.work"))
(defvar J-WORK-SERVER (file-exists-p "~/.work-server"))
(eval-when-compile
  (setq package-archives
        (list (if CI-RUNNING
                  '("gnu-elpa" . "http://elpa.gnu.org/packages/")
                '("gnu-elpa"   . "https://elpa.gnu.org/packages/"))
              '("melpa-stable" . "https://stable.melpa.org/packages/")
              '("melpa"        . "https://melpa.org/packages/")
              '("nongnu"          . "https://elpa.nongnu.org/nongnu/"))
        package-archive-priorities
        '(("melpa-stable" . 10)
          ("melpa"        . 5)
          ("gnu-elpa"     . 0))

        ;; TODO move these packages elsewhere
        ;; Override and use standard melpa for a few packages.
        package-pinned-packages
        '(
          (evil . "melpa")
          (goto-chg . "melpa")
          ;; Beta testing
          (alda-mode . "melpa")
          ;; bash-completion is very out of date
          (bash-completion . "melpa")
          ;; graphviz-dot-mode patch for emacs 26
          (graphviz-dot-mode . "melpa")
          ;; evil org
          (evil-org . "melpa")
          ;; circe-notifications is out of date
          (circe-notifications . "melpa")
          ;; general.el needs a newer use-package
          (use-package . "melpa")
          ;; alert.el bugfixes
          (alert . "melpa")
          ;; evil-smartparens bugfixes
          (evil-smartparens . "melpa")
          ;; circe bleeding
          (circe . "melpa")
          ;; pdf-tools never updates
          (pdf-tools . "melpa")
          ;; bleeding edge lsp
          (lsp-mode . "melpa")
          (eglot . "melpa")
          ;; bleeding flycheck
          (flycheck . "melpa")
          ;; bleeding projectile
          (projectile . "melpa")
          (typescript-mode . "melpa")
          ;; mpdel never updates
          (mpdel . "melpa")
          (tablist . "melpa")
          ;; ggtags is buggy
          (ggtags . "melpa")
          ;; Updated dumb jump
          (dumb-jump . "melpa")
          )))

(unless CI-RUNNING
  (setq load-prefer-newer t)
  (setenv "INSIDE_EMACS" "true")
  (setenv "EDITOR" "emacsclient"))

(defvar my-keys-minor-mode-map (make-keymap) "my-keys-minor-mode keymap.")
(define-minor-mode my-keys-minor-mode
  "A minor mode so that my key settings override annoying major modes."
  t " mkb" 'my-keys-minor-mode-map)
(my-keys-minor-mode 1)

;; TODO fix this sometime...
;;(when (boundp 'package-quickstart)
;;  (setq package-quickstart t))

(provide 'jay-constants)



;;; Code:

(require 'general)
(require 'jay-constants)

(use-package pianobar
  :defer t
  :commands (pianobar jay/play-or-pause)
  :config
  (defun jay/pianobar-p ()
    "Check if we should use emms or pianobar"
    (when (boundp 'pianobar-buffer)
      (comint-check-proc pianobar-buffer)))
  (defun jay/play-or-pause ()
    (interactive)
    (if (jay/pianobar-p)
        (pianobar-play-or-pause)
      (libmpdel-playback-play-pause)))
  (defun jay/music-next ()
    (interactive)
    (if (jay/pianobar-p)
        (pianobar-next-song)
      (libmpdel-playback-next)))
  (defun jay/music-prev ()
    (interactive)
    (unless (jay/pianobar-p)
      (libmpdel-playback-previous)))

  ;; Turn off modline
  (setq pianobar-enable-modeline nil
        pianobar-run-in-background t)
  ;; Try to get some secrets!
  (require 'pianobar-secrets nil 'noerror)
  :general
  (:keymaps
   '(pianobar-mode-map)
   "M-m" #'jay/play-or-pause
   "n" #'pianobar-next-song)

  ;; Misc mappings without a leader
  (:keymaps
   'eyebrowse-mode-map
   "M-m" #'jay/play-or-pause)
  (:prefix
   my-leader-1
   ;;Pianobar pause and play
   "P" #'pianobar
   "mm" #'pianobar-play-or-pause
   "mn" #'jay/music-next
   "mp" #'jay/music-prev
   ;; Kick!
   "mk" #'pianobar-sigint
   "mt" #'pianobar-shelve-current-song
   "m+" #'pianobar-love-current-song
   "m=" #'pianobar-love-current-song
   "m-" #'pianobar-ban-current-song
   "mq" #'pianobar-quit))

(provide 'pianobar-config)

;;; pianobar-config.el ends here

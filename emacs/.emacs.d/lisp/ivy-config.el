;;; ivy-config.el - My ido configs -*- lexical-binding: t -*-

;; WARNING, see ivy-config too!

;;; Code:

(require 'general)
(require 'diminish)
(require 'jay-constants)

(use-package ivy
  :diminish ivy-mode
  :config
  (ivy-mode)
  (define-key ivy-minibuffer-map [escape] 'minibuffer-keyboard-quit)

  (setq ivy-height 8
        ivy-use-virtual-buffers t)

  (general-define-key
   :keymaps '(ivy-minibuffer-map)
   "C-<return>" (lambda () (interactive) (ivy-alt-done t))
   "RET" #'ivy-alt-done
   "S-SPC" nil)

  ;; Mu4e + ivy
  (setq mu4e-completing-read-function 'ivy-completing-read))

;; for counsel-M-x history
(use-package smex
  :demand)

(use-package counsel
  :after ivy
  :defer t
  :diminish counsel-mode
  :commands (counsel-esh-history counsel-esh-directory)
  :init
  (global-set-key (kbd "M-x") #'counsel-M-x)

  :config
  (counsel-mode)
  (setq counsel-rg-base-command
        "rg -S --no-heading --line-number --color never  --ignore-file $HOME/.config/ripgrep/ignore %s ."
        counsel-file-jump-args (split-string ". -name .git -prune -o -path ./tombs -prune -o -type f -print")
        counsel-dired-jump-args (split-string ". -name .git -prune -o -path ./tombs -prune -o -type d -print"))

  ;; TODO contribute this back or advice
  (defun counsel-esh-history ()
    "Browse Eshell history."
    (interactive)
    (require 'em-hist)
    (require 'esh-mode)
    (delete-region eshell-last-output-end (point-max))
    (goto-char eshell-last-output-end)
    (counsel--browse-history eshell-history-ring))
  (defun counsel-esh-directory ()
    "Browse Eshell directories."
    (interactive)
    (counsel-require-program "find")
    (let* ((default-directory default-directory))
      (ivy-read "Find file: "
                (split-string
                 (shell-command-to-string
                  (concat find-program " * -type f -not -path '*\/.git*'"))
                 "\n" t)
                :matcher #'counsel--find-file-matcher
                :action #'ivy-completion-in-region-action
                :require-match 'confirm-after-completion
                :history 'file-name-history
                :keymap counsel-find-file-map
                :caller 'counsel-file-jump)))
  (defun counsel-minibuffer-directory ()
    "Browse Eshell directories."
    (interactive)
    (let ((enable-recursive-minibuffers t))
      (counsel-require-program "find")
      (let* ((default-directory default-directory))
        (ivy-read "Find file: "
                  (split-string
                   (shell-command-to-string
                    (concat find-program " * -type f -not -path '*\/.git*'"))
                   "\n" t)
                  :matcher #'counsel--find-file-matcher
                  :action #'insert
                  :require-match 'confirm-after-completion
                  :history 'file-name-history
                  :keymap counsel-find-file-map
                  :caller 'counsel-file-jump))))
  (defun jay/start-mpv-directory ()
    (interactive)
    (start-process "mpv" nil "mpv" default-directory))
  (ivy-set-actions
   'counsel-file-jump
   `(("d" ,(lambda (x)
             (dired (or (file-name-directory x) default-directory)))
      "open in dired")
     ("x" ,(apply-partially #'start-process "xdg-open" nil "xdg-open")
      "open with xdg")
     ("m" ,(apply-partially #'start-process "mpv" nil "mpv")
      "open with mpv")))

  (defun jay/counsel-rg-current-dir ()
    (interactive)
    (counsel-rg nil default-directory))

  :general
  (general-define-key
   :prefix my-leader-1
   "s" #'counsel-rg
   "S" #'jay/counsel-rg-current-dir)
  (general-define-key
   :keymaps '(evil-normal-state-map)
   "C-S-P" #'counsel-locate)
  (general-define-key
   :prefix my-leader-1
   :states '(normal)
   :keymaps 'dired-mode-map
   "j" #'counsel-dired-jump
   "J" #'counsel-file-jump
   "v" #'jay/start-mpv-directory)
  (general-define-key
   :states '(normal)
   :keymaps 'dired-mode-map
   "<C-return>" #'dired-do-async-shell-command)
  (general-define-key
   :prefix my-leader-1
   "d" #'counsel-dired)
  (general-define-key
   :keymaps 'ivy-minibuffer-map
   "M-RET" #'ivy-dispatching-done))

(provide 'ivy-config)

;;; ivy-config.el ends here

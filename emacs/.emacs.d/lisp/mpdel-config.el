;;; mpdel-config.el --- My emms config -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

(use-package ivy-mpdel
  :defer)

(use-package mpdel
  :commands (libmpdel-playback-play-pause mpdel-browser-open)
  :defer t
  :init
  :config
  (require 'ivy-mpdel)
  :general
  (general-define-key
   :prefix my-leader-1
   "M" #'mpdel-browser-open))

(provide 'mpdel-config)

;;; mpdel-config.el ends here

;;; package --- Jay's mu4e configuration -*- lexical-binding: t -*-

;;; Commentary:

;; Config for mu4e

;;; Code:

(require 'jay-constants)
(require 'general)

(use-package mu4e
  :if (executable-find "mu")
  :init
  (setq mu4e-update-interval 1800)
  :ensure nil
  :defer t
  :general
  (general-define-key
   :prefix my-leader-1
   "N" #'mu4e)

  :config
  (general-define-key
   :keymaps 'mu4e-compose-mode-map
   :states 'normal
   :prefix my-leader-1
   "e" #'mml-secure-message-encrypt-pgpmime
   "s" #'mml-secure-message-sign-pgpmime)

  ;; Additional Bindings

  (general-define-key
   :keymaps 'mu4e-headers-mode-map
   :states '(motion normal)
   "T" #'mu4e-headers-mark-thread
   "W" #'mu4e-headers-toggle-include-related
   "M" #'mu4e-headers-mark-for-something
   "C-d" #'vim-scroll-half-page-down
   "C-u" #'vim-scroll-half-page-up
   "d" #'mu4e-headers-mark-for-read
   "D" (lambda () (interactive)
         (mu4e-headers-mark-thread-using-markpair '(read)))
   "t" #'mu4e-headers-mark-for-trash)

  (general-define-key
   :keymaps 'mu4e-view-mode-map
   :states '(motion normal)
   "d" #'mu4e-view-mark-for-read
   "t" #'mu4e-view-mark-for-trash)

  ;; Try to avoid quit when possible
  (general-define-key
   :keymaps 'mu4e-main-mode-map
   :states '(motion normal)
   ",q" #'mu4e-quit
   "q" #'quit-window)

  (require 'message)
  ;;; smtp

  (require 'smtpmail)

  ;; encryption
  (setq mml-secure-openpgp-encrypt-to-self t
        mml-secure-smime-encrypt-to-self t)

  ;;; Contexts
  (setq mu4e-maildir "~/.mail"
        mu4e-get-mail-command "mbsync -a")
  (setq mu4e-user-mail-address-list '("jaygkamat@gmail.com" "jgkamat@fb.com")
        mu4e-contexts
        `( ,(make-mu4e-context
             :name "personal"
             :enter-func (lambda () (mu4e-message "Entering personal context"))
             :leave-func (lambda () (mu4e-message "Leaving personal context"))
             ;; we match based on the contact-fields of the message
             :match-func (lambda (msg)
                           (when msg
                             (string-match-p "^/gmail" (mu4e-message-field msg :maildir))))
             :vars '((user-mail-address      . "jaygkamat@gmail.com"  )
                     (user-full-name         . "Jay Kamat" )
                     (mu4e-maildir-shortcuts . (("/gmail/INBOX"                 . ?i)
                                                ("/gmail/[Gmail]/.Sent Mail"    . ?s)
                                                ("/gmail/Arch"                  . ?a)
                                                ("/gmail/[Gmail]/.Trash"        . ?t)
                                                ("/gmail/Reciepts"              . ?r)))
                     (mu4e-drafts-folder     . "/gmail/[Gmail]/.Drafts")
                     (mu4e-sent-folder       . "/gmail/[Gmail]/.Sent Mail")
                     (mu4e-trash-folder      . "/gmail/[Gmail]/.Trash")
                     (mu4e-refile-folder     . "/gmail/Arch")
                     (smtpmail-queue-dir     . "~/.mail/gmail/queue")
                     (smtpmail-queue-mail    . nil)
                     (smtpmail-smtp-service  . 587)
                     (smtpmail-smtp-server   . "smtp.gmail.com")))
           ,(make-mu4e-context
             :name "work"
             :enter-func (lambda () (mu4e-message "Entering work context"))
             :leave-func (lambda () (mu4e-message "Leaving work context"))
             ;; no leave-func
             ;; we match based on the maildir of the message
             ;; this matches maildir /Arkham and its sub-directories
             :match-func (lambda (msg)
                           (when msg
                             (string-match-p "^/work" (mu4e-message-field msg :maildir))))
             :vars '( (user-mail-address      . "jgkamat@fb.com"  )
                      (user-full-name         . "Jay Kamat" )
                      (mu4e-maildir-shortcuts . (("/work/INBOX"        . ?i)
                                                 ("/work/Archive"      . ?a)
                                                 ("/work/Sent Items"   . ?s)
                                                 ("/work/Deleted Items"        . ?t)))
                      (mu4e-drafts-folder     . "/work/Drafts")
                      (mu4e-sent-folder       . "/work/Sent Items")
                      (mu4e-trash-folder      . "/work/Deleted Items")
                      (mu4e-refile-folder     . "/work/Archive")
                      (smtpmail-queue-dir     . "~/.mail/work/queue")
                      (smtpmail-queue-mail    . nil)
                      (smtpmail-smtp-service  . 587)
                      (smtpmail-smtp-server   . "smtp.office365.com")))))

  (setq mu4e-headers-sort-direction 'ascending
        mu4e-change-filenames-when-moving t
        mu4e-decryption-policy t
        mu4e-attachment-dir  "~/Downloads"
        mu4e-compose-reply-ignore-address (rx (or "@noreply."
                                                  (and bol "noreply@"))))
  (setq message-confirm-send t
        message-dont-reply-to-names t)
  ;; show images
  (setq mu4e-view-show-images nil)

  ;; general emacs mail settings; used when composing e-mail
  ;; the non-mu4e-* stuff is inherited from emacs/message-mode
  (setq mail-user-agent 'mu4e-user-agent)

  (setq mu4e-sent-messages-behavior 'sent)

  (setq mu4e-compose-format-flowed t
        fill-flowed-encode-column 78
        mu4e-compose-dont-reply-to-self t)

  ;; spell check
  (add-hook 'mu4e-compose-mode-hook
            (defun my-do-compose-stuff ()
              "My settings for message composition."
              (set-fill-column 78)
              (flyspell-mode)))

  ;; Advise read-char-exclusive to escape with esc
  (defun jay/read-char-quit-esc (func &rest args)
    (let ((ans (apply func args)))
      (when (eql ans ?\C-\[)
        (keyboard-quit))
      ans))
  (advice-add 'read-char-exclusive :around #'jay/read-char-quit-esc)


  ;; (require 'org-mu4e)
  )

(provide 'j-mu4e-config)

;;; j-mu4e-config.el ends here

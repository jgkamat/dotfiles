;;; package --- Jay's gnus configuration -*- lexical-binding: t -*-

;;; Commentary:

;; Config and keybinds for evil gnus

;;; Code:

(require 'jay-constants)
(require 'general)

(use-package gnus
  :defer t
  :ensure nil
  :general
  (
   :prefix my-leader-1
   "N" #'gnus))

;; Key bindings are in gnus-sum
(use-package gnus-sum
  :defer t
  :ensure nil
  :config
  (general-define-key
   :keymaps '(gnus-group-mode-map)
   :states '(normal)
   "RET" (lookup-key gnus-group-mode-map (kbd "RET"))
   ;; Get all messages from group
   "<C-return>" (lambda ()
                  (interactive)
                  (gnus-group-select-group t))
   "q" (lookup-key gnus-group-mode-map (kbd "q"))
   "r" #'gnus-group-get-new-news
   "a" (lookup-key gnus-group-mode-map (kbd "l"))
   "A" (lookup-key gnus-group-mode-map (kbd "L"))
   "r" (lookup-key gnus-group-mode-map (kbd "g"))
   "R" #'gnus-group-get-new-news-this-group
   "m" (lookup-key gnus-group-mode-map (kbd "m"))
   "M-j" (lookup-key gnus-group-mode-map (kbd "n"))
   "M-k" (lookup-key gnus-group-mode-map (kbd "p")))

  (general-define-key
   :keymaps '(gnus-summary-mode-map)
   :states '(normal)
   "RET" (lookup-key gnus-summary-mode-map (kbd "RET"))
   "q" (lookup-key gnus-summary-mode-map (kbd "q"))
   "u" (lookup-key gnus-summary-mode-map (kbd "M-u"))
   "d" (lookup-key gnus-summary-mode-map (kbd "d"))
   "D" #'gnus-summary-catchup
   "m" (lookup-key gnus-summary-mode-map (kbd "m"))
   "M-j" 'gnus-summary-next-unread-article
   "M-k" 'gnus-summary-prev-unread-article
   "r" nil
   "ra" 'gnus-summary-wide-reply-with-original
   "ro" 'gnus-summary-reply-with-original
   "R" (lookup-key gnus-summary-mode-map (kbd "R"))
   "M" (lookup-key gnus-summary-mode-map (kbd "m"))
   "m" (lookup-key gnus-summary-mode-map (kbd "Bm"))
   "^" (lookup-key gnus-summary-mode-map (kbd "^")))

  (general-define-key
   :keymaps '(gnus-article-mode-map)
   :states '(normal)
   "RET" (lookup-key gnus-summary-mode-map (kbd "RET"))
   "q" (lookup-key gnus-summary-mode-map (kbd "q"))
   "u" (lookup-key gnus-summary-mode-map (kbd "M-u"))
   "d" (lookup-key gnus-summary-mode-map (kbd "d"))
   "m" (lookup-key gnus-summary-mode-map (kbd "Bm"))
   "M-j" 'gnus-summary-next-unread-article
   "M-k" 'gnus-summary-prev-unread-article
   "r" nil
   "ra" 'gnus-summary-wide-reply-with-original
   "ro" 'gnus-summary-reply-with-original
   "R" (lookup-key gnus-summary-mode-map (kbd "R")))

  (general-define-key
   :keymaps '(message-mode-map)
   :states '(normal)
   :prefix my-leader-1
   "e" (lambda ()
         (interactive)
         (mml-secure-message-encrypt-pgpmime))
   "s" (lambda ()
         (interactive)
         (mml-secure-message-sign-pgpmime)))

  ;; Default to evil normal state in most gnus stuff
  (evil-set-initial-state 'gnus-group-mode 'normal)
  (evil-set-initial-state 'gnus-summary-mode 'normal)
  (evil-set-initial-state 'gnus-article-mode 'normal)

  (setq gnus-message-replyencrypt t
        gnus-message-replysign t
        gnus-message-replysignencrypted t
        message-confirm-send t))


(provide 'gnus-config)

;;; gnus-config.el ends here

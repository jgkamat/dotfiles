
;;; Code:

(require 'compile)

;; (general-define-key
;;  :prefix my-leader-1
;;  "l" #'compile-dwim)

(general-define-key
 :keymaps '(compilation-mode-map)
 "g" nil)


(defun load-this-file ()
  "Loads the current buffer in emacs lisp, with no confirmation"
  (interactive)
  (message "Loading %s..." (buffer-name))
  ;; (load-file (shell-quote-argument buffer-file-name))
  (load-file (kill-new (buffer-file-name (window-buffer (minibuffer-selected-window))))))

(general-define-key
 :states '(normal)
 :keymaps '(emacs-lisp-mode-map)
 :prefix my-leader-1
 "E" #'load-this-file)

(general-define-key
 :states '(normal)
 :keymaps '(my-keys-minor-mode-map)
 :prefix my-leader-1
 "x" #'next-error
 "X" #'previous-error
 "l" #'recompile)

;; follow compile output
(setq compilation-scroll-output 'first-error)
;; (setq compilation-scroll-output t)

;; from enberg on #emacs
;; Closes compile window in 0.5 seconds if no errors
(add-to-list
 'compilation-finish-functions
 (lambda (buf str)
   (when (and
          ;; Only if quit-restore is not set
          (and (get-buffer-window buf)
               (window-parameter (get-buffer-window buf) 'quit-restore))
          (string-prefix-p "finished" str))
     ;;no errors, make the compilation window go away in a few seconds
     (progn
       (run-at-time
        "1 sec" nil (lambda (buffer)
                      (unless (get-buffer-process buffer)
                        (when (get-buffer-window buffer)
                          (quit-window nil (get-buffer-window buffer)))))
        buf)
       (message "No Compilation Errors!")))))

;; use complx always on asm files
(add-hook 'asm-mode-hook
          (lambda ()
            (when buffer-file-name
              (set (make-local-variable 'compile-command)
                   (concat "complx " (shell-quote-argument buffer-file-name))))))

(add-hook 'tex-mode-hook
          (lambda ()
            (when buffer-file-name
              (set (make-local-variable 'compile-command)
                   (concat "latex " (shell-quote-argument buffer-file-name))))))

(require 'ansi-color)
;; (defun colorize-compilation-buffer ()
;;   (toggle-read-only)
;;   (ansi-color-apply-on-region compilation-filter-start (point))
;;   (toggle-read-only))
;; (add-hook 'compilation-filter-hook 'colorize-compilation-buffer)

(provide 'compile-tweaks)

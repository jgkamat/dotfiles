;;; circe-config.el --- My circe configs -*- lexical-binding: t -*-

;; Copyright (C) 2014-2019 Jay Kamat
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(require 'general)

(use-package circe
  :defer t
  :general
  (general-define-key
   :keymaps '(my-keys-minor-mode-map)
   :states '(normal)
   ;; similar to weechat next buffer
   "M-a" #'tracking-next-buffer)
  :init
  (defun circe-network-connected-p (network)
    "Return non-nil if there's any Circe server-buffer whose
`circe-server-netwok' is NETWORK."
    (catch 'return
      (dolist (buffer (circe-server-buffers))
        (with-current-buffer buffer
          (if (string= network circe-network)
              (throw 'return t))))))
  (defun circe-maybe-connect (network)
    "Connect to NETWORK, but ask user for confirmation if it's
already been connected to."
    (interactive "sNetwork: ")
    (if (or (not (circe-network-connected-p network))
            (y-or-n-p (format "Already connected to %s, reconnect?" network)))
        (circe network)))

  (defun irc ()
    "Connect to IRC through my bouncer"
    (interactive)
    (require 'circe)
    (require 'jay-constants)
    (circe-maybe-connect "b-libera")
    (circe-maybe-connect "b-esper")
    (circe-maybe-connect "b-oftc")
    (circe-maybe-connect "b-uplink")
    (circe-maybe-connect "b-synirc")
    (if J-WORK
        (circe-maybe-connect "work")))
  (defun irc-raw ()
    "Connect to IRC raw."
    (interactive)
    (require 'circe)
    (circe-maybe-connect "Libera")
    (circe-maybe-connect "Esper"))

  (defun jay/irc-user-init ()
    (require 'circe)
    (when (and desktop-save-mode
               (not (circe-server-buffers))
               (yes-or-no-p "Jack into IRC?"))
      (irc)))
  ;; Remind to connect to IRC if we haven't already
  (add-hook 'after-init-hook
            (lambda () (run-at-time "5 min" nil #'jay/irc-user-init)))
  :config

  (setq circe-default-part-message "Bye!")

  (defun jay/fetch-password (&rest params)
    (require 'auth-source)
    (let ((match (car (apply 'auth-source-search params))))
      (if match
          (let ((secret (plist-get match :secret)))
            (if (functionp secret)
                (funcall secret)
              secret))
        (error "Password not found for %S" params))))

  (cl-defun jay/nickserv-password (server &key (bouncer-server nil))
    (if (not bouncer-server)
        (jay/fetch-password :user "jgkamat" :host server)
      (setq server (concat server "/" bouncer-server))
      (concat
       ;; Add username and pw
       "jay@" (system-name)
       (jay/fetch-password :user "jgkamat" :host server))))

  (setq circe-network-options
        `(("b-libera"
           :use-tls t
           :nick "jgkamat"
           :realname "Kay"
           :host "s.jgkamat.uk"
           :port 6697
           :user "jay"
           :server-buffer-name "libera:mechjay:{host}:{port}"
           :pass ,(lambda (x)
                    (jay/nickserv-password x :bouncer-server "libera")))
          ("b-esper"
           :use-tls t
           :nick "jgkamat"
           :realname "Kay"
           :host "s.jgkamat.uk"
           :port 6697
           :user "jay"
           :server-buffer-name "esper:mechjay:{host}:{port}"
           :pass ,(lambda (x)
                    (jay/nickserv-password x :bouncer-server "esper")))
          ("b-oftc"
           :use-tls t
           :nick "jgkamat"
           :realname "Kay"
           :host "s.jgkamat.uk"
           :port 6697
           :user "jay"
           :server-buffer-name "oftc:mechjay:{host}:{port}"
           :pass ,(lambda (x)
                    (jay/nickserv-password x :bouncer-server "oftc")))
          ("b-uplink"
           :use-tls t
           :nick "jgkamat"
           :realname "Kay"
           :host "s.jgkamat.uk"
           :port 6697
           :user "jay"
           :server-buffer-name "uplink:mechjay:{host}:{port}"
           :pass ,(lambda (x)
                    (jay/nickserv-password x :bouncer-server "uplink")))
          ("b-synirc"
           :use-tls t
           :nick "jgkamat"
           :realname "Kay"
           :host "s.jgkamat.uk"
           :port 6697
           :user "jay"
           :server-buffer-name "synirc:mechjay:{host}:{port}"
           :pass ,(lambda (x)
                    (jay/nickserv-password x :bouncer-server "synirc")))
          ("b-gitter"
           :use-tls t
           :nick "jgkamat"
           :realname "Kay"
           :host "s.jgkamat.uk"
           :port 6697
           :user "jay"
           :server-buffer-name "gitter:mechjay:{host}:{port}"
           :lagmon-disabled t
           :pass ,(lambda (x)
                    (jay/nickserv-password x :bouncer-server "gitter")))

          ("Libera"
           :use-tls t
           :nick "jkamat"
           :realname "Kay"
           :host "irc.libera.chat"
           :port 6697
           :sasl-username "jay"
           :sasl-password ,#'jay/nickserv-password
           :channels ())
          ("Esper"
           :nick "jaykamat"
           :use-tls t
           :realname "Kay"
           :host "nova.esper.net"
           :port 6697
           :sasl-username "jgkamat"
           :sasl-password ,#'jay/nickserv-password
           :channels ("#kspofficial" "#factorio"))
          ("Oftc"
           :nick "jaykamat"
           :use-tls t
           :realname "Kay"
           :host "irc.oftc.net"
           :port 6697
           :sasl-username "jgkamat"
           :sasl-password ,#'jay/nickserv-password
           :channels ("#awesome"))
          ("Snoo"
           :nick "jaykamat"
           :use-tls t
           :realname "Kay"
           :host "irc.snoonet.org"
           :port 6697
           :sasl-username "jgkamat"
           :sasl-password ,#'jay/nickserv-password
           :channels ("#tech" "#programming" "#linux"))
          ("uplink"
           :nick "jaykamat"
           :use-tls nil
           :realname "Kay"
           :host "irc.uplinkcorp.net"
           :port 6667
           :sasl-username "jgkamat"
           :sasl-password ,#'jay/nickserv-password
           :channels ("#uplink"))
          ))

  (require 'circe-secrets nil 'noerror)

  (general-define-key
   :keymaps '(circe-mode-map)
   :states '(insert)
   ;; similar to weechat next buffer
   "M-a" #'tracking-next-buffer
   "M-RET" (lambda () (interactive) (goto-char (point-max))))

  ;; Fix comment breaking circe
  (general-define-key
   :keymaps '(circe-mode-map)
   :states '(normal)
   "M-j" #'ignore)

  (setq circe-use-cycle-completion t)

  (setq circe-reduce-lurker-spam t
        circe-active-users-timeout 3600)

  (setq lui-highlight-keywords '("\\bjkamat\\b" "\\bjaykamat\\b" "\\bperf-oncall\\b"))

  (setq lui-max-buffer-size  51200)

  (setq tracking-position 'end
        tracking-max-mode-line-entries 5
        tracking-sort-faces-first t
        tracking-shorten-modes '(circe-channel-mode))

  ;; Better tracking for busy channels
  (setq tracking-ignored-buffers
        '(("#python" circe-highlight-nick-face)
          ("#c++" circe-highlight-nick-face)
          ("##rust" circe-highlight-nick-face)
          ("#btrfs" circe-highlight-nick-face)))
  (defadvice circe-command-SAY (after jjf-circe-unignore-target)
    (when-let ((ignored (tracking-ignored-p (current-buffer) nil)))
      (setq tracking-ignored-buffers
            (remove ignored tracking-ignored-buffers))
      (message "This buffer will now be tracked.")))
  (ad-activate 'circe-command-SAY)

  (defun jay/lui-tracking-scroll (&rest _)
    (when tracking-start-buffer
      (goto-char (point-max))))
  (advice-add 'tracking-next-buffer :after #'jay/lui-tracking-scroll)

  ;; Bot handling
  (defvar jay/bot-list '("fsbot" "rudybot" "qutebrowser-gh" "qutebrowser-bot" "Not-8d6a"))
  (defvar jay/gh-user "@jgkamat")

  (add-hook 'circe-chat-mode-hook #'jay/circe-prompt)
  (defun jay/circe-prompt ()
    (lui-set-prompt
     (concat (propertize (concat (buffer-name) ">")
                         'face 'circe-prompt-face) " ")))

  ;; Keep place
  ;; (setq lui-track-behavior 'before-tracking-next-buffer)
  (enable-lui-track)

  ;; Logging
  ;; (require 'lui-logging)
  ;; (setq
  ;;  lui-logging-file-format "{buffer}.txt"
  ;;  lui-logging-format "[%F %T] {text}"
  ;;  lui-logging-flush-delay 10)
  ;; (enable-lui-logging-globally)

  ;; paste
  (require 'lui-autopaste)
  (add-hook 'circe-channel-mode-hook 'enable-lui-autopaste)

  ;; Nick colors
  (require 'circe-color-nicks)
  (enable-circe-color-nicks)
  (setq circe-color-nicks-everywhere t)

  ;; Lagmon and auto-reconnect(?)
  (require 'circe-lagmon)
  (circe-lagmon-mode)
  (setq circe-lagmon-check-interval 120)

  (setq circe-format-self-say circe-format-say)

  (setq lui-time-stamp-format "[%I:%M %p]")

  (setq lui-flyspell-p t
        ;; TODO why do we have this?
        ;; lui-flyspell-alist '((".*" "default"))
        )

  ;; The relevant keys can be found in circe-display-table
  (dolist (message '("353"))
    (circe-set-display-handler message 'circe-display-ignore))

  ;; Don't use evil-repeat when sending messages (aaah)
  (when (fboundp 'evil-without-repeat)
    (defun jay/evil-no-repeat-wrapper (orig-fun &rest args)
      (evil-without-repeat
        (apply orig-fun args)))
    (advice-add 'lui-insert :around #'jay/evil-no-repeat-wrapper)))

(use-package circe-notifications
  :after circe
  :init
  (add-hook 'circe-server-connected-hook 'enable-circe-notifications)
  ;; Kay is too short to be a reliable highlight, and we added it above
  (defun jay/wordify-advice (orig-fn &rest args)
    (mapcar
     (apply-partially #'format "\\b%s\\b")
     (apply orig-fn args)))
  (advice-add 'circe-notifications-nicks-on-all-networks :around #'jay/wordify-advice)

  ;; TODO add more elaborate config for notifications
  :config
  (require 'cl-lib)

  (defvar circe-notifications-high-pri-strings)
  (setq circe-notifications-watch-strings
        '("^#qutebrowser-offtopic$"
          "^#rmsbolt$"
          "^#lugatgt$"
          "^##lugatgt$"
          "^#interns$"
          ;; "^#uplink$"
          "^#darwinia$" "^#defcon$"
          ;;"^##art$"
          "^#object-introspection$"
          ;; "^#elfutils$"
          "^#bpftrace$"
          ;; "^#fallenlondon$"
          ))
  (setq circe-notifications-wait-for 0
        circe-notifications-high-pri-strings
        (cl-remove-duplicates
         (append lui-highlight-keywords
                 (circe-notifications-nicks-on-all-networks))
         :test #'equal))

  (defun jay/circe-notify (nick body channel)
    "My custom notification function."
    (when
        (not (member nick jay/bot-list))
      (let* ((high-pri
              ;; Find if we have any keywords matching in the body
              (or
               (cl-find-if
                (lambda (item) (string-match item body))
                circe-notifications-high-pri-strings)
               ;; If we have a privmsg
               (and
                (not (string= nick "*status"))
                (cl-find channel
                         circe-notifications-high-pri-strings
                         :test #'string-match)))))
        (alert
         body
         :severity (if high-pri 'high 'low)
         :title (format "%s (%s)" nick channel)))))
  (setq circe-notifications-notify-function #'jay/circe-notify)

  ;; Never ever notify for join/part
  (defun circe-notifications-JOIN (&rest _))
  (defun circe-notifications-PART (&rest _))
  (defun circe-notifications-QUIT (&rest _))

  (require 'erc-hl-nicks))

(use-package circe-chanop
  :ensure nil
  :after circe)

;; Better highlight strategy for circe

(use-package erc-hl-nicks
  :after circe
  :config
  (defun circe-nick-color-for-nick (nick)
    "Override color strategy with my own"
    (erc-hl-nicks-color-for-nick nick))
  (when (> emacs-major-version 25)
    (defun circe-nick-color-nick-list ()
      (mapcar (lambda (nick) (string-trim-right nick (regexp-opt '("[m]"))))
              (circe-channel-nicks))))

  ;; Prevent dark nicks
  (setq erc-hl-nicks-minimum-luminence 100))

(provide 'circe-config)

;;; circe-config.el ends here

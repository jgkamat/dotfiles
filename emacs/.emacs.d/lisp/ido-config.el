;;; ido-config.el - My ido configs -*- lexical-binding: t -*-

;; WARNING, see helm-config too!

;;; Code:

(require 'general)
(use-package ido
  :config
  (ido-mode 1)
  (ido-everywhere 1)

  (setq ido-enable-flex-matching t
        ido-everywhere t
        ido-use-faces nil)
  (defun jay/recentf-ido-find-file ()
    "Find a recent file using Ido."
    (interactive)
    (let ((file (ido-completing-read "Choose recent file: " recentf-list nil t)))
      (when file
        (find-file file))))

  (defun jay/ido-find-all-files ()
    "Combine recentf ido and projectile ido."
    (interactive)
    (let*
        ((my-ido-list
          (append
           ;; If in projectile project, add project files!
           (if (projectile-project-p)
               (projectile-current-project-files)
             nil)
           ;; Always add the recentf list
           recentf-list))
         (file (ido-completing-read "--> " my-ido-list nil t)))
      (when file
        (find-file
         (if (eql (elt file 0) ?/)
             file
           (concat (projectile-project-root) file))))))
  ;; Ido show paren stuff
  (defun my-remove-mic-paren-face-in-minibuffer ()
    (set (make-variable-buffer-local 'paren-match-face) nil))

  (add-hook 'ido-minibuffer-setup-hook #'my-remove-mic-paren-face-in-minibuffer)
  (define-key ido-common-completion-map (kbd "C-<return>") 'ido-select-text)


  )

(use-package ido-completing-read+
  :config
  (ido-ubiquitous-mode 1))

(use-package amx
  :init
  (amx-mode)
  ;; (global-set-key (kbd "M-x") 'amx)
  (global-set-key (kbd "M-X") 'amx-major-mode-commands)
  ;; ;; This is your old M-x.
  ;; (global-set-key (kbd "C-c M-x") 'execute-extended-command)
  )

(provide 'ido-config)
;;; ido-config.el ends here

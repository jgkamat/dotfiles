;;; tabbar-tweaks.el --- My personal tabbar tweaks -*- lexical-binding: t -*-

;;; Commentary:
;; 	tabbar

;; need corner images!
;; (setq tabbar-use-images nil)

(require 'tabbar)

(tabbar-mode 1)

;; Tabbar settings
(set-face-attribute
 'tabbar-default nil
 :background "gray20"
 :foreground "gray20"
 :box '(:line-width 1 :color "gray20" :style nil))
(set-face-attribute
 'tabbar-unselected nil
 :background "gray75"
 :foreground "gray20"
 :box '(:line-width 5 :color "gray20" :style nil))
(set-face-attribute
 'tabbar-selected nil
 :background "gray75"
 :foreground "black"
 :box '(:line-width 5 :color "black" :style nil))
(set-face-attribute
 'tabbar-modified nil
 :background "gray75"
 :foreground "gray30"
 :box '(:line-width 5 :color "gray30" :style nil))
(set-face-attribute
 'tabbar-highlight nil
 :background "gray75"
 :foreground "black"
 :underline nil
 :box '(:line-width 5 :color "black" :style nil))
(set-face-attribute
 'tabbar-button nil
 :box '(:line-width 1 :color "gray20" :style nil))
(set-face-attribute
 'tabbar-separator nil
 :background "gray20"
 :height 0.6)

(defun string-starts (string prefix)
  "Return t if STRING starts with PREFIX."
  (and
   (string-match (rx-to-string `(: bos ,prefix) t) string) t))

(defun string-ends (string prefix)
  "Return t if STRING ends with PREFIX."
  (and
   (string-match (rx-to-string `(: prefix ,bos) t) string) t))

;; Sort tabs by dir structure
(defun tabbar-add-tab (tabset object &optional append)
  "Add to TABSET a tab with value OBJECT if there isn't one there yet.
If the tab is added, it is added at the beginning of the tab list,
unless the optional argument APPEND is non-nil, in which case it is
added at the end."
  (let ((tabs (tabbar-tabs tabset)))
    (if (tabbar-get-tab object tabset)
        tabs
      (let ((tab (tabbar-make-tab object tabset)))
        (tabbar-set-template tabset nil)
        (set tabset
             (if (projectile-project-p)
                 (sort (cons tab tabs) #'tabbar-default-directory<)
               (if append
                   (append tabs (list tab))
                 (cons tab tabs))))))))

(defun tabbar-default-directory< (a b)
  "Is the `default-directory' of tab A `string<' than that of B?"
  ;; If not a file buffer, sort by its buffer name"
  (if (and (buffer-local-value 'buffer-file-name (car a)) (buffer-local-value 'buffer-file-name (car b)))
      (string<
       (expand-file-name (buffer-local-value 'buffer-file-name (car a)))
       (expand-file-name (buffer-local-value 'buffer-file-name (car b))))

    (progn
      (let ((aname (buffer-name (car a))) (bname (buffer-name (car b))))
        ;; Keep # buffers later
        (setq aname (if (eql (elt aname 0) ?#) (concat "~" aname) aname))
        (setq bname (if (eql (elt bname 0) ?#) (concat "~" bname) bname))

        (string<
         aname bname)))))

(defun tabbar-tab-derived-mode-p (mode tab)
  "Is the major mode of TAB derived from MODE?"
  (with-current-buffer (car tab)
    (derived-mode-p mode)))


;; group buffers if any *s are left
(defun my-tabbar-groups-project ()
  "Return the list of group names the current buffer belongs to."
  (list
   (cond
    ;; ((member (buffer-name)
    ;;    '("*compilation*"))
    ;;   "Compile" ;; this is a group name)
    ;; A *buffer*
    ((string-equal "*" (substring (buffer-name) 0 1))
     "Emacs")
    ;; Ends with .pdf
    ((string-equal
      ".pdf"
      (substring (buffer-name)
                 (- (length (buffer-name)) 4)
                 (length (buffer-name))))
     "Documents")
    ((string-equal major-mode "erc-mode")
     "ERC")
    ((string-equal major-mode "weechat-mode")
     "Weechat")
    ((string-equal major-mode "matrix-client-mode")
     "Matrix")
    ;; TODO find a real solution to stop hangs.
    ;; Probably prevent running (projectile-project-name) in minibuffer
    ((file-remote-p default-directory)
     "Tramp")
    (t
     ;; Otherwise, group by projectile project
     (if (projectile-project-p)
         (projectile-project-name)
       "General")))))

(defvar jay-buffer-show-regex
  (concat "^"
          (regexp-opt
           '("*ansi-term*"
             ;; "*compilation*"
             "*shell*"
             "*terminal"
             "*run"
             "*gud"
             "*eshell"
             "*Python"
             "*inferior-lisp"
             "*eww"
             "*xkcd"
             "*info"
             "*slime-repl")))
  "Regex, which when matched, allows a hidden buffer to stay.")

;; hide some buffers
(setq tabbar-buffer-list-function
      (lambda ()
        (remove-if
         (lambda (buffer)
           (and
            (or
             ;; Hide buffers starting with " " or "*"
             (find (aref (buffer-name buffer) 0) " *")
             ;; Hide TAGS
             (string-equal (buffer-name buffer) "TAGS"))
            (not
             ;; Show buffers on the whitelist
             (string-match jay-buffer-show-regex (buffer-name buffer)))))
         (buffer-list))))



;; Cache The tabbar groups function to speed it up!
(defun my-cached (func)
  "Turn a function into a cache dict."
  (lexical-let ((table (make-hash-table :test 'equal))
                (f func))
    (lambda (key)
      (let ((value (gethash key table)))
        (if value
            value
          (puthash key (funcall f) table))))))

(defvar cached-ppn nil)

(defun clear-projectile-cache ()
  "Clears my custom projectile cache."
  (interactive)
  (setf cached-ppn (my-cached 'my-tabbar-groups-project)))

;; (clear-projectile-cache)
;; Clear projectile cache when we open a new buffer (seems to be good enough)
(add-hook 'change-major-mode-hook #'clear-projectile-cache)

;; Be evil, and replace major mode's specific tabbar with our own.
(add-hook 'Info-mode-hook #'tabbar-local-mode)
(add-hook 'weechat-mode-hook #'tabbar-local-mode)
(add-hook 'matrix-client-mode-hook #'tabbar-local-mode)

(defun my-tabbar-groups-by-project ()
  (funcall cached-ppn (buffer-name)))

(setq tabbar-buffer-groups-function 'my-tabbar-groups-by-project)

(general-define-key
 :keymaps 'tabbar-mode-map
 ;; Tabbar config
 "M-h" #'tabbar-backward-tab
 "M-l" #'tabbar-forward-tab
 "M-H" #'tabbar-backward-group
 "M-L" #'tabbar-forward-group)

;; Use tabbar
(setq erc-header-line-uses-tabbar-p t)

(provide 'tabbar-tweaks)

;;; tabbar-tweaks ends here

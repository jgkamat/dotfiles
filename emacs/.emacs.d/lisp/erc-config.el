;;; erc-config.el --- My erc configs -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

(require 'erc)

;; ERC Configs
(setq
 erc-server-history-list '("irc.freenode.net"))
(setq erc-default-server "irc.freenode.net"
      erc-prompt (lambda () (concat (buffer-name) ">"))
      erc-join-buffer 'bury)

(setq erc-modules
      '(autojoin button completion fill irccontrols list match
                 menu move-to-prompt netsplit networks noncommands
                 readonly ring stamp track truncate keep-place))
(use-package erc-scrolltoplace
  :after erc
  :config
  (add-to-list 'erc-modules 'scrolltoplace))
(erc-update-modules)

;; Set personal information
(setq erc-nick "jkamat"
      erc-user-full-name "Jay"
      erc-keywords '("jgkamat"))

(setq erc-autojoin-channels-alist
      '(("freenode.net"
         "#emacs"
         "#lugatgt"
         "#org-mode"
         "#qutebrowser"
         "#hy"
         "#evil-mode")))

;; Autologin if possible
(when (require 'erc-secrets nil 'noerror)
  (setq erc-prompt-for-nickserv-password nil))

(when (and (boundp 'bouncer-pw) (boundp 'bouncer-addr))
  (defun erc-jay ()
    "Joins the freenode irc network."
    (interactive)
    (erc :server bouncer-addr :port 6697
         :nick "jgkamat" :full-name "Jay Kamat" :password bouncer-pw))
  (defun erc-jay-direct ()
    (interactive)
    (erc-tls :server "irc.freenode.net" :port 6697
             :nick "jaykamat" :full-name "Jay Kamat" :password freenode-pw)))

(defun erc-cmd-QBV (nick)
  "Ask NICK for their qb version details."
  (erc-send-message
   (concat nick ": what backend, qutebrowser version, and"
           " Qt version? (:open qute://version)")))

;; Only join after ident
(setq erc-autojoin-timing 'ident)

;; ERC tracking mode

(require 'a)
(defconst jay/erc-shorten-alist
  (a-list "qutebrowser" "qb"
          "lugatgt" "l@gt"))

(defun jay/erc-shorten-fn (name)
  "Function that shortens erc NAMEs."
  (dolist (pair jay/erc-shorten-alist)
    (setq name
          (replace-regexp-in-string (car pair) (cdr pair) name)))
  name)

(defun jay/erc-shorten-names (names)
  "Personal additions to erc-track-shorten-names.
NAMES names to shorten."
  (mapcar #'jay/erc-shorten-fn
          (erc-track-shorten-names names)))

(setq erc-track-exclude-types '("JOIN" "NICK" "PART" "QUIT" "MODE"
                                "324" "329" "332" "333" "353" "477")
      erc-track-exclude-server-buffer t
      erc-format-query-as-channel-p t
      erc-track-switch-direction 'importance
      erc-track-shorten-function #'jay/erc-shorten-names
      erc-track-faces-priority-list
      '(erc-error-face
        erc-current-nick-face
        erc-keyword-face
        erc-nick-msg-face
        erc-direct-msg-face
        erc-dangerous-host-face
        erc-prompt-face))



;; ERCN Notification rules
(require 'erc-desktop-notifications)
(use-package ercn
  :after erc
  :config
  (setq ercn-notify-rules
        '((current-nick . all)
          (keyword . all)
          (pal . all)
          (query-buffer . all)
          (message . ("#qutebrowser" "#lugatgt")))

        ercn-suppress-rules
        '((dangerous-host . all)
          (fool . all)
          (system . all)))

  (defun do-notify (nickname message)
    "My personal notification code.
NICKNAME the nickname that is speaking
MESSAGE the message to show"
    (let ((message
           (if (functionp 's-collapse-whitespace)
               (s-collapse-whitespace message)
             message)))
      (erc-notifications-notify nickname message)))
  (add-hook 'ercn-notify-hook 'do-notify))


;; No user list on join
(setq erc-hide-list
      '("353" ;; User list
        ))

;; No join/leave messages
(setq erc-lurker-hide-list '("JOIN" "PART" "QUIT" "NICK"))
(setq erc-lurker-threshold-time 3600)

;; Nice times
(setq erc-timestamp-format "[%I:%M %p]"
      erc-timestamp-format-right erc-timestamp-format
      erc-insert-timestamp-function #'erc-insert-timestamp-right)

(use-package erc-hl-nicks
  :after erc
  :config
  (setq erc-hl-nicks-skip-nicks
        '("jgkamat" "jkamat"))
  (add-to-list 'erc-hl-nicks-skip-faces "erc-current-nick-face")
  (add-to-list 'erc-hl-nicks-skip-faces "erc-keyword-face"))

;; reconnection
(setq erc-server-auto-reconnect t
      erc-server-reconnect-attempts 10
      erc-server-reconnect-timeout 60)

(general-define-key
 :keymaps '(my-keys-minor-mode-map)
 :states '(normal)
 ;; similar to weechat next buffer
 "M-a" #'erc-track-switch-buffer)

;; Let M-a switch buffers in insert mode
(general-define-key
 :keymaps '(erc-mode-map)
 :states '(insert)
 ;; similar to weechat next buffer
 "M-a" #'erc-track-switch-buffer
 "M-RET" (lambda () (interactive) (goto-char (point-max))))


;; Truncation
(setq erc-max-buffer-size 40000)
(add-hook 'erc-insert-post-hook 'erc-truncate-buffer)

;; logging
;; Hack to fix erc log restore
;; (setq erc-log-channels-directory "./.erclogs/"
;;   erc-save-buffer-on-part t
;;   erc-log-write-after-send t
;;   erc-log-write-after-insert t
;;   erc-log-insert-log-on-open nil)

;; Turn off logging
(setq erc-save-buffer-on-part nil)
(setq-default erc-enable-logging nil)

(evil-set-initial-state 'erc-mode 'normal)

(provide 'erc-config)

;;; erc-config.el ends here

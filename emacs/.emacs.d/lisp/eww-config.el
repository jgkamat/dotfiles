;;; eww-config.el --- My config for eww

;;; Commentary:
;; None

;;; Code:

(require 'eww)
(require 'seq)

(setq eww-history-limit 10)

(general-define-key
 :keymaps '(eww-mode-map)
 :states '(normal)
 "o" #'eww
 "J" #'eww-back-url
 "K" #'eww-forward-url
 "go" #'eww-browse-with-external-browser
 "r" #'eww-reload
 "<tab>" #'shr-next-link
 "<C-tab>" #'shr-previous-link
 "Y" #'eww-copy-page-url
 "b" nil)

(general-define-key
 :keymaps '(eww-link-keymap shr-image-map)
 "i" nil
 "w" nil
 "ESC" nil
 "o" nil)
(general-define-key
 :keymaps '(shr-image-map image-map)
 "=" #'image-increase-size)

(general-define-key
 :prefix my-leader-1
 "w" #'eww)

(setq shr-max-image-proportion 0.8
      shr-image-animate nil

      shr-color-visible-luminance-min 50)

(defvar cleanup-eww-timer nil
  "Timer for cleaning up stock eww buffers after x time.")
(unless cleanup-eww-timer
  (setq
   cleanup-eww-timer
   (run-with-idle-timer
    (* 60 60)
    nil
    (lambda ()
      (mapcar
       #'kill-buffer
       (seq-filter
        (lambda (buffer)
          (string= "*eww*"
                   (buffer-name buffer)))
        (buffer-list)))))))

(use-package visual-fill-column
  :defer t
  :init
  ;; (add-hook 'eww-mode-hook #'visual-fill-column-mode)
  )

(provide 'eww-config)

;;; eww-config.el ends here

;;; config.el --- Configs that do not depend on external packages -*- lexical-binding: t -*-

;; Copyright (C) 2014-2019 Jay Kamat
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;; General Helper Functions

(defun multi-add-to-list (list &rest elements)
  "Modify the LIST to include all ELEMENTS.

Internally uses `add-to-list', so all elements will be treated as if they are using `add-to-list' calls"
  (dolist (elt elements)
    (add-to-list list elt))
  (eval list))

;;;; Misc Configuration

;; Vim scrolling
;; (setq redisplay-dont-pause t
;; 	scroll-margin 5
;; 	scroll-step 1)

(setq scroll-step 1
      auto-window-vscroll nil
      scroll-conservatively 10000
      scroll-margin 0
      scroll-up-aggressively 0.00
      scroll-down-aggressively 0.0
      mouse-wheel-progressive-speed nil)

;; Fonts. Choose whatever you want I guess.
(cond
 ((find-font (font-spec :name "Monoid"))
  (set-frame-font "Monoid-9" nil t))
 ((find-font (font-spec :name "DejaVu Sans Mono"))
  (set-frame-font "DejaVu Sans Mono-10" nil t))
 ((find-font (font-spec :name "Terminus"))
  (set-frame-font "Terminus-12" nil t))
 (t
  ;; We can't do squat
  (message "You don't have any good fonts installed!")))

;; gc-cons-threshold modifications
;; (defun my-minibuffer-exit-hook ()
;;   "Restore gc to previous value."
;;   (setq gc-cons-threshold +gc-cons-value+))

;; (defun my-minibuffer-setup-hook ()
;;   "Multiply gc cons by 2 to avoid gc if possible."
;;   (setq gc-cons-threshold (* 2 gc-cons-threshold)))

;; Don't GC when minibuffer is open
;; (add-hook 'minibuffer-setup-hook #'my-minibuffer-setup-hook)
;; (add-hook 'minibuffer-exit-hook #'my-minibuffer-exit-hook)
;; Allow for a larger mininbuffer!

(setq max-mini-window-height 5)

(defun garbage-collect-no-msg ()
  "Collect garbage with no messages."
  (let ((garbage-collection-messages nil))
    (garbage-collect)))

(defvar garbage-collection-timer nil)
(unless garbage-collection-timer
  (setq garbage-collection-timer
        (run-with-idle-timer
         20 t
         #'garbage-collect-no-msg)))

;; take out garbage if we loose focus for 1s
;; (prevents hang on quick switches between frames)
(defvar focus-garbage-collection-timer nil)
(add-hook 'focus-out-hook
          (lambda ()
            (unless focus-garbage-collection-timer
              (setq focus-garbage-collection-timer
                    (run-with-idle-timer
                     5 nil
                     #'garbage-collect-no-msg)))))
(add-hook 'focus-in-hook
          (lambda ()
            (when focus-garbage-collection-timer
              (cancel-timer focus-garbage-collection-timer)
              (setq focus-garbage-collection-timer nil))))

;; Garbage collection messages
;; Uncomment if garbage collection messages are desired
;; (setq garbage-collection-messages t)

;; Toolbars
(when window-system
  (tool-bar-mode -1))
(menu-bar-mode -1)

;; Paren mode
(require 'paren)
(setq show-paren-delay 0.05)           ;; how long to wait?
(setq show-paren-style 'expression) ;; alternatives are 'parenthesis' and 'mixed'
(show-paren-mode 1)

(defun unfill-paragraph ()
  "Takes a multi-line paragraph and make it into a single line of text.
This is the opposite of `fill-paragraph'."
  (interactive)
  (let ((fill-column (point-max)))
    (fill-paragraph nil)))

(defun unfill-region ()
  "Takes a multi-line paragraph and make it into a single line of text.
This is the opposite of `fill-region'."
  (interactive)
  (let ((fill-column (point-max)))
    (call-interactively #'fill-region)))

(add-hook 'org-mode-hook
          (lambda ()
            ;; Don't turn on for org mode
            ;; https://lists.gnu.org/archive/html/emacs-orgmode/2016-05/msg00186.html
            (setq-local show-paren-mode nil)
            ;; Use auto-fill in org mode
            (turn-on-auto-fill)
            ;; Automatically revert changes from syncthing
            (auto-revert-mode 1)))

;; Open in qb
(require 'browse-url)
(require 'shr)
(setq browse-url-browser-function (apply-partially 'jay/browse-url-generic nil)
      shr-external-browser (apply-partially 'jay/browse-url-generic t)
      ;; If you just want qb
      ;; browse-url-browser-function #'browse-url-generic
      browse-url-generic-program "firefox")

(defvar jay/video-regexp)
(setq jay/video-regexp (regexp-opt '("youtube.com" "youtu.be")))
(defvar jay/music-regexp)
(setq jay/music-regexp "mp[34]$")

(defun jay/browse-url-generic (external url &rest _)
  "Browse to URL via jumplist.

EXTERNAL whether this function should go to an external browser if needed."

  ;; Legally, ')' and "'" are valid characters at the end of URLs.
  ;; However, this leads to (link) breaking. Let's trim them ourselves from the end.
  (setq url
        (thread-last url
          (string-remove-suffix ")")
          (string-remove-suffix "'")
          (string-remove-suffix "\"")))
  (cond
   ((or (string-match jay/video-regexp url)
        (string-match jay/music-regexp url))
    ;; Play in MPV
    (let* ((browse-url-generic-program "mpv")
           (browse-url-generic-args '("--profile=web")))
      (browse-url-generic url)))
   (t (if (or external
              ;; TODO re-evaluate internal browser.
              ;; right now, it seems to use too much memory, even when it's just w3m.
              t)
          (browse-url-generic url)
        (w3m-browse-url url)))))

;; Colors
(add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)
(add-to-list 'comint-output-filter-functions 'ansi-color-process-output)

;; Shell commands
(add-hook 'term-mode-hook
          (lambda ()
            (setq yas-dont-activate t)))

;; Shell config
(require 'comint)
(setq
 comint-scroll-to-bottom-on-input t
 comint-scroll-to-bottom-on-output t
 comint-scroll-show-maximum-output nil
 comint-input-ignoredups t
 comint-password-prompt-regexp
 (format "\\(%s\\).*:\\s *\\'"
         (regexp-opt
          (append '("[su]") password-word-equivalents))))

;; use python3 in shell
(require 'python)
(setq python-shell-interpreter "python3")

;; Save state on exit
;; Also closes welcome buffer
(require 'desktop)
(desktop-save-mode 1)

;; Autosave after 30s
;; Don't run if desktop-save-mode is not true
(defvar my-desktop-autosave-timer (not desktop-save-mode))
(unless my-desktop-autosave-timer
  (setq my-desktop-autosave-timer
        (run-with-idle-timer 30 nil
                             #'desktop-auto-save)))

(unless (display-graphic-p)
  (setq desktop-restore-forces-onscreen nil)
  (defun desktop-restoring-frameset-p ()
    "True if calling `desktop-restore-frameset' will actually restore it.

Removes graphical from the list of required parameters"
    (and desktop-restore-frames desktop-saved-frameset t)))


;; Don't save non-important buffers
(multi-add-to-list
 'desktop-modes-not-to-save
 'dired-mode
 'Info-mode
 'info-lookup-mode
 'fundamental-mode)

;; Start an emacs server
;; This is needed for magit (sigh)
(require 'server)
(when
    (and (fboundp 'server-running-p)
         (not (server-running-p)))
  (server-start))

;; Clean up buffers
(require 'midnight)
(midnight-mode 1)
(setq clean-buffer-list-delay-general 2)
;; Try to blacklist circe server buffers
;; TODO solve this in a better way
(add-to-list 'clean-buffer-list-kill-never-regexps ":6697$")
(add-to-list 'clean-buffer-list-kill-never-regexps "^#")
;; run clean-buffer-list to manually clean

;; no scrollbar
(when window-system
  (scroll-bar-mode -1)
  (when (boundp 'horizontal-scroll-bar-mode)
    (horizontal-scroll-bar-mode -1)))

;; autosave settings
(setq auto-save-default nil)
;; (setq make-backup-files nil)

;; Put autosave in /tmp
;; Save all tempfiles in $TMPDIR/emacs$UID/
(defconst emacs-tmp-dir (format "%s/%s%s/" temporary-file-directory "emacs" (user-uid)))
(setq backup-directory-alist
      `((".*" . ,emacs-tmp-dir)))

(setq sentence-end-double-space nil)

;; Syntax highlight
(global-font-lock-mode t)
(setq font-lock-maximum-decoration
      '((c++-mode . 2)
        (c-mode . 2)
        (java-mode . 2)
        (php-mode . 2)
        (t . t)))
;;
(defun jay/very-large-file ()
  "My function to check to see if performance should be optimized."
  (let ((line-numbers (line-number-at-pos (point-max))))
    (> line-numbers 3000)))

;; better blinking
(setq blink-cursor-interval 0.25
      blink-cursor-delay 0.5
      blink-cursor-blinks 0)
(blink-cursor-mode 1)

(add-hook 'doc-view-mode-hook
          (lambda ()
            (end-of-visual-line)
            ;; Ensure we have updates automagically!
            (auto-revert-mode 1)
            (setq-local blink-cursor-blinks 1)
            (setq-local blink-cursor-interval most-positive-fixnum)
            (setq-local blink-cursor-delay most-positive-fixnum)))

;; Recentf config
(require 'recentf)
(recentf-mode 1)
(setq recentf-max-menu-items 50)

;; Indent arguments nicely
(setq c-default-style
      '((java-mode . "k&r")
        (awk-mode . "awk")
        (c-mode . "k&r")
        (other . "k&r")))

;; No more super wide tabs ever.
(setq-default tab-width 4)

;; // comments
(add-hook 'c-mode-hook
          (lambda ()
            (setq
             comment-start "//"
             comment-end "")))

(add-hook 'prog-mode-hook (lambda () (setq-local scroll-margin 4)))
(add-hook 'text-mode-hook (lambda () (setq-local scroll-margin 4)))
(add-hook 'eww-mode-hook (lambda () (setq-local scroll-margin 4)))
(add-hook 'Info-mode-hook (lambda () (setq-local scroll-margin 4)))
(add-hook 'erc-mode-hook (lambda () (setq-local scroll-margin 0)))
(add-hook 'mu4e-headers-mode-hook (lambda () (setq-local scroll-margin 3)))

(add-hook 'lisp-mode-hook
          (lambda ()
            (set (make-local-variable 'lisp-indent-function)
                 'common-lisp-indent-function)))

(add-to-list 'auto-mode-alist '("\\.cl\\'" . common-lisp-mode))

;; Yes/No -> y/n
(defalias 'yes-or-no-p 'y-or-n-p)

;; Confirm exit
(setq confirm-kill-emacs 'y-or-n-p)

;; SSH via tramp
(require 'tramp)
(require 'tramp-sh)
(setq tramp-default-method "ssh")
;; (eval-after-load 'tramp '(setenv "SHELL" "/bin/sh"))
(setq tramp-versbose 3
      ;; Save temp files to /tmp/tramp
      tramp-auto-save-directory "/tmp/tramp"
      ;; Disable default tramp controlmaster options, giving us faster ssh access
      ;; https://emacs.stackexchange.com/questions/22306/working-with-tramp-mode-on-slow-connection-emacs-does-network-trip-when-i-start
      tramp-ssh-controlmaster-options "")

;; Tramp remote sudo hacks
(set-default 'tramp-default-proxies-alist
             '(
               ;; Don't use any proxies on the local machine
               (system-name ".*" nil)
               ("localhost" ".*" nil)
               ;; Use ssh proxy if we're connecting by root anywhere else
               (".*" "\\`root\\'" "/ssh:%h:")))

(add-hook 'find-file-hook
          (lambda ()
            (when (file-remote-p default-directory)
              (setq-local projectile-mode-line "Projectile[???]"))))

;; Try to get X11 forwarding? :O
;; info:tramp#Remote%20processes
(add-to-list 'tramp-remote-process-environment
             (format "DISPLAY=%s" (getenv "DISPLAY")))

(setq use-dialog-box nil)  ; Don't use GUI dialogs

;; Titlebar
(setq frame-title-format "MechJay - %b")

;; Dos2unix
;; (setq inhibit-eol-conversion t)

(defun dos2unix ()
  "Convert this entire buffer from MS-DOS text file format to UNIX."
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (replace-regexp "\r$" "" nil)
    (goto-char (1- (point-max)))
    (if (looking-at "\C-z")
        (delete-char 1))))

;; Browser config, overrides to use xdg open if possible even under X-Generic
(with-eval-after-load "browse-url"
  (defun browse-url-can-use-xdg-open ()
    "Return non-nil if the \"xdg-open\" program can be used.
xdg-open is a desktop utility that calls your preferred web browser.
This requires you to be running either Gnome, KDE, Xfce4 or LXDE."
    (and (getenv "DISPLAY")
         (executable-find "xdg-open")
         ;; xdg-open may call gnome-open and that does not wait for its child
         ;; to finish.  This child may then be killed when the parent dies.
         ;; Use nohup to work around.  See bug#7166, bug#8917, bug#9779 and
         ;; http://lists.gnu.org/archive/html/emacs-devel/2009-07/msg00279.html
         (executable-find "nohup"))))

;; Prevent cluttering ~ in gnus
(setq
      ;; Don't use non-monospaced fonts for html emails
      ;; shr-use-fonts nil

      ;; Don't clutter ~/
      message-directory "~/.emacs.d/mail/"
      gnus-directory "~/.emacs.d/news/")

;; Dired config
(setq dired-listing-switches "-alh"
      dired-dwim-target t
      dired-guess-shell-alist-user '((".*" "xdg-open")))

(defun dired-run-shell-command (command)
  "Redefine dired-run-shell-command to avoid buffers and annoying prompts."
  (let ((handler
	       (find-file-name-handler (directory-file-name default-directory)
				                         'shell-command)))
    (if handler (apply handler 'shell-command (list command))
      (call-process-shell-command command)))
  ;; Return nil for sake of nconc in dired-bunch-files.
  nil)


;; ASM comments are whatever they want them to be
;; (setq asm-comment-char ?\;)
(setq asm-comment-char ?\#)

(defun create-scratch-buffer nil
  "Create a new scratch buffer, if the existing one was deleted."
  (interactive)
  (unless (get-buffer "*scratch*")
    (switch-to-buffer (get-buffer-create "*scratch*"))
    (lisp-interaction-mode)
    (insert initial-scratch-message)))

;; Stolen from fortune-cookie mode
(defun fortune-cookie-comment (arg prefix)
  "Comment ARG with PREFIX.
ARG is the input string.
PREFIX is prepended to each line of ARG."
  (interactive)
  (mapconcat
   (lambda (x) (concat prefix x))
   (split-string arg "\n" t) "\n"))

(setq initial-scratch-message
      (concat ";; --- MechJay on " (system-name) " --- \n;;\n"))
(when
    (and (executable-find "cowsay")
         (executable-find "fortune"))

  (let*
      ((fortune-command
        (if (not (call-process "fortune" nil nil nil "-o"))
            "fortune -o"
          "fortune"))
       (cowsay-command
        (or (executable-find "cowthink")
            (executable-find "cowsay")))
       (cowsay-args  "-W 60 -f tux -n")
       (cowsay-result
        (fortune-cookie-comment
         (shell-command-to-string (concat fortune-command "|" cowsay-command " " cowsay-args)) ";; ")))

    ;; Scratch message
    (setq initial-scratch-message
          (concat initial-scratch-message cowsay-result))))

;; Ibuffer config
(require 'ibuf-ext)
(add-to-list 'ibuffer-never-show-predicates
             (lambda (buffer)
               (with-current-buffer buffer
                 (or (eq major-mode 'erc-mode)
                     (eq major-mode 'pianobar-mode)))))

;; Proced config
(add-hook 'proced-mode-hook
          (lambda ()
            (visual-line-mode -1)
            (setq truncate-lines t)))

;; Automatic Copyright Update
;; TODO fix -present
;; (add-hook 'before-save-hook 'copyright-update)

;; Better splitting
(defun jay/split-window-sensibly (&optional window)
  "replacement `split-window-sensibly' function which prefers vertical splits"
  (interactive)
  (let ((window (or window (selected-window))))
    (or (and (window-splittable-p window t)
             (with-selected-window window
               (split-window-right)))
        (and (window-splittable-p window)
             (with-selected-window window
               (split-window-below))))))
(setq split-window-preferred-function #'jay/split-window-sensibly)
(setq split-height-threshold 80
      split-width-threshold 160)

(defvar split-height-abs-threshold 15
  "If a window is shorter than this, exclude it from ALL splitting, not just horizontal.
Great for thin compile buffers")
(defun jay/splittable-advice (fun win &optional horizontal)
  (if (<= (window-height win) split-height-abs-threshold)
      nil
    (funcall fun win horizontal)))
(advice-add 'window-splittable-p :around #'jay/splittable-advice)

;; Url decoding
(defun func-region (start end func)
  "run a function over the region between START and END in current buffer."
  (save-excursion
    (let ((text (delete-and-extract-region start end)))
      (insert (funcall func text)))))
(defun urlencode (start end)
  "urlencode the region between START and END in current buffer."
  (interactive "r")
  (func-region start end #'url-hexify-string))
(defun urldecode (start end)
  "de-urlencode the region between START and END in current buffer."
  (interactive "r")
  (func-region start end #'url-unhex-string))

;; Disable vc on tramp
;; Maybe we can eventually use vc over non-magit
(setq vc-ignore-dir-regexp
      (format "\\(%s\\)\\|\\(%s\\)"
              (default-value 'vc-ignore-dir-regexp)
              tramp-file-name-regexp))

;; Allow colors to work when noninteractive
(when noninteractive
  (custom-set-faces
   ;; Draculized minimal: https://gitlab.com/jgkamat/darculized
   ;; TODO find out why default face is not applying.
   '(default                      ((t (:foreground "#909396" :background "#262626"))))
   '(font-lock-builtin-face       ((t (:foreground "#598249"))))
   '(font-lock-comment-face       ((t (:foreground "#5e6263"))))
   '(font-lock-constant-face      ((t (:foreground "#15968D"))))
   '(font-lock-function-name-face ((t (:foreground "#2F7BDE"))))
   '(font-lock-keyword-face       ((t (:foreground "#598249"))))
   '(font-lock-string-face        ((t (:foreground "#15968D"))))
   '(font-lock-type-face		       ((t (:foreground "#598249"))))
   '(font-lock-variable-name-face ((t (:foreground "#2F7BDE"))))
   '(font-lock-warning-face       ((t (:foreground "#bd3832" :weight bold)))))
  ;; (setq htmlize-use-rgb-map 'force)
  ;; (require 'htmlize)
  )

;; No audible bells!!
(setq ring-bell-function 'ignore)

;; No warning spam (should this be set?)
(setq warning-minimum-level :error)

;; Path fix...
(when (file-exists-p "/opt/homebrew/bin")
  (setenv "PATH" (concat (getenv "PATH") ":/opt/homebrew/bin"))
  (setq exec-path (append exec-path '("/opt/homebrew/bin"))))



(provide 'config)
;;; config.el ends here

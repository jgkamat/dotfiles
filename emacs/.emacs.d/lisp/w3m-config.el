;;; w3m-config.el --- My config for w3m

;;; Commentary:
;; None

;;; Code:

(use-package w3m
  :defer t
  :general
  (general-define-key
   :prefix my-leader-1
   "w" #'w3m)
  (general-define-key
   :keymaps '(w3m-mode-map)
   :states '(normal)
   "o" #'w3m-goto-url
   "t" #'w3m-goto-url-new-session
   "J" #'w3m-view-previous-page
   "K" #'w3m-view-next-page
   "go" (lambda () (interactive) (jay/browse-url-generic t w3m-current-url))
   "r" #'w3m-reload-this-page
   "<tab>" #'w3m-next-anchor
   "<C-tab>" #'w3m-previous-anchor
   "Y" (lambda () (interactive) (kill-new w3m-current-url))
   "RET" #'w3m-view-this-url
   "<M-h>" #'w3m-tab-move-left
   "<M-l>" #'w3m-tab-move-right
   "L" #'w3m-tab-next-buffer
   "H" #'w3m-tab-previous-buffer
   "x" #'w3m-delete-buffer
   "b" nil
   "q" #'quit-window
   "T" #'w3m-toggle-inline-images)
  :config
  (evil-set-initial-state 'w3m-mode 'normal)
  (setq w3m-search-default-engine "duckduckgo"
        w3m-default-display-inline-images t
        w3m-confirm-leaving-secure-page nil))

(provide 'w3m-config)

;;; w3m-config.el ends here

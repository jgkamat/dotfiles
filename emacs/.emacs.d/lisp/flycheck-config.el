;;; flycheck-config.el --- My personal Flycheck Config File

;; Copyright (C) 2014-2019 Jay Kamat
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;; Flycheck specific configs

;;; Code:


(use-package flycheck
  :defer t
  :init
  (defun turn-on-flycheck ()
    "Force enables flycheck."
    (interactive)
    (flycheck-mode 1))
  ;; turn on flycheck mode
  (unless (or (eq system-type 'cygwin)
              (string= (system-name) "gilly"))
    (add-hook 'prog-mode-hook #'turn-on-flycheck)
    (add-hook 'yaml-mode-hook #'turn-on-flycheck))

  :config
  (setq flycheck-idle-change-delay 1.5
        flycheck-highlighting-mode 'symbols)

  ;; Use current Emacs load path
  (setq-default flycheck-emacs-lisp-load-path 'inherit)

  (add-hook 'c++-mode-hook
	          (lambda ()
		          ;; Make sure we're building with cpp 14
		          (setq flycheck-gcc-language-standard "c++17"
			              flycheck-clang-language-standard "c++17")))


  ;; Set flycheck to use pylint3 when possible
  (when (executable-find "pylint3")
    (setq flycheck-python-pylint-executable "pylint3"))
  (setq flycheck-python-pycompile-executable "python3")

  ;; TODO fix flycheck-add-next-checker to avoid adding dupes
  ;; for python, disable standard checkers entirely
  (setq flycheck-checkers
        (cons 'python-mypy (delq 'python-mypy flycheck-checkers)))
  (flycheck-add-next-checker 'python-mypy '(t . python-flake8))
  (add-hook
   'python-mode-hook
   (lambda ()
     (when (or (not (stringp buffer-file-name)) (file-remote-p buffer-file-name))
       (add-to-list 'flycheck-disabled-checkers 'python-mypy))))

  ;; Don't check on a newline
  (setq flycheck-check-syntax-automatically
        (delete 'new-line flycheck-check-syntax-automatically))

  (require 'jay-constants)
  (defun jay/power-wrap (func &rest args)
    (unless LOW-POWER-MODE
      (apply func args)))

  (advice-add 'flycheck-buffer :around #'jay/power-wrap))

(use-package flycheck-popup-tip
  :init
  (add-hook 'flycheck-mode-hook 'flycheck-popup-tip-mode)
  :config
  (setq flycheck-popup-tip-error-prefix "-> "))

;; (use-package flycheck-clojure
;;   :defer t
;;   :commands clojure-mode
;;   :config
;;   (flycheck-clojure-setup))

(provide 'flycheck-config)

;;; flycheck-config.el ends here

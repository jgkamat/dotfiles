

;; (when (require 'matrix-client nil 'noerror)
;; 	)

(use-package alert
  :commands (alert)
  :init
  (setq alert-default-style 'libnotify))

(use-package matrix-client
  :defer t
  :ensure nil
  :quelpa (matrix-client
           :fetcher github
           :repo "jgkamat/matrix-client-el")

  :commands (matrix-client-ng-connect)
  :config
  (setq matrix-client-render-membership nil
        matrix-client-render-presence nil
        matrix-client-use-tracking t
        matrix-client-save-token t)

  ;; Turn off debug-on-error when entering the debugger
  (add-hook 'debugger-mode-hook (lambda () (setq debug-on-error nil)))
  (add-hook 'debugger-mode-hook (lambda ()
                                  ;; Turn off global-visual-line-mode
                                  (visual-line-mode -1)
                                  (setq truncate-lines t)))

  ;; (add-hook 'debugger-mode-hook #'matrix-client-disconnect)
  (defun jay/debugger-extract ()
    (interactive)
    (with-current-buffer "*Backtrace*"
      (write-file "~/OUTPUT")))

  ;; (defvar jay/matrix-notify-watchlist)
  ;; (setq jay/matrix-notify-watchlist '("#Emacs:matrix.org"
  ;;                                     "#matrix-client-el:matrix.org"
  ;;                                     "#jgkamat-reentry-testing:matrix.org"))

  ;; (cl-defmethod jay/matrix-notify ((room matrix-client-room) event-type data)
  ;;   (cl-destructuring-bind (body buffer name) (jay/matrix-data-to-list room data)
  ;;     (let* ((high-pri
  ;;             ;; Find if we have any keywords matching in the body
  ;;             (or
  ;;              (cl-find-if
  ;;               (lambda (item) (string-match item body))
  ;;               circe-notifications-high-pri-strings)
  ;;              ;; TODO handle privmsg
  ;;              )))
  ;;       (when (or
  ;;              high-pri
  ;;              (and
  ;;               ;; Only notify msgs
  ;;               (string= event-type "m.room.message")
  ;;               ;; Never notify for freenode rooms
  ;;               (not (jay/matrix-room-matches room "^#freenode_"))
  ;;               ;; Only notify for things on the whitelist (for now)
  ;;               (cl-find-if (apply-partially #'jay/matrix-room-matches room)
  ;;                           jay/matrix-notify-watchlist)))
  ;;         (alert
  ;;          body
  ;;          :severity (if high-pri 'high 'low)
  ;;          :title (format "%s (%s)" name (buffer-name buffer)))))))

  ;; (defun jay/matrix-data-to-list (room data)
  ;;   "Convert DATA to (list body buffer sender)"
  ;;   (pcase-let* (((map content sender event_id) data)
  ;;                ((map body) content)
  ;;                (display-name (matrix-client-displayname-from-user-id room sender))
  ;;                (buffer (oref room :buffer)))
  ;;     (list body buffer display-name)))

  ;; (defun jay/matrix-room-matches (room room-str)
  ;;   "Check if room-str matches a room-list"
  ;;   (with-slots (aliases id) room
  ;;     (or
  ;;      (cl-find room-str aliases :test #'string=)
  ;;      (string= id room-str))))
  ;; (defun jay/matrix-room-matches-regexp (room room-str)
  ;;   "Check if room-str matches a room-list"
  ;;   (with-slots (aliases id) room
  ;;     (or
  ;;      (cl-find room-str aliases :test #'string-match-p)
  ;;      (string-match-p room-str id))))
  ;; (setq matrix-client-notify-dispatcher #'jay/matrix-notify)
  )


(provide 'matrix-config)

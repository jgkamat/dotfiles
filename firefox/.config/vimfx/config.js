
// My config for vimfx
vimfx.set('hints.chars', 'asdfghjkl')

// Scroll speed
const {classes: Cc, interfaces: Ci, utils: Cu} = Components;
let {Preferences} = Cu.import('resource://gre/modules/Preferences.jsm', {});
Preferences.set({
    'toolkit.scrollbox.verticalScrollDistance': 10,
    'toolkit.scrollbox.verticalScrollDistance': 10,
    'extensions.VimFx.prevent_autofocus': true,
    'smoothScroll.lines.spring-constant': '1',
    'smoothScroll.pages.spring-constant': '1',
    'smoothScroll.other.spring-constant': '1',

});

let map = (shortcuts, command, custom=false) => {
  vimfx.set(`${custom ? 'custom.' : ''}mode.normal.${command}`, shortcuts)
}

let imap = (shortcuts, command, custom=false) => {
  vimfx.set(`${custom ? 'custom.' : ''}mode.ignore.${command}`, shortcuts)
}

function disable_cmd ( cmd )	{
    map("", cmd);
}

map('H', 'tab_select_previous')
map('L', 'tab_select_next')
map('J', 'history_back')
map('K', 'history_forward')
map('<a-h>', 'tab_move_backward')
map('<a-l>', 'tab_move_forward')

map('<c-u>', 'scroll_half_page_up')
map('<c-d>', 'scroll_half_page_down')
map('', 'scroll_page_down')

imap('<escape>', 'exit')
map('u', 'tab_restore')

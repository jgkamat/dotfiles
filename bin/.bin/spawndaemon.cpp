#!/usr/bin/env -S c "-std=c++17 -Wall -Werror -lX11 -lXfixes -O2 --"

/*
* A simple daemon to spawn things from the clipboard, with an auth hash ensuring fs access. Hopefully used for spawning
* things from surfingkeys.
* When things are copied to the clipboard with format:
* spawndaemon:<security hash>:<mode>:<args>
* an exec mode of <mode> is used and passed <args>
*/

#include <X11/Xatom.h>
#include <X11/Xlib.h>
#include <X11/extensions/Xfixes.h>
#include <cstdio>
#include <cstdlib>
#include <cstdbool>
#include <ctime>
#include <iostream>
#include <fstream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>
#include <csignal>
#include <functional>
#include <chrono>
#include <thread>
#include <sys/stat.h>

const float THROTTLE_S = 1;
const std::string ALPHABET = "abcdefghijklmnopqrstuvwxyz";
const int PASS_LEN = 40;
const std::string TRIGGER = "spawndaemon:";

Display *disp;

const std::unordered_map<std::string_view, std::function<std::string(std::string_view)>> HANDLERS = {
	{"mpv", [](auto url){return std::string("mpv --profile=web '").append(url).append("'");}}
};

std::string exec(const char* cmd) {
	std::array<char, 128> buffer;
	std::string result;
	std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
	if (!pipe) {
		throw std::runtime_error("popen() failed!");
	}
	while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
		const std::string_view& data = buffer.data();
		if (result.empty()) {
			if (data.rfind(TRIGGER, 0) == std::string::npos)
				// If we don't start with the trigger, bail immediately!
				break;
		}
		result += data;
	}
	return result;
}

std::string genHash() {
	std::string password = "";
	const std::string filename = "/tmp/jay/spawndaemon-hash";
	mkdir("/tmp/jay", 0777);
	for (auto i = 0; i < PASS_LEN; i++)
		password.append(ALPHABET, rand() % ALPHABET.size(), 1);

	std::ofstream file;
	file.open(filename);
	if (!file.is_open()) {
		std::cerr << "Error opening file, trying again!" << std::endl;
		std::this_thread::sleep_for(std::chrono::milliseconds(5000));
		file.open(filename);
		if (!file.is_open()) {
			std::cerr << "We can't really recover from it..." << std::endl;
			XCloseDisplay(disp);
			exit(1);
		}
	}
	file << password;
	file.close();
	return password;
}

void signalHandler(int signum) {
	std::cout << "exiting on request!" << std::endl;
	exit(signum);
	XCloseDisplay(disp);
}

int main() {
	srand(time(nullptr));
	signal(SIGINT, signalHandler);

	Window root;
	Atom clip;
	XEvent evt;

	auto last_run = time(nullptr);
	std::string last_clip = "";

	disp = XOpenDisplay(NULL);
	if (!disp) {
		std::cerr << "Can't open X display\n" << std::endl;
		exit(1);
	}

	// Generate hash for "security"
	const auto password = genHash();

	while (true) {
		// Wait for the next clipboard copy event.
		root = DefaultRootWindow(disp);
		clip = XInternAtom(disp, "CLIPBOARD", False);
		XFixesSelectSelectionInput(disp, root, XA_PRIMARY, XFixesSetSelectionOwnerNotifyMask);
		XFixesSelectSelectionInput(disp, root, clip, XFixesSetSelectionOwnerNotifyMask);
		XNextEvent(disp, &evt);

		if (difftime(time(nullptr), last_run) < THROTTLE_S)
			continue;
		time(&last_run);

		// Get clipboard contents (not selection)
		const auto clip_contents_raw = exec("xclip -sel clip -o");
		std::string_view clip_contents = clip_contents_raw;
		if (clip_contents == last_clip)
			continue;
		last_clip = clip_contents_raw;

		// Split into parts, bail whenever we see anything unexpected.
		if (clip_contents.rfind(TRIGGER, 0) == std::string::npos)
			continue;
		clip_contents = clip_contents.substr(clip_contents.find(":") + 1);

		auto colon_pos = clip_contents.find(":");
		if (colon_pos == std::string::npos)
			continue;
		const auto live_pass = clip_contents.substr(0, colon_pos);

		if (live_pass != password)
			continue;
		clip_contents = clip_contents.substr(colon_pos + 1);

		colon_pos = clip_contents.find(":");
		if (colon_pos == std::string::npos)
			continue;

		const auto mode = clip_contents.substr(0, colon_pos);
		const auto args = clip_contents.substr(colon_pos + 1);
		if (args.find("'") != std::string::npos)
			continue;

		const auto handler = HANDLERS.find(mode);
		if (handler == HANDLERS.end())
			continue;

		// We're primed, go!
		auto final_command = handler->second(args);
		final_command.append(" &");
		std::cout << "Executing: " << final_command << std::endl;
		system(final_command.c_str());
	}
}

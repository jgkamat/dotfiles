#!/bin/sh
pactl load-module module-null-sink
# Come up with slave name from
# pactl list short sinks
pactl load-module module-combine-sink sink_name=combined slaves=null,alsa_output.pci-0000_1e_00.3.analog-stereo

#
# bspwm hotkeys
#

super + shift + q
	bspc quit

super + x
	bspc node -c

# super + t
# 	bspc desktop -l next

super + b
	bspc desktop -B

super + {s,t,u}
	state={pseudo_tiled,floating,fullscreen}; \
	bspc query -N -n "focused.$\{state\}" && state=tiled; \
	bspc node -t "$state"

super + {grave,Tab}
	bspc {node,desktop} -f last

# super + apostrophe
# 	bspc window -s last

super + {o,i}
	bspc control --record-history off; \
	bspc node {older,newer} -f; \
	bspc control --record-history on

super + y
	bspc query -N -n focused.automatic && bspc node -n last.!automatic || bspc node last.leaf -n focused

super + {_,shift + }{h,j,k,l}
	bspc node -{f,s} {west,south,north,east}

# Switch forward/backwards in windows
# Now used by parcellite for Super-Clipboard
# super + {_,shift + }c
# 	bspc node -f {next,prev}

super + {comma,period}
	bspc desktop -C {backward,forward}

super + {g, semicolon}
	bspc desktop -f {prev,next}

super + shift + {g, semicolon}
	bspc node -d {prev,next} -f

super + ctrl + shift + {g, semicolon}
	bspc node -d {prev,next}

super + ctrl + {g, semicolon}
	state={prev,next}; \
	bspc desktop -s $\{state\}; \
	bspc desktop -f $\{state\}

super + ctrl + {h,j,k,l}
	bspc node -p {west,south,north,east}

super + ctrl + {_,shift + }space
	bspc {node -p cancel,desktop -c}

# expand a window by moving one of its side outward
super + alt + {h,j,k,l}
  bspc node -z {left -10 0,bottom 0 10,top 0 -10,right 10 0}

# contract a window by moving one of its side inward
super + alt + shift + {h,j,k,l}
  bspc node -z {right -10 0,top 0 10,bottom 0 -10,left 10 0}

super + ctrl + {1-9}
	bspc node -o 0.{1-9}

super + {_,shift + }{1-9,0}
	bspc {desktop -f,node -d} ^{1-9,10}

# ~button1
# 	bspc pointer -g focus

# super + button{1-3}
# 	; bspc pointer -g {move,resize_side,resize_corner}
#
# super + @button{1-3}
# 	bspc pointer -u

# Monitor

super + ctrl + o
	bspc desktop -m older

super + ctrl + d
	bspc desktop -r

#
# wm independent hotkeys
#

super + Return
	urxvt

super + shift + i
	emacs

super + shift + b
	qutebrowser --backend webengine
	# firefox
	# qutebrowser
	# chromium
	# vivaldi

super + shift + p
	keepassxc

super + p
	dmenu_run -fn "Terminus-9"

# make sxhkd reload its configuration files:
super + q
	pkill -USR1 -x sxhkd


# Sound
XF86AudioLowerVolume
	amixer -q set Master 5%- unmute
XF86AudioRaiseVolume
	amixer -q set Master 5%+ unmute
XF86AudioMute
	amixer -q set Master toggle


super + bracket{left,right}
	amixer -q set Master 5%{-,+} unmute

super + shift + bracket{left,right}
	amixer -q set Master 1%{-,+} unmute

super + v
	amixer -q set Master toggle

# Move mouse
super + m
	xdotool mousemove 9999 9999

# Beastness
super + {minus, equal}
	{brightnessctl s 5-,brightnessctl s +5}
	# {light -U 5 || xbacklight -dec 5,light -A 5 || xbacklight -inc 5}

super + shift + {minus,plus}
	{brightnessctl s 1-,brightnessctl s +1}
	# {light -U 1 || xbacklight -dec 1,light -A 1 || xbacklight -inc 1}

ctrl + alt + l
	xscreensaver-command -lock

ctrl + alt + s
	systemctl suspend

super + shift + s
	jshotfull


#!/bin/bash
# A small script to nuke slack channels with qutebrowser
#
# Unfortunatley you can't use your comp while this is running but whatever.

COUNT=150

while [ $COUNT -gt 1 ]; do
	echo starting
	sleep 5
	echo key 1
	xdotool key f l a d
	sleep 1
	echo key 2
	xdotool key f k j h
	sleep 1
	echo key 3
	xdotool key f l a h
	sleep 1
	echo key 4
	xdotool key f l s g
	echo Waiting
	sleep 30

	COUNT="$(expr $COUNT - 1)"
	echo count is: $COUNT
done

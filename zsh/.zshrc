[ "$TERM" = "dumb" -a -z "$INSIDE_EMACS" ] && unsetopt zle && PS1='$ ' && return

# Set up the prompt

autoload -Uz promptinit
autoload -Uz select-word-style
autoload -U colors && colors
promptinit

# Conflicting with bedrock
# if [ -f /etc/profile ]; then
# 	# Source /etc/profile for changes on every start
# 	# should be fairly fast
# 	source /etc/profile
# fi
if [ -f ~/.zprofile ]; then
	# Source /etc/profile for changes on every start
	# should be fairly fast
	source ~/.zprofile
fi

# Allow for functions in the prompt.
setopt PROMPT_SUBST

# Autoload zsh functions.
if [ -f ~/.zsh_functions ]; then
	fpath=(~/.zsh_functions $fpath)
	autoload -U ~/.zsh_functions(:t)
	source ~/.zsh_functions
fi

# If needed, add a ~/.zsh_auth file to provide some api keys and stuff
if [ -f ~/.zsh_auth ]; then
	source ~/.zsh_auth
fi

# Enable auto-execution of functions.
if [ $TERM != 'linux' ]; then
	typeset -ga preexec_functions
	typeset -ga precmd_functions
	typeset -ga chpwd_functions

	# Make sure keyboard repeat is actually working!
	xset r rate 250 30 2>/dev/null >&2
fi

if [ -z "$INSIDE_EMACS" ] && ! $(echo "$TERM" | grep -q 'eterm.*'); then
	INSIDE_EMACS=false
	# if [ "$(uname)" = "Linux" ]; then
	# 	export TERM='rxvt-unicode-256color'
	# fi
else
	INSIDE_EMACS=true
	export LANG=en_US.UTF-8
	export LC_ALL=en_US.UTF-8
	# Uncomment if ditching term
	unsetopt zle
fi

if [ -n "$SSH_CLIENT" ] || [ -n "$SSH_TTY" ]; then
	SSH_CONNECTED=true
else
	SSH_CONNECTED=false
fi

if $(echo "$TERM" | grep -q 'rxvt'); then
	# Tabs mess up a lot of terminals, so don't change them on anything but urxvt
	# Mainly this breaks cli-visualizer in xterm =)
	tabs -4
fi

if [ "$(uname)" = "Darwin" ] && command -v brew >/dev/null 2>&1; then
	export PATH="$(brew --prefix coreutils)/libexec/gnubin:/usr/local/bin:$PATH"
fi
# export TERM='screen-256color'

# Append git functions needed for prompt.
preexec_functions+='preexec_update_git_vars'
precmd_functions+='precmd_update_git_vars'
chpwd_functions+='chpwd_update_git_vars'

# Set the prompt.
# PROMPT=$'%B%~%b$(prompt_git_info) '

# prompt redhat
PROMPT="%(?,%F{grey}[,%F{red}%B[)%b%f%B%{$fg[red]%}%n%{$reset_color%}@%{$fg[green]%}%m%{$fg[yellow]%}%B %1~%{$reset_color%}%b%b%(?,%F{grey}],%F{red}%B])%b%f%{$fg[red]%}%(#.#.%{$fg[green]%}$)%{$reset_color%} "
# Don't put a right prompt, else we conflict with term line mode
if ! $INSIDE_EMACS; then
	RPROMPT=$'$(prompt_git_info) %{$reset_color%}[%B%{$fg[yellow]%}%h%b%{$reset_color%}] %{$fg[green]%}%t%{$reset_color%}'
fi

if $SSH_CONNECTED; then
	PROMPT="%B%F{yellow}<REMOTE>%F{$reset_color}%b $PROMPT"
fi

# Bash comments
setopt interactivecomments

setopt inc_append_history
# Causes extended history to show up!
# setopt share_history
setopt hist_ignore_dups
setopt hist_ignore_space			# dont write history to file if space
setopt histappend					# append history
setopt hist_reduce_blanks			# Remove extra blanks from each command line being added to history
setopt autopushd					# Auto add to dir stack
setopt nobeep						# Last ditch effort
setopt nohup						# Dont need nohup command
unsetopt extended_history


# setopt extended_history			#nice hist file

# Use vim mode for navigation
# bindkey -v
bindkey -e

# Don't fall for M-x trap!
bindkey "^[x" redisplay

# Real (emacs) commands!
bindkey \^U backward-kill-line
bindkey "^R" history-incremental-search-backward
bindkey "^A" beginning-of-line
bindkey "^B" backward-char
bindkey "^W" backward-kill-word
bindkey "^D" delete-char-or-list
bindkey "^E" end-of-line
bindkey "^F" forward-char
bindkey "^[." insert-last-word
bindkey "^P" up-line-or-history
bindkey "^N" down-line-or-history

bindkey "^[b" backward-word
bindkey "^[B" backward-word

bindkey "^[f" forward-word
bindkey "^[F" forward-word

# WARNING: This makes esc keys almost impossible to hit.
# Turn me off if you need <ESC>b, for example, rather than M-b
KEYTIMEOUT=50

# Vim mode indicator
if [ "$TERM" = "xterm-256color"  -o "$TERM" = "rxvt-unicode-256color" -o "$TERM"  = "screen" ]; then
	# TODO find a way to do this that dosen't sacrifice block cursor
	# function zle-line-init zle-keymap-select {
	# 	VIM_KEYMAP="${${KEYMAP/vicmd/\033[2 q}/(main|viins)/\033[6 q}"
	# 	print -n -- "$VIM_KEYMAP"
	# }
	# zle -N zle-line-init
	# zle -N zle-keymap-select
fi

# FZF search
if command -v ag>/dev/null; then
	export FZF_DEFAULT_COMMAND='ag -l -g ""'
fi

# Keep 100000 lines of history within the shell and save it to ~/.zsh_history:
HISTSIZE=100000
SAVEHIST=100000
HISTFILE=~/.zsh_history

# save history in multiple windows
# avoid duplicates..
# export HISTCONTROL=ignoredups:erasedups
# append history entries..
# After each command, save and reload history
# TODO make this not overwrite the currernt PROMPT_CMD
PROMPT_COMMAND="history -a; history -a; history -r"


# Use modern completion system
autoload -Uz compinit

# WARNING: INSECURE, '-u' added for convenience
compinit -u

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=2
if command -v dircolors >/dev/null 2>&1; then
	eval "$(dircolors -b)"
fi
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true

zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'

# tox use default completion
compdef _default tox

# Hacks for boot
BOOT_JAVA_PATHS=("/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/rt.jar" "/usr/lib64/icedtea8/jre/lib/rt.jar" "/opt/icedtea-bin-7.2.6.8/jre/lib/rt.jar")
for bootpath in ${BOOT_JAVA_PATHS[*]}; do
	if [ -f "$bootpath" ]; then
		export JDK7_BOOTCLASSPATH="$bootpath"
		break
	fi
done
if [ -d "/opt/launch4j" ]; then
	export PATH="$PATH:/opt/launch4j"
fi

# Setup ccache
if [ -d /usr/lib/ccache/bin ]; then
	export PATH="/usr/lib/ccache/bin:$PATH"
fi


# CD to old dir if possible. Don't CD if we started a shell with a non-home dir
if [ "`pwd`" = "${HOME}" ]  && [ -f "/tmp/`whoami`/pwd" ] && [ -d "$(cat /tmp/`whoami`/pwd)" ]; then
	cd "$(cat /tmp/`whoami`/pwd)"
fi

# A function to see if a command exists. Basically cross platform which
exists() {
	command -v $1 >/dev/null
	return $?
}

# Source autojump before aliases
[[ -s /usr/share/autojump/autojump.zsh ]] && source /usr/share/autojump/autojump.zsh
[ -f /opt/homebrew/etc/profile.d/autojump.sh ] && . /opt/homebrew/etc/profile.d/autojump.sh

# set up aliases (default and extras)
source ~/.zsh_aliases

# Pretty colors
[ -f ~/.zsh_termcap ] && source ~/.zsh_termcap

# ctrl-w
select-word-style shell

# Emacs specific config

if $INSIDE_EMACS; then
	# Let's gear up!
	alias vi='emacsclient'
	export EDITOR='emacsclient'
	export PAGER='cat'
elif [ -z "$EDITOR" ]; then
	export EDITOR=vim
fi

# added by travis gem
if ! $INSIDE_EMACS; then
	export FZF_DEFAULT_OPTS="--no-height --no-reverse"
	[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
fi

# GPG agent setup
GPG_TTY=$(tty)
export GPG_TTY

# OPAM configuration
. /home/jay/.opam/opam-init/init.zsh > /dev/null 2> /dev/null || true


# The next line updates PATH for the Google Cloud SDK.
if [ -f '/opt/google-cloud-sdk/path.zsh.inc' ]; then . '/opt/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/opt/google-cloud-sdk/completion.zsh.inc' ]; then . '/opt/google-cloud-sdk/completion.zsh.inc'; fi

# if exists complete; then
# 	complete -W "$(kubectl config get-contexts -o name | sed 's/$/ /' | tr '\n' ' ')" lens
# fi

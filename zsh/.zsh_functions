#!/bin/zsh

# A script for functions.

# Prompt

update_current_git_vars() {
    unset __CURRENT_GIT_BRANCH
    unset __CURRENT_GIT_BRANCH_STATUS
    unset __CURRENT_GIT_BRANCH_IS_DIRTY

    local st="$(git status -uno 2>/dev/null)"
    if [[ -n "$st" ]]; then
        local -a arr
        arr=(${(f)st})

        if [[ $arr[1] =~ 'Not currently on any branch.' ]]; then
            __CURRENT_GIT_BRANCH='no-branch'
        else
            __CURRENT_GIT_BRANCH="${arr[1][(w)4]}";
        fi

        if [[ $arr[2] =~ 'Your branch is' ]]; then
            if [[ $arr[2] =~ 'ahead' ]]; then
                __CURRENT_GIT_BRANCH_STATUS='ahead'
            elif [[ $arr[2] =~ 'diverged' ]]; then
                __CURRENT_GIT_BRANCH_STATUS='diverged'
            else
                __CURRENT_GIT_BRANCH_STATUS='behind'
            fi
        fi

        if [[ ! $st =~ 'nothing to commit' ]]; then
            __CURRENT_GIT_BRANCH_IS_DIRTY='1'
        fi
    fi
}

prompt_git_info() {
    if [ -n "$__CURRENT_GIT_BRANCH" ]; then
        local s="%b%{$reset_color%}[%{$fg[cyan]%}%B"
        s+="$__CURRENT_GIT_BRANCH"
        case "$__CURRENT_GIT_BRANCH_STATUS" in
            ahead)
                s+="↑"
                ;;
            diverged)
                s+="↕"
                ;;
            behind)
                s+="↓"
                ;;
        esac
        if [ -n "$__CURRENT_GIT_BRANCH_IS_DIRTY" ]; then
            s+="⚡"
        fi
        s+="%b%{$reset_color%}]"

        printf "%s" $s
    fi
}

chpwd_update_git_vars() {
    update_current_git_vars
}

preexec_update_git_vars() {
    case "$1" in
        git*)
            __EXECUTED_GIT_COMMAND=1
            ;;
		g*)
			__EXECUTED_GIT_COMMAND=1
			;;
    esac
}

precmd_update_git_vars() {
    if [ -n "$__EXECUTED_GIT_COMMAND" ]; then
        update_current_git_vars
        unset __EXECUTED_GIT_COMMAND
    fi
}

# -- END Prompt --

confirm() {
	# call with a prompt string or use a default
	printf "\n"
	read "response?-Are you sure? [y/N] "
	case $response in
		[yY][eE][sS]|[yY])
			true
			;;
		*)
			false
			;;
	esac
}

check_installed() {
	command -v $1 >/dev/null
	return $?
}


# Emacs tramp updates
function eterm-update {
	# Only set the full hostname for ssh sessions.
	if [[ -n "$SSH_CONNECTION" ]]; then
		# For ssh connections, use the hostname (it is assumed here
		# that there is an ssh alias on the connecting machine that has
		# the same name as the ssh alias).
		echo -e "\033AnSiTh" "$(hostname)"
	else
		# For local sessions, use the full host name so tramp will know
		# that the path is not remote.
		echo -e "\033AnSiTh" "$(hostname -f)"
	fi

	echo -e "\033AnSiTu" "$LOGNAME" # $LOGNAME is more portable than using whoami.
	echo -e "\033AnSiTc" "$(pwd)"
}


# Save location of cd every time.
chpwd() {
	# emulate -L zsh
	if [ -d "`pwd`" ]; then
		mkdir -p /tmp/`whoami`
		echo "`pwd`" > /tmp/`whoami`/pwd
	fi
	# We can't check for $EMACS since we're remoting in.
	if [ "$TERM" = "eterm-color" ]; then
		# eterm-update
	fi
}

# be careful
if command -v dd>/dev/null 2>&1; then
	alias dd='lsblk && confirm && sudo /usr/bin/env dd status=progress $@'
fi

if command -v vi >/dev/null 2>&1; then
	# Wrapper function for vi (so vi will cd as well)
	function vi() {
		if [ -n "$1" -a -d "$1" ]; then
			cd $1
		else
			if command -v nvim >/dev/null 2>&1; then
				env nvim "$@"
			elif command -v vim >/dev/null 2>&1; then
				env vim "$@"
			else
				env vi "$@"
			fi
		fi
	}
fi

# docker remove all images/containers
if check_installed docker; then
	drmc() {
		docker rm $@ $(docker ps -a -q)
	}
	drmi() {
		docker rmi $@ $(docker images --format "{{.Repository}}:{{.Tag}}" | fgrep -v '<none>')
		docker rmi $@ $(docker images -q)
	}
	dockertree() {
		docker run --rm -v /var/run/docker.sock:/var/run/docker.sock nate/dockviz images -t
	}
fi


# create dummy PRs for perf workflow
dummy-pr(){
  cd ${HOME}/universe
  BRANCH_UUID=$(uuidgen | cut -c1-6)
  BRANCH_TS=$(date +%Y%m%d%H%M)
  BRANCH_NAME="PerfWorkflows_${BRANCH_TS}_${BRANCH_UUID}"
  git checkout master && git fetch databricks master && git pull --rebase databricks master
  git checkout -b $BRANCH_NAME databricks/master
  git commit --allow-empty -m "$BRANCH_NAME Dummy PR"
  # git push origin HEAD:$BRANCH_NAME
  git pp
}

#!/bin/zsh

# A function to see if a command exists. Basically cross platform which
exists() {
	command -v $1 >/dev/null
	return $?
}

# set ls colors and hyperlinks
alias ls='ls --color=always --hyperlink=auto'
# Fix broken colors
LS_COLORS="$LS_COLORS:ow=7;32"

# ls aliases
alias ll='ls -l'
alias la='ls -la'

# cp fast
alias cp='cp --reflink=auto'

# start redshift only with configuration files
alias redshift='redshift -c ~/.redshiftrc'

# When using sudo, use alias expansion (otherwise sudo ignores your aliases)
# Preserve sudo XDG as well
# alias sudo='sudo env XDG_CONFIG_HOME=$XDG_CONFIG_HOME '
alias sudo='sudo '
alias s='sudo '
alias se='sudoedit'

# redshift toggle
alias togredshift='pkill -USR1 redshift'

if exists xdg-open; then
	open() {
		xdg-open $1 &> /dev/null &
	}
fi

alias icat='kitty +kitten icat'

# alias calc to qualculate-gtk
alias calc='qalc'

# make sl even better
alias cls='clear'

if exists popd; then
	# alias popd, pushd happens automagically
	alias jk='popd'
fi

# start emacs without desktop if started by cli
alias em='emacs'
alias emacs='emacs --no-desktop'
alias ec='emacsclient'
alias emacsclient='emacsclient -n'

# lisp
if exists sbcl; then
	alias cl='sbcl'
	if exists rlwrap; then
		alias sbcl='rlwrap sbcl --noinform'
	fi
fi

# Use nvim when possible
if exists nvim; then
       alias vim='nvim'
elif exists vimx; then
       alias vim='vimx'
fi

if exists optirun; then
	alias nvidia-smi='optirun nvidia-smi'
fi

# Heh.
if exists vi; then
	alias vii='vi'
fi

# Heh. Heh,
alias nano='vim'

if exists docker; then
	alias d='docker'
fi

if exists lab; then
	GIT_CMD="lab"
elif exists hub; then
	GIT_CMD="hub"
else
	GIT_CMD="git"
fi


if exists git; then
	alias g='git'
	alias gs='git status'
	alias gr='cd $(git rev-parse --show-toplevel)'
fi

if exists make; then
	alias m='make'
fi

alias py='python'
if exists python3; then
	# alias python='python3'
	alias py3='python3'
fi

if exists python2; then
	alias py2='python2'
fi

if exists feh; then
	alias feh='feh --scale-down'
fi

if exists xclip; then
	alias xclip='xclip -sel clip'
fi

# Always use gpg2
if exists gpg2; then
	alias gpg='gpg2'
fi

alias grep='grep --color=always'

if exists dmesg; then
	alias dmesg='dmesg --color=always'
fi

if exists rg; then
	alias rg='rg -S --color=always --ignore-file $HOME/.config/ripgrep/ignore'
fi

# systemd
if exists systemctl; then
	alias sc='systemctl'
	alias scu='systemctl --user'
	alias jc='journalctl'
	alias jcu='journalctl --user'
fi

# image viewer
if exists pqiv; then
	alias iv='pqiv'
fi

if exists circleci; then
	alias ci='circleci'
fi

if exists python3; then
	alias py='python3'
elif exists python; then
	alias py='python'
fi

if exists stow; then
	alias stow-all='stow $(find * -maxdepth 0 -type d)'
fi

if exists emerge; then
	# alias eupgrade='emerge -uaDN --with-bdeps=y @world'
	# No new use flags
	alias eupgrade='sudo emerge -uaND --with-bdeps=y @world'
	# alias esync='sudo emerge --sync --ask=n'
	alias esync='sudo emerge-webrsync'
fi

if ! exists brw && exists brl; then
	alias brw='brl which'
fi

if exists bpftrace; then
	alias bt='bpftrace'
fi

if exists perf; then
	alias perfdoctorme='sudo sh -c "echo 0 >/proc/sys/kernel/perf_event_paranoid"'
	alias unperfdoctorme='sudo sh -c "echo 3 >/proc/sys/kernel/perf_event_paranoid"'
fi

if exists quilt; then
	alias dquilt="quilt --quiltrc=${HOME}/.quiltrc-dpkg"
fi

if exists buck && [ "${INSIDE_EMACS:-false}" != "false" ]; then
	alias buck='TERM=dumb buck'
fi
if exists fbpkg && [ "${INSIDE_EMACS:-false}" != "false" ]; then
	alias fbpkg='TERM=dumb fbpkg'
fi

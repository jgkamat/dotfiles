# set-option -g mouse-select-pane on
# set-option -g mouse-select-window on
# set-window-option -g mode-mouse on

# vim bindings
set-window-option -g mode-key vi

# fixes colors?
# set -g default-terminal "screen-256color"

# fixes nvim lag
set -s escape-time 0

# Set prefix key to Ctrl-a
# unbind-key C-b
# set-option -g prefix C-a

# make split panes easier!
bind-key \ split-window -h # Split panes horizontal
bind-key - split-window -v # Split panes vertically

# quick movement
is_vim='echo "#{pane_current_command}" | grep -iqE "(^|\/)g?(view|n?vim?)(diff)?$"'
bind -n C-h if-shell "$is_vim" "send-keys C-h" "select-pane -L"
bind -n C-j if-shell "$is_vim" "send-keys C-j" "select-pane -D"
bind -n C-k if-shell "$is_vim" "send-keys C-k" "select-pane -U"
bind -n C-l if-shell "$is_vim" "send-keys C-l" "select-pane -R"
bind -n C-\ if-shell "$is_vim" "send-keys C-\\" "select-pane -l"
bind-key n select-window -n
bind-key p select-window -p

# Broadcast to all panes on C-b b
bind-key b setw synchronize-panes

# send the prefix to client inside window
bind-key C-a send-prefix

## TODO come up with a better keybind for this that won't conflict with my emacs.
bind-key -n M-g prev
bind-key -n M-'\;' next

# Let cursor shape change in tmux
set -g -a terminal-overrides ',*:Ss=\E[%p1%d q:Se=\E[2 q'

# --- colors (solarized dark)
# default statusbar colors
set -g status-bg black
set -g status-fg yellow
set -g status-attr default

# default window title colors
setw -g window-status-fg brightblue
setw -g window-status-bg default

# active window title colors
setw -g window-status-current-fg yellow
setw -g window-status-current-bg default
setw -g window-status-current-attr dim

# Start windows and panes at 1, not 0
set -g base-index 1
setw -g pane-base-index 1

# pane border
set -g pane-border-fg black
set -g pane-border-bg default
set -g pane-active-border-fg yellow
set -g pane-active-border-bg default

# command line/message text
set -g message-bg black
set -g message-fg yellow

# pane number display
set -g display-panes-active-colour yellow
set -g display-panes-colour brightblue

# clock
setw -g clock-mode-colour yellow
# --- end colors

* MechJay [[https://gitlab.com/jgkamat/dotfiles/pipelines][file:https://gitlab.com/jgkamat/dotfiles/badges/master/pipeline.svg?style=shield]]

This is the source repository for the MechJay Automation System.

While not perfect, this system saves many hours of time per day, both by
improving efficiency and by simply being a more pleasant environment to work in.

MechJay is split up into individual stow modules, so you can apply configuration
for individual applications at a time.

* Install

MechJay uses [[https://www.gnu.org/software/stow/][GNU Stow]]. Edit the [[file:.stowrc][stowrc]] file to point to your home directory and:

#+BEGIN_SRC shell
stow */
#+END_SRC

You can add ~--adopt~ to the command to overwrite existing files (by bringing
them into this repo), or you can specify only the directories you wish by
listing them instead of using the glob.

To delete broken symlinks (maybe from a migration) run this:

#+BEGIN_SRC shell
find -L ${HOME} -maxdepth 0 -type l -delete
#+END_SRC

* Programs

Here are a list of notable applications used. It's probably outdated.

| Category      | Application                    |
|---------------+--------------------------------|
| OS            | bedrock {debian,fedora,gentoo} |
| WM            | awesome                        |
| Web           | firefox + tridactyl            |
| Editor        | emacs + evil                   |
| File Browsing | dired/shell                    |
| irc           | circe                          |
| PDF           | zathura                        |
| Images        | pqiv                           |
| Video         | mpv                            |
| Shell         | zsh/shell.el/eshell            |
| Music         | pianobar.el                    |
| Email         | mu4e                           |
| Art           | Krita                          |

* Screenshots
Here are some screenshots so you can get a feel for MechJay. They're probably outdated.
** Clean
[[https://i.imgur.com/ov9u71A.jpg]]
** qutebrowser
[[https://i.imgur.com/3kVis02.png]]
** Terminals
[[https://i.imgur.com/NfiDcRE.jpg]]
** Emacs
[[https://i.imgur.com/XK0xGR2.jpg]]
** Bedrock
[[https://i.imgur.com/hP0kNOG.png]]
* License

Everything is GPLv3 unless stated otherwise in the file or directory.
There are a few GPLv2 components in here as well.

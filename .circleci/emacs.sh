#!/bin/bash

set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$DIR"

emacs -nw --no-desktop \
	--load ./noninteractive.el \
	--load ../emacs/.emacs.d/init.el \
	--batch -f toggle-debug-on-error -f batch-byte-compile \
	../emacs/.emacs.d/init.el

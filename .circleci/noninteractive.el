;;; noninteractive.el --- A script to be sourced when starting CI

;;; Commentary:

;; A simple script with elisp starting CI

;;; Code:
(defun y-or-n-p (&rest _)
  "Overrides 'y-or-n-p'."
   t)
(defun yes-or-no-p (&rest _)
  "Overrides 'yes-or-no-p'."
   t)

;; Try to work around elpa not loading
(defvar CI-RUNNING t)

(provide 'noninteractive)

;;; noninteractive.el ends here

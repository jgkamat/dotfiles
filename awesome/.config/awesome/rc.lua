-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
-- local naughty = require("naughty")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup").widget
local vicious = require("vicious")
local dpi = require("beautiful.xresources").apply_dpi

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
beautiful.init(awful.util.get_themes_dir() .. "zenburn/theme.lua")
beautiful.get().font = "Rounded Mplus 1c, Sans 9"
beautiful.border_width=1
beautiful.get().bg_normal = "#262626"
beautiful.get().progressbar_bg = "#262626"
beautiful.get().fg_normal = "#909396"
beautiful.get().taglist_bg_focus = "#5e6263"

beautiful.get().fg_focus   = "#D2D8D9"
-- beautiful.get().fg_focus   = "black"
beautiful.get().fg_urgent  = "#262626"
beautiful.get().bg_normal  = "#262626"
beautiful.get().bg_focus   = "#373a3a"
beautiful.get().bg_urgent  = "#bd3832"
beautiful.get().progressbar_fg = "#bd3832"

-- TODO find a way to toggle focus instead of mouse on ctrl-j/k
-- awful.screen.default_focused_args = {client=true, mouse=false}

-- See https://github.com/awesomeWM/awesome/pull/1191
local function adjust_borders(c, t)
	local s = nil
    local max = nil
    local only_one = nil
	local clients = nil
	if c ~= nil then
		-- Use currently selected screen/tag
		s = c.screen
		max = s.selected_tag.layout.name == "max"
		clients = s.clients
		only_one = #s.clients == 1
	elseif t ~= nil then
		-- Use given tag (even if not selected)
		s = nil
		max = t.layout.name == "max"
		clients = t:clients()
		only_one = #clients == 1
	end
    for _, c in pairs(clients) do
        if (max or only_one) and not c.floating or c.maximized then
            c.border_width = 0
        else
            c.border_width = beautiful.border_width
        end
    end
end

client.connect_signal("manage", function(c)
	adjust_borders(c, nil)
end)
client.connect_signal("unmanage", function(c)
	adjust_borders(c, nil)
end)
client.connect_signal("property::maximized", function(c)
	adjust_borders(c, nil)
end)
client.connect_signal("tagged", function(c, t)
	adjust_borders(nil, t)
end)
client.connect_signal("untagged", function(c, t)
	adjust_borders(nil, t)
end)
tag.connect_signal("property::layout", function(t)
	adjust_borders(nil, t)
end)

-- This is used later as the default terminal and editor to run.
terminal = "kitty -1"
editor = os.getenv("EDITOR") or "editor"
user = os.getenv("USER") or "jay"
editor_cmd = terminal .. " " .. editor

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
    awful.layout.suit.tile,
    awful.layout.suit.max,
    awful.layout.suit.spiral,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.tile.top,
    awful.layout.suit.floating,
    -- awful.layout.suit.fair,
    -- awful.layout.suit.fair.horizontal,
    -- awful.layout.suit.spiral.dwindle,
    -- awful.layout.suit.max.fullscreen,
    -- awful.layout.suit.magnifier,
    -- awful.layout.suit.corner.nw,
    -- awful.layout.suit.corner.ne,
    -- awful.layout.suit.corner.sw,
    -- awful.layout.suit.corner.se,
}
-- }}}

-- {{{ Helper functions
local function client_menu_toggle_fn()
    local instance = nil

    return function ()
        if instance and instance.wibox.visible then
            instance:hide()
            instance = nil
        else
            instance = awful.menu.clients({ theme = { width = 250 } })
        end
    end
end
-- }}}

-- {{{ Menu
-- Create a launcher widget and a main menu
myawesomemenu = {
   { "hotkeys", function() return false, hotkeys_popup.show_help end},
   { "manual", terminal .. " -e man awesome" },
   { "edit config", editor_cmd .. " " .. awesome.conffile },
   { "restart", awesome.restart },
   { "quit", function() awesome.quit() end}
}

myfavicon = "/home/" .. user .. "/dotfiles/awesome/.config/favicon.png"

mymainmenu = awful.menu({ items = { { "awesome", myawesomemenu, myfavicon },
							  { "open terminal", terminal },
							  { "screenshot", function () awful.spawn("jshot") end }
}
					   })

mylauncher = awful.widget.launcher({
		image = myfavicon,
		menu = mymainmenu })

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- Keyboard map indicator and switcher
mykeyboardlayout = awful.widget.keyboardlayout()

-- {{{ Wibar
-- Create a textclock widget
mytextclock = wibox.widget.textclock("%r %a, %Y-%m-%d ", 1)

-- Create a wibox for each screen and add it
local taglist_buttons = awful.util.table.join(
                    awful.button({ }, 1, function(t) t:view_only() end),
                    awful.button({ modkey }, 1, function(t)
                                              if client.focus then
                                                  client.focus:move_to_tag(t)
                                              end
                                          end),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, function(t)
                                              if client.focus then
                                                  client.focus:toggle_tag(t)
                                              end
                                          end)
                    -- awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
                    -- awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
                )

local tasklist_buttons = awful.util.table.join(
                     awful.button({ }, 1, function (c)
                                              -- if c == client.focus then
                                              --     c.minimized = true
                                              -- else
                                              --     -- Without this, the following
                                              --     -- :isvisible() makes no sense
                                              --     c.minimized = false
                                              --     if not c:isvisible() and c.first_tag then
                                              --         c.first_tag:view_only()
                                              --     end
                                              --     -- This will also un-minimize
                                              --     -- the client, if needed
                                              --     client.focus = c
                                              --     c:raise()
                                              -- end

											client.focus = c
											c:raise()
                                          end),
                     awful.button({ }, 3, client_menu_toggle_fn()),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                          end))

hostname = io.popen("uname -n"):read()
if hostname == "laythe" then
	beautiful.get().wallpaper = "/home/jay/MEGA/Pictures/Wallpapers/KSP/munkerbin.png"
elseif hostname == "eve" then
	beautiful.get().wallpaper = "/home/jay/MEGA/Pictures/Wallpapers/OneShot/NikoDark.png"
elseif hostname == "vall" then
	beautiful.get().wallpaper = "/home/jgkamat/Pictures/wallpapers/kspjool.jpg"
end
local function set_wallpaper(s)
    -- Wallpaper
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        -- If wallpaper is a function, call it with the screen
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
		if s.geometry.width < s.geometry.height then
			-- Vertical wallpaper
			wallpaper = "/home/jay/MEGA/Pictures/Wallpapers/Libretta/Libretta-Libra.png"
		end
        gears.wallpaper.maximized(wallpaper, s, true)
    end
end

-- Vicious Widgets
color_normal =   '#909396'
color_good =     '#598249'
color_degraded = '#B68800'
color_bad =      '#bd3832'
local function format_color(str, value, deg_value, bad_value, dec, sep)
	bad_value = bad_value or 90
	deg_value = deg_value or 80
	sep = sep or '|'
	dec = dec or false
	value = value or 0
	color = color_normal
	if dec then
		if value < bad_value then
			color = color_bad
		elseif value < deg_value then
			color = color_degraded
		end
	else
		if value > bad_value then
			color = color_bad
		elseif value > deg_value then
			color = color_degraded
		end
	end
	return '<span color="' .. color .. '">' .. str .. "</span>" ..
		'<span color="' .. color_good .. '">' .. sep .. '</span>'
end


numcpus = vicious.call(
	vicious.widgets.cpu, function(w, args)
		return #args - 1
end)

local cpuwidget = wibox.container.rotate()
cpuwidget.direction = 'east'
cpuwidget.widget = wibox.layout.flex.vertical()
cpuwidget.widget.max_widget_size = 4
cpu_arr = {}
for i = 1, numcpus do
	local cpuprogress = wibox.widget.progressbar()
	cpuwidget.widget:add(cpuprogress)
	cpu_arr[#cpu_arr + 1] = cpuprogress
end
cputimer = gears.timer {
    autostart = true,
    timeout   = 1,
}
cputimer:connect_signal(
	"timeout",
	function()
		local cpuvals = vicious.call(
			vicious.widgets.cpu, function(a, widget)
				widget[1] = nil
				return widget
		end)
		-- print(cpuwidget:get_widget().children[1]:set_value(1))
		-- cpuwidget[1][1].widget:set_value(0.5)
		-- widget.widget:set_value(index / 10)
		for index, widget in pairs(cpu_arr) do
			if index ~= 'layout' then
				widget:set_value(cpuvals[index + 1] / 100)
			end
		end
	end
)

memwidget = wibox.widget.textbox()
vicious.register(memwidget, vicious.widgets.mem,
				 function (widget, args)
					 return format_color("|ラム:" .. args[1] .. "%", args[1])
				 end, 3)

batterywidget = wibox.widget.textbox()
vicious.register(batterywidget, vicious.widgets.bat,
				 function (widget, args)
					 val = args[1] .. args[2] .. "%"
					 if args[3] ~= "N/A" then
						 val = val .. " " .. args[3]
					 end
					 return format_color(val, args[2], 25, 15, true)
				 end, 59, "BAT0")

find_interface = function ()
	for line in io.lines("/proc/net/dev") do
		for iface in string.gmatch(line, "[%s]*([%a%d]+):") do
			if string.find(iface, "wlan") or string.find(iface, "wlp") then
				return iface
			end
		end
	end
end

netwidget = wibox.widget.textbox()
vicious.register(netwidget, vicious.widgets.wifiiw,
				 function (widget, args)
					 -- return args["mode"]
					 if args['{ssid}'] == nil or args['{ssid}'] == 'N/A' then
						 return '<span color="' .. color_bad .. '">' .. "???" .. '</span>' .. '|'
					 end
					 return '<span color="' .. color_good .. '">' ..
						 args['{ssid}'] .. '@' .. math.min(99, args['{linp}']) .. '|' ..
						 '</span>'
				 end, 3, find_interface() or "wlan0")

volumewidget = wibox.widget.textbox()
vicious.register(volumewidget, vicious.widgets.volume,
				 function(widget, args)
					 local val = 0
					 if args[2] == "🔈" then
						 val = 2000
					 elseif args[1] < 10 then
						 val = 1000
					 end
					 return  format_color(args[1] .. args[2], val, 500, 1500)
				 end, 3, "Master")

micwidget = wibox.widget.textbox()
vicious.register(micwidget, vicious.widgets.volume,
				 function(widget, args)
					 local val = 0
					 local noise = "🎤"
					 if args[2] == "🔈" then
						 val = 2000
						 noise = "🔌"
					 elseif args[1] < 10 then
						 val = 1000
					 end
					 return  format_color("R:" .. noise, val, 500, 1500)
				 end, 3, "Capture")

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

awful.screen.connect_for_each_screen(function(s)
    -- Wallpaper
    set_wallpaper(s)

    -- Each screen has its own tag table.
	if s.geometry.width > s.geometry.height then
		awful.tag({ "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }, s, awful.layout.layouts[1])
	else
		awful.tag({ "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }, s, awful.layout.layouts[5])
	end

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(awful.util.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, taglist_buttons)

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, tasklist_buttons)

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "top", screen = s, height = dpi(20) })

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            mylauncher,
            s.mytaglist,
            s.mypromptbox,
        },
        s.mytasklist, -- Middle widget
        { -- Right widgets
			cpuwidget,
			memwidget,
			batterywidget,
			netwidget,
			volumewidget,
			micwidget,
            mytextclock,
            layout = wibox.layout.fixed.horizontal,
            -- mykeyboardlayout,
            wibox.widget.systray(),
            s.mylayoutbox,
        },
    }
end)
-- }}}

-- {{{ Mouse bindings
root.buttons(awful.util.table.join(
    awful.button({ }, 3, function () mymainmenu:toggle() end)
    -- awful.button({ }, 4, awful.tag.viewnext),
    -- awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}

-- {{{ Key bindings
globalkeys = awful.util.table.join(
    awful.key({ modkey, "Mod1"    }, "h",      hotkeys_popup.show_help,
              {description="show help", group="awesome"}),
    awful.key({ modkey,           }, "Left",   awful.tag.viewprev,
              {description = "view previous", group = "tag"}),
    awful.key({ modkey,           }, "Right",  awful.tag.viewnext,
              {description = "view next", group = "tag"}),
    -- awful.key({ modkey,           }, "Escape", awful.tag.history.restore,
    --           {description = "go back", group = "tag"}),

    awful.key({ modkey,           }, "j",
        function ()
            awful.client.focus.byidx( 1)
        end,
        {description = "focus next by index", group = "client"}
    ),
    awful.key({ modkey,           }, "k",
        function ()
            awful.client.focus.byidx(-1)
        end,
        {description = "focus previous by index", group = "client"}
    ),
    -- awful.key({ modkey,           }, "w", function () mymainmenu:show() end,
    --           {description = "show main menu", group = "awesome"}),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end,
              {description = "swap with next client by index", group = "client"}),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end,
              {description = "swap with previous client by index", group = "client"}),
    -- awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end,
    --           {description = "focus the next screen", group = "screen"}),
    awful.key({ modkey, "Mod1" }, "j", function () awful.screen.focus_relative( 1) end,
              {description = "focus the next screen", group = "screen"}),
    -- awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end,
    --           {description = "focus the previous screen", group = "screen"}),
    awful.key({ modkey, "Mod1" }, "k", function () awful.screen.focus_relative(-1) end,
              {description = "focus the previous screen", group = "screen"}),
	-- TODO find a key for this..
    -- awful.key({ modkey, "Shift" }, "s",
	-- 	function ()
	-- 		local s = awful.screen.focused()
	-- 		awful.screen.focus_relative(1)
	-- 		s:swap(awful.screen.focused())
	-- 	end,
	-- 	{description = "swap with next screen", group = "screen"}),
	awful.key({ modkey, "Shift"   }, "u", awful.client.urgent.jumpto,
              {description = "jump to urgent client", group = "client"}),
    awful.key({ modkey,           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end,
        {description = "go back", group = "client"}),

    -- Standard program
    awful.key({ modkey,           }, "Return", function () awful.spawn(terminal) end,
              {description = "open a terminal", group = "launcher"}),
    -- awful.key({ modkey,   "Shift" }, "b", function () awful.spawn("qutebrowser --loglines=0") end,
    --           {description = "launch browser", group = "launcher"}),
    -- awful.key({ modkey,   "Shift" }, "b", function () awful.spawn("chromium --force-dark-mode") end,
    --           {description = "launch browser", group = "launcher"}),
    awful.key({ modkey,   "Shift" }, "b", function () awful.spawn("firefox") end,
              {description = "launch browser", group = "launcher"}),
    -- awful.key({ modkey,   "Shift" }, "p", function () awful.spawn("firefox --private-window") end,
    --           {description = "launch browser", group = "launcher"}),
    awful.key({ modkey,   "Shift" }, "e", function () awful.spawn("emacs") end,
              {description = "launch emacs", group = "launcher"}),


    awful.key({ modkey,           }, "s", function () awful.spawn("jshot") end,
              {description = "Take a screenshot", group = "launcher"}),
    awful.key({ modkey,   "Shift" }, "s", function () awful.spawn("jshotfull") end,
              {description = "Take a fullscreen shot", group = "launcher"}),

    awful.key({ "Control",   "Mod1" }, "l", function () awful.spawn("i3lock -c 262626") end,
              {description = "lock screen", group = "launcher"}),
    awful.key({ "Control",   "Mod1" }, "s", function () awful.spawn("jsleep") end,
              {description = "sleep", group = "launcher"}),
    awful.key({ "Control",   "Mod1" }, "i", function () awful.spawn("jlights") end,
              {description = "sleep", group = "launcher"}),
    -- awful.key({ "Control",   "Mod1" }, "d", function () awful.spawn("systemctl hibernate") end,
    --           {description = "hibernate", group = "launcher"}),
    -- awful.key({ modkey, }, "m", function () awful.spawn("xdotool mousemove_relative 0 99999999") end,
    --           {description = "move mouse away", group = "launcher"}),
    awful.key({ modkey, }, "m", function () awful.spawn("mpc toggle") end,
              {description = "Toggle mpd music", group = "launcher"}),
    awful.key({ modkey }, "]", function () awful.spawn("amixer -q set Master 5%+ unmute") end),
    awful.key({}, "XF86AudioRaiseVolume", function () awful.spawn("amixer -q set Master 5%+ unmute") end),
    awful.key({ modkey }, "[", function () awful.spawn("amixer -q set Master 5%- unmute") end),
    awful.key({}, "XF86AudioLowerVolume", function () awful.spawn("amixer -q set Master 5%- unmute") end),
    awful.key({ modkey, "Shift" }, "]", function () awful.spawn("amixer -q set Master 1%+ unmute") end),
    awful.key({ modkey, "Shift" }, "[", function () awful.spawn("amixer -q set Master 1%- unmute") end),
    awful.key({ modkey }, "v", function () awful.spawn("amixer -q set Master toggle") end),
    awful.key({ modkey }, "XF86AudioMute", function () awful.spawn("amixer -q set Master toggle") end),
    awful.key({ modkey }, "r", function () awful.spawn("amixer -q set Capture toggle") end),
    awful.key({ modkey }, "-", function () awful.spawn("brightnessctl s 5-") end),
    awful.key({ modkey }, "=", function () awful.spawn("brightnessctl s 5+") end),
    awful.key({ modkey, "Shift" }, "-", function () awful.spawn("brightnessctl s 1-") end),
    awful.key({ modkey, "Shift" }, "=", function () awful.spawn("brightnessctl s 1+") end),

    awful.key({ modkey }, "n", function () awful.spawn("dunstctl close") end),
    awful.key({ modkey, "Ctrl" }, "n", function () awful.spawn("dunstctl history-pop") end),
    awful.key({ modkey, "Shift" }, "n", function () awful.spawn("dunstctl close-all") end),

    awful.key({ modkey, "Shift" }, "r", awesome.restart,
              {description = "reload awesome", group = "awesome"}),
    -- awful.key({ modkey, "Shift"   }, "q", awesome.quit,
    --           {description = "quit awesome", group = "awesome"}),

    awful.key({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)          end,
              {description = "increase master width factor", group = "layout"}),
    awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)          end,
              {description = "decrease master width factor", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1, nil, true) end,
              {description = "increase the number of master clients", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1, nil, true) end,
              {description = "decrease the number of master clients", group = "layout"}),
    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1, nil, true)    end,
              {description = "increase the number of columns", group = "layout"}),
    awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1, nil, true)    end,
              {description = "decrease the number of columns", group = "layout"}),
    awful.key({ modkey,           }, "space", function () awful.layout.inc( 1)                end,
              {description = "select next", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(-1)                end,
              {description = "select previous", group = "layout"}),

    awful.key({ modkey, "Control" }, "u",
              function ()
                  local c = awful.client.restore()
                  -- Focus restored client
                  if c then
                      client.focus = c
                      c:raise()
                  end
              end,
              {description = "restore minimized", group = "client"}),

	-- Workspaces
    awful.key({ modkey,  }, ";", awful.tag.viewnext,
              {description = "move to next tag", group = "workspaces"}),
    awful.key({ modkey,  }, "g", awful.tag.viewprev,
              {description = "move to prev tag", group = "workspaces"}),
	-- TODO fix this horrible hack
    awful.key({ modkey,  "Shift" }, ";",
		function ()
			local to_move = client.focus
			awful.tag.viewnext()
			if to_move then
				to_move:move_to_tag(awful.screen.focused().selected_tag)
			end
		end,
			  {description = "move with window to next tag", group = "workspaces"}),
	awful.key({ modkey, "Shift" }, "g",
		function ()
			local to_move = client.focus
			awful.tag.viewprev()
			if to_move then
				to_move:move_to_tag(awful.screen.focused().selected_tag)
			end
		end,
		{description = "move client to previous tag", group = "layout"}),

	awful.key({ modkey,  }, "o", awful.tag.history.restore,
              {description = "move to last tag", group = "workspaces"}),
    -- Menubar
    -- awful.key({ modkey }, "p", function() menubar.show() end,
    --           {description = "show the menubar", group = "launcher"})
    awful.key({ modkey }, "p", function () awful.spawn("rofi -show run") end,
              {description = "launch application", group = "launcher"}),
    awful.key({ modkey }, "b", function () awful.spawn("rofi -show window") end,
              {description = "launch application", group = "launcher"})
)

clientkeys = awful.util.table.join(
    awful.key({ modkey,           }, "u",
        function (c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        {description = "toggle fullscreen", group = "client"}),
    awful.key({ modkey,           }, "x",      function (c) c:kill()                         end,
              {description = "close", group = "client"}),
    awful.key({ modkey,  }, "t",  awful.client.floating.toggle                     ,
              {description = "toggle floating", group = "client"}),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end,
              {description = "move to master", group = "client"}),
    awful.key({ modkey, "Shift"   }, "o",      function (c) c:move_to_screen()               end,
              {description = "move to screen", group = "client"}),
    -- awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end,
    --           {description = "toggle keep on top", group = "client"}),
    -- awful.key({ modkey,           }, "n",
    --     function (c)
    --         -- The client currently has the input focus, so it cannot be
    --         -- minimized, since minimized clients can't have the focus.
    --         c.minimized = true
    --     end ,
    --     {description = "minimize", group = "client"}),
    awful.key({ modkey, "Shift"   }, "m",
        function (c)
            c.maximized = not c.maximized
            c:raise()
        end ,
        {description = "maximize", group = "client"})
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 10 do
    globalkeys = awful.util.table.join(globalkeys,
        -- View tag only.
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = awful.screen.focused()
                        local tag = screen.tags[i]
                        if tag then
                           tag:view_only()
                        end
                  end,
                  {description = "view tag #"..i, group = "tag"}),
        -- Toggle tag display.
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = awful.screen.focused()
                      local tag = screen.tags[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end,
                  {description = "toggle tag #" .. i, group = "tag"}),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:move_to_tag(tag)
							  tag:view_only()
						  end
                     end
                  end,
                  {description = "move focused client to tag #"..i, group = "tag"}),
        -- Toggle tag on focused client.
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:toggle_tag(tag)
                          end
                      end
                  end,
                  {description = "toggle focused client on tag #" .. i, group = "tag"})
    )
end

clientbuttons = awful.util.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize))

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     raise = true,
                     keys = clientkeys,
                     buttons = clientbuttons,
                     screen = awful.screen.preferred,
                     placement = awful.placement.no_overlap+awful.placement.no_offscreen
     }
    },

    -- Floating clients.
    { rule_any = {
        instance = {
          "DTA",  -- Firefox addon DownThemAll.
          "copyq",  -- Includes session name in class.
        },
        class = {
          "Arandr",
          "Gpick",
          "Kruler",
          "MessageWin",  -- kalarm.
          "Sxiv",
          "Wpa_gui",
          "pinentry",
          "veromix",
          "xtightvncviewer"},

        name = {
          "Event Tester",  -- xev.
        },
        role = {
          "AlarmWindow",  -- Thunderbird's calendar.
          "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
        }
      }, properties = { floating = true }},

    -- Add titlebars to normal clients and dialogs
    { rule_any = {type = {
					  -- "normal",
					  "dialog" }
      }, properties = { titlebars_enabled = true }
	},
	{ rule_any = {
		  class = { "steam" },
	},
	  properties = {
		  titlebars_enabled = false,
		  floating = true,
		  border_width = 0,
		  border_color = 0,
		  size_hints_honor = false
	  }
	},

    -- Set Firefox to always map on the tag named "2" on screen 1.
    -- { rule = { class = "Firefox" },
    --   properties = { screen = 1, tag = "2" } },
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    if awesome.startup and
      not c.size_hints.user_position
      and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
    -- buttons for the titlebar
    local buttons = awful.util.table.join(
        awful.button({ }, 1, function()
            client.focus = c
            c:raise()
            awful.mouse.client.move(c)
        end),
        awful.button({ }, 3, function()
            client.focus = c
            c:raise()
            awful.mouse.client.resize(c)
        end)
    )

    awful.titlebar(c) : setup {
        { -- Left
            awful.titlebar.widget.iconwidget(c),
            buttons = buttons,
            layout  = wibox.layout.fixed.horizontal
        },
        { -- Middle
            { -- Title
                align  = "center",
                widget = awful.titlebar.widget.titlewidget(c)
            },
            buttons = buttons,
            layout  = wibox.layout.flex.horizontal
        },
        { -- Right
            awful.titlebar.widget.floatingbutton (c),
            awful.titlebar.widget.maximizedbutton(c),
            awful.titlebar.widget.stickybutton   (c),
            awful.titlebar.widget.ontopbutton    (c),
            awful.titlebar.widget.closebutton    (c),
            layout = wibox.layout.fixed.horizontal()
        },
        layout = wibox.layout.align.horizontal
    }
end)

-- Enable sloppy focus, so that focus follows mouse.
-- client.connect_signal("mouse::enter", function(c)
--     if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
--         and awful.client.focus.filter(c) then
--         client.focus = c
--     end
-- end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}

-- MULTI MONITOR MIGRATION
tag.connect_signal(
	"request::screen", function(t)
		for s in screen do
			if s ~= t.screen then
				local t2 = awful.tag.find_by_name(s, t.name)
				if t2 then
					-- There's a tag with the same name so we can merge them together
					local clients = t:clients()
					for key,client in pairs(clients) do
						client:move_to_tag(t2)
					end
				end
				-- We are SOL, let us be migrated in the fallback
				return
			end
		end
end)


-- Locking
-- awful.spawn.with_shell("sleep 1s && xset dpms 3600 3600 3600 && xset s 3600 && xss-lock -- i3lock -c 262626 --nofork")
awful.spawn.with_shell("sleep 1s && xset dpms 3600 3600 3600")

#!/bin/bash
set -euo pipefail
set -x

MONITOR="HDMI-A-0"
PAD="HUION Huion Tablet Pad pad"
PEN="HUION Huion Tablet Pen stylus"

for ID in $(xinput | grep 'HUION' | grep 'pointer' | grep -Po '(?<=id=)[0-9]+'); do
	xinput map-to-output "$ID" "$MONITOR"
done

if $(xsetwacom --list | grep -q "$PAD"); then
	xsetwacom --set "$PAD" Button 1 "key +ctrl +z -z -ctrl"
	xsetwacom --set "$PAD" Button 2 "key e"
	xsetwacom --set "$PAD" Button 3 "key +ctrl +shift +z -z -shift -ctrl"
	xsetwacom --set "$PAD" Button 8 "key +ctrl"
else
	PAD="HUION Huion Tablet_HS611 Pad pad"
	PEN="HUION Huion Tablet_HS611 stylus"
	xsetwacom --set "$PAD" Button 1 "key ctrl"
	xsetwacom --set "$PAD" Button 2 "key m"
	xsetwacom --set "$PAD" Button 3 "key +shift" # brush
	xsetwacom --set "$PAD" Button 8 "key +slash"
	xsetwacom --set "$PAD" Button 9 "key e"
	xsetwacom --set "$PAD" Button 10 "key +ctrl +z -z -ctrl"
	xsetwacom --set "$PAD" Button 11 "key +ctrl +shift +z -z -shift -ctrl"

	xsetwacom set "$PEN" Button 3 "key +Ctrl +space" # zoom

	# xsetwacom --set "$PAD" Button 15 "key -"
	# xsetwacom --set "$PAD" Button 16 "key +ctrl +shift + -shift -ctrl"
fi

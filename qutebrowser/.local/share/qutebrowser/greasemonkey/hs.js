// ==UserScript==
// @name         HSChatExpander
// @include      http://*homestuck.com/*
// @include      https://*homestuck.com/*
// @run-at       document-end
// ==/UserScript==

// A simple script to pop open chat logs.
Array.from(document.getElementsByClassName('o_chat-log-btn')).
	forEach((e) => e.click());

#!/bin/bash

set -eux

# A script to generate jay's 'integration' build of qutebrowser.
# Run like  ~/dotfiles/qutebrowser/gen_integration.sh from inside the repo
# Assumes 'upstream' in qb upstream, and that hub is installed
#
# Note: this is rather insecure, as it pulls from a bunch of people's repos.
# Please be mindful when using this.

git status

# fetches
git fetch upstream

hub fetch jgkamat
hub fetch toofar

git checkout integration
git reset --hard upstream/master

git merge jgkamat/jay/str-validation-cache --no-edit
git merge jgkamat/jay/keys-unneeeded-code --no-edit
git merge jgkamat/jay/scroll-perf --no-edit
git merge jgkamat/jay/autosave-perf --no-edit
git merge jgkamat/jay/url-add --no-edit
git merge jgkamat/jay/private-window --no-edit
git merge toofar/history_nav_completion --no-edit
git merge jgkamat/jay/per-domain-bindings --no-edit
git merge jgkamat/jay/load-finished --no-edit
git merge jgkamat/jay/completion-skip --no-edit
git merge jgkamat/jay/scroll-hints --no-edit
git merge jgkamat/jay/contenteditable --no-edit


# Slightly broken, not too much to fix though
# git merge rakanalh/feature/session-add-url-to-tab-3853 --no-edit
# Need to resolve a trivial merge conflict
# git merge toofar/feat/per_url_zoom.default --no-edit

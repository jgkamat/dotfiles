
"""Python memory profiling tools for qutebrowser."""

import tracemalloc
import gc

from qutebrowser.api import cmdutils, message

INITIAL_SNAPSHOT = False

initial_snapshot = None
current_snapshot = None

if INITIAL_SNAPSHOT:
	tracemalloc.start()
	gc.collect()
	initial_snapshot = tracemalloc.take_snapshot()
	current_snapshot = None

@cmdutils.register()
def tracemalloc_snapshot():
	"""Start tracemalloc tracing (if not running), and take a new snapshot."""
	global current_snapshot
	if not tracemalloc.is_tracing():
		message.info("Started tracemalloc!")
		tracemalloc.start()
	gc.collect()
	current_snapshot = tracemalloc.take_snapshot()
	message.info("Taken snapshot!")

@cmdutils.register()
def tracemalloc_stop():
	"""Stop tracemalloc tracing."""
	tracemalloc.stop()
	message.info("Stopped tracemalloc!")

@cmdutils.register()
def tracemalloc_diff(initial: bool = False):
	"""Use tracemalloc to diff memory usage.

		Take the diff between a starting snapshot and a current one with tracemalloc.
	"""
	if not tracemalloc.is_tracing():
		message.error("Tracemalloc not running!")
		return
	global current_snapshot
	gc.collect()
	new_snapshot = tracemalloc.take_snapshot()
	if initial or not current_snapshot:
		message.info("diffing with initial snapshot")
		s = initial_snapshot
	else:
		message.info("diffing with most recent snapshot")
		s = current_snapshot

	if s is None:
		message.error("No snapshot to compare to.")

	top_stats = new_snapshot.compare_to(s, 'lineno')
	top_stats = top_stats[:100]
	top_stats = "\n".join(map(str, top_stats))
	# TODO find a better way to out this
	message.info(top_stats)

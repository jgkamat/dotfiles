# Copyright (C) 2019  Jay Kamat <jaygkamat@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
A more advanced yanking solution for urls in qutebrowser.
A common use is automatically getting ssh urls instead of https
urls from gitlab.

It is configured with a 'smartyank-rules' file in the config dir,
which is a json map from regex to replace regex (python style).

Multiple rules may match, allowing threading.

Sample Rules:

{
	"^[^:]+://(git(?:hub|lab).com)/([^/]+)/([^/]+)$": "git@\\1:\\2/\\3.git",
	"^[^:]+://(git(?:hub|lab).com)/([^/]+)/([^/]+)/issues/(\\d+)$": "\\2/\\3#\\4"
}
"""

from qutebrowser.config.configfiles import ConfigAPI  # noqa: F401
from qutebrowser.config.config import ConfigContainer # noqa: F401
from qutebrowser.api import interceptor, cmdutils, message, apitypes

from PyQt5.QtGui import QClipboard
from PyQt5.QtWidgets import QApplication
from PyQt5.QtCore import QUrl

import typing, json, re

config = config  # type: ConfigAPI # noqa: F821 pylint: disable=E0602,C0103
c = c  # type: ConfigContainer # noqa: F821 pylint: disable=E0602,C0103

SMARTYANK_FILE = config.configdir / "smartyank-rules"

smartyank_rules = {}  # type: typing.Dict[str, str]

@cmdutils.register()
def smartyank_write() -> None:
	"""Write smartyank rules to disk."""
	global smartyank_rules
	with open(SMARTYANK_FILE, "w") as f:
		json.dump(smartyank_rules, f,
				  indent=4, separators=(',', ': '))

@cmdutils.register()
def smartyank_read() -> None:
	"""Write smartyank rules to disk."""
	global smartyank_rules
	if not SMARTYANK_FILE.exists():
		return
	with open(SMARTYANK_FILE, "r") as f:
		try:
			smartyank_rules = json.load(f)
		except Exception as e:
			message.error("Error decoding rules: {}".format(e))

# Read file if present, otherwise, create it
if not SMARTYANK_FILE.exists():
	smartyank_write()
else:
	smartyank_read()

def _set_clipboard(to_set: str, selection: bool = False, quiet: bool = False) -> None:
	mode = QClipboard.Selection if selection else QClipboard.Clipboard
	QApplication.clipboard().setText(to_set, mode=mode)
	where = "selection" if selection else "clipboard"
	if not quiet:
		message.info("Wrote {} to {}.".format(to_set, where))

@cmdutils.register()
@cmdutils.argument("tab", value=cmdutils.Value.cur_tab)
def smartyank(tab: apitypes.Tab, sel=False, quiet=False, debug=False):
	"""Provide more advanced yanking functionality than the standard yank.

		Args:
			sel: Use the primary selection instead of the clipboard.
			quiet: Don't show an information message.
	"""
	global smartyank_rules
	url = tab.url()
	if not url.isValid():
		message.error("Url was not valid!")
		return
	url = url.toString(QUrl.RemovePassword | QUrl.FullyEncoded)

	for pattern, replace in smartyank_rules.items():
		old_url = url
		if debug:
			message.info(
				"Attempting '{}'->'{}' on '{}'"
				.format(pattern, replace, url))
		url = re.sub(pattern, replace, url)
		if debug and old_url != url:
			message.info("Hit: {}".format(url))

	_set_clipboard(url, sel, quiet)


"""Tools for cpu profiling qutebrowser."""

import cProfile
import subprocess
import threading
import tempfile
import os
import distutils.spawn

from qutebrowser.api import cmdutils, message

current_profile = None

@cmdutils.register()
def cprofile_start():
	"""Start or continue profile."""
	global current_profile
	if current_profile is not None:
		current_profile.disable()
	current_profile = cProfile.Profile()
	current_profile.enable()
	message.info("Profiling started!")

@cmdutils.register()
def cprofile_pause():
	"""Pause profiling."""
	global current_profile
	if current_profile is not None:
		current_profile.disable()
		message.info("Profiling paused!")
	else:
		message.error("No profile is running!")


@cmdutils.register()
def cprofile_stop():
	"""Stop profiling and display results."""
	global current_profile
	if not (distutils.spawn.find_executable("pyprof2calltree") and
			distutils.spawn.find_executable("kcachegrind")):
		message.error("pyprof2calltree and kcachegrind not installed!")
		return
	cprofile_pause()
	if current_profile is None:
		return
	def dump():
		with tempfile.TemporaryDirectory() as dirname:
			pname = os.path.join(dirname, "profile")
			cname = os.path.join(dirname, "callgraph")
			current_profile.dump_stats(pname)
			message.info("Launching kcachegrind")
			subprocess.run(['pyprof2calltree', '-k', '-i', pname, '-o', cname])
	t = threading.Thread(target=dump)
	t.daemon = True
	t.start()

@cmdutils.register()
def cprofile_abort():
	"""Stop profiling and cleanup."""
	global current_profile
	if current_profile is not None:
		current_profile.disable()
		current_profile = None
	message.info("All profiles cleaned up!")

from qutebrowser.config.configfiles import ConfigAPI  # noqa: F401
from qutebrowser.config.config import ConfigContainer # noqa: F401
import os

config = config  # type: ConfigAPI # noqa: F821 pylint: disable=E0602,C0103
c = c  # type: ConfigContainer # noqa: F821 pylint: disable=E0602,C0103

dirname = os.path.dirname(os.path.abspath(__file__))

DARK_STYLESHEET = config.configdir / \
	"stylesheets/solarized-everything-css/" \
	"css/darculized/darculized-aggressive.css"

if DARK_STYLESHEET.exists():
	c.content.user_stylesheets = [str(DARK_STYLESHEET)]
	c.content.user_stylesheets.append(
		os.path.join(dirname, "./phabricator.css"))
	c.content.user_stylesheets.append(
		os.path.join(dirname, "./joverrides.css"))
	c.content.user_stylesheets.append(
		os.path.join(dirname, "./nyan-scrollbar.user.css"))

	c.colors.webpage.bg = "#262626"

	# Add toggling binding
	config.bind(',s', 'config-cycle --temp content.user_stylesheets "' +
			   str(c.content.user_stylesheets) + '" ""')

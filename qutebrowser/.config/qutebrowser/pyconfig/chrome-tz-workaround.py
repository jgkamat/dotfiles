
import os

from qutebrowser.utils import qtutils

if not qtutils.version_check("5.13"):
	if os.environ.get('TZ', '') == ":/etc/localtime":
		os.environ['TZ'] = "America/Los_Angeles"

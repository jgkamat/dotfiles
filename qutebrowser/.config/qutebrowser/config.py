from qutebrowser.config.configfiles import ConfigAPI  # noqa: F401
from qutebrowser.config.config import ConfigContainer # noqa: F401

import sys, os

config = config  # type: ConfigAPI # noqa: F821 pylint: disable=E0602,C0103
c = c  # type: ConfigContainer # noqa: F821 pylint: disable=E0602,C0103

# Load autoconfig before the rest of python config
config.load_autoconfig()

config.source('pyconfig/aliases.py')
config.source('pyconfig/themes.py')
config.source('pyconfig/bunny.py')
config.source('pyconfig/chrome-tz-workaround.py')


try:
	from qutebrowser.api import message

	# 'plugins'
	config.source('pyconfig/qute-tracemalloc.py')
	config.source('pyconfig/qute-cprofile.py')

	config.source('pyconfig/redirectors.py')
	config.source('pyconfig/smartyank.py')

	sys.path.append(os.path.join(sys.path[0], 'jmatrix'))
	config.source("jmatrix/jmatrix/integrations/qutebrowser.py")
	sys.path.append(os.path.join(sys.path[0], 'jblock'))
	config.source("jblock/jblock/integrations/qutebrowser.py")

	# must load late as possible
	config.source("pyconfig/whitelist-first-party.py")

except ImportError:
	pass

config.source('qutenyan/nyan.py')
